/* ------------------------------------------------------------------------------
@name: Calculator Chart
@description: Calculator Chart
--------------------------------------------------------------------------------- */

/**** CALCULATORCHART
 * --------------------------------------------------------------------------------- */
var calculatorChart = (function () {
  // --- createElement
  var createElement = function() {
    // create wrapper
    var _graphicelements = '<div class="ocbc-card-legend"></div>'+
                            '<div class="ocbc-graphic-bar">'+
                              '<div class="ocbc-graphic-bar__box">'+
                                '<div class="ocbc-graphic-bar__visual">'+
                                  '<div class="ocbc-graphic-bar__visual__area-wrapper"></div>'+
                                  '<div class="ocbc-graphic-bar__visual__bar-wrapper"></div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="ocbc-graphic-bar__range"></div>'+
                            '</div>';
    $('.js-calc-graphic-chart').html(_graphicelements);
  }

  // --- draw
  var draw = function(data) {
    var _legends = '',
    _colorArea = '#dae0e6',
    _colors = ['#329989', '#d6ad14', '#c41432', '#d16701'],
    _color = '',
    _barChart = '',
    _areaChart = '',
    _ranges = '',
    _targetBar = 0,
    _heightArea1 = '',
    _heightArea2 = '',
    _bgArea1 = '',
    _bgArea2 = '',
    _tooltipPosition = '',
    _chartTarget = '';

    // legend + graphic
    $.each(data.data, function(idxD, valD) {
      var _amount = handleFormatIDR(valD.amount),
        _type = '',
        _target = 0,
        _classDot = '',
        _dotEl = '',
        _legendItem = '',
        _legendLabel = '',
        _barItem = '',
        _areaItem = '',
        _heightBar = '',
        _posBar = '',
        _tootipClass = '';

      if (data.calc == 'pendidikan') {

        if (idxD < 3) {
          if (idxD == 0) {
            _posBar = 'left:0;';
            if (data.data[0].amount > data.data[2].amount) {
              _heightBar = 'height: 144px;';
              _heightArea1 = 'border-bottom-width: 144px;';
              _tooltipPosition = 'style="bottom: 24px;left: 400px;"';
            } else {
              _heightBar = 'height: 24px;';
              _heightArea1 = 'border-bottom-width: 24px;';
              _tooltipPosition = 'style="bottom: 88px;left: 400px;"';
            }
            _heightArea2 = 'border-bottom-width: 24px;';
          } else  if (idxD == 1) {
            _posBar = 'right:64px;';
            if (data.data[1].amount > data.data[2].amount) {
              _heightBar = 'height: 100%;';
              if (data.data[0].amount > data.data[2].amount) {
                _heightArea1 += 'border-top-width: 96px;border-right: solid 800px '+_colorArea+';';
              } else {
                _heightArea1 += 'border-top-width: 216px;border-right: solid 800px '+_colorArea+';';
              }
              _bgArea1 = ' ocbc-graphic-bar__visual__area--dark';
            } else {
              _heightBar = 'height: 144px;';
              _heightArea1 += 'border-top-width: 120px;border-right: solid 800px '+_colorArea+';';
            }
            _tootipClass = ' ocbc-graphic-bar__visual__bar--left-tooltip';
          } else {
            _posBar = 'right:0;';
            if (data.data[2].amount > data.data[0].amount && data.data[2].amount > data.data[1].amount) {
              _heightBar = 'height: 100%;';
              _heightArea2 += 'border-top-width: 216px;border-right: solid 864px'+_colorArea+';';
              _bgArea2 = ' ocbc-graphic-bar__visual__area--dark';
            } else if (data.data[2].amount > data.data[0].amount && data.data[2].amount < data.data[1].amount) {
              _heightBar = 'height: 144px;';
              _heightArea2 += 'border-top-width: 120px;border-right: solid 864px'+_colorArea+';';
            } else if (data.data[2].amount < data.data[0].amount && data.data[2].amount < data.data[1].amount) {
              _heightBar = 'height: 24px;';
              if (data.data[0].amount > data.data[2].amount) {
                _heightArea2 += 'border-top-width: 120px;border-left: solid 864px '+_colorArea+';';
              } else {
                _heightArea2 += 'border-top-width: 24px;border-right: solid 864px '+_colorArea+';';
              }
            }
            _tootipClass = ' ocbc-graphic-bar__visual__bar--left-tooltip';
          }


          _type = 'bar';
          _target = _targetBar;
          _color = _colors[idxD];

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';
          _barChart += _barItem;

          _targetBar++;

        } else {
          _type = 'area';

          if (data.data.length == 4) {
            _target = 0;
            _areaItem = '<div class="ocbc-graphic-bar__visual__area'+_bgArea1+'" style="'+_heightArea1+'">'+
                            '<div class="ocbc-graphic-bar__tooltip" '+_tooltipPosition+'>'+
                                '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                                '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                            '</div>'+
                        '</div>'+
                        '<div class="ocbc-graphic-bar__visual__area'+_bgArea2+'" style="'+_heightArea2+'"></div>';
          } else {
            if (idxD == 3) {
              _target = 1;
              _areaItem = '<div class="ocbc-graphic-bar__visual__area" style="'+_heightArea1+'"></div>';
              _areaItem += '<div class="ocbc-graphic-bar__visual__area'+_bgArea2+'" style="'+_heightArea2+'">'+
                              '<div class="ocbc-graphic-bar__tooltip" style="bottom: 96px;left: 432px;">'+
                                  '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                                  '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                              '</div>'+
                          '</div>';
            } else {
              _target = 2;
              _areaItem = '<div class="ocbc-graphic-bar__visual__area'+_bgArea2+'" style="'+_heightArea2+'">'+
                              '<div class="ocbc-graphic-bar__tooltip" style="bottom: 96px;left: 432px;">'+
                                  '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                                  '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                              '</div>'+
                          '</div>';
            }
          }
          _areaChart += _areaItem;
        }

      } else if (data.calc == 'kpr') {
        if (idxD == 0) {
          _type = 'bar';
          _target = 0;
          _color = _colors[0];
          _posBar = 'left: 0;';
          _heightBar = 'height: 24px;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';

        } else if (idxD == 1) {
          _type = 'area';
          _target = 0;

          _areaItem = '<div class="ocbc-graphic-bar__visual__area" style="border-bottom-width:24px;border-top-width:72px;border-right-width:432px;">'+
                          '<div class="ocbc-graphic-bar__tooltip" style="bottom:12px;left:216px;">'+
                              '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                              '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                              '<p class="ocbc-graphic-bar__label">'+valD.label+'</p>'+
                          '</div>'+
                      '</div>';

        } else if (idxD == 2) {
          _type = 'bar';
          _target = 1;
          _color = _colors[1];
          _posBar = 'left: 432px;';
          _heightBar = 'height: 96px;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';

        } else if (idxD == 3) {
          _type = 'bar';
          _target = 2;
          _color = _colors[3];
          _posBar = 'right: 0;';
          _heightBar = 'height: 100%;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _tootipClass = ' ocbc-graphic-bar__visual__bar--left-tooltip';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';

        } else if (idxD == 4) {
          _type = 'area';
          _target = 1;

          _areaItem = '<div class="ocbc-graphic-bar__visual__area" style="left:456px;border-bottom-width:96px;border-top-width:144px;border-right-width:432px;">'+
                          '<div class="ocbc-graphic-bar__tooltip" style="bottom:48px;left:216px;">'+
                              '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                              '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                          '</div>'+
                      '</div>';
        }

        _areaChart += _areaItem;
        _barChart += _barItem;

      } else if (data.calc == 'kpr-return') {
        if (idxD == 0) {
          _type = 'bar';
          _target = 0;
          _color = _colors[0];
          _posBar = 'left: 0;';
          _heightBar = 'height: 24px;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';

        } else if (idxD == 1) {
          _type = 'area';
          _target = 0;

          _areaItem = '<div class="ocbc-graphic-bar__visual__area" style="border-bottom-width:24px;border-top-width:62px;border-right-width:736px;">'+
                          '<div class="ocbc-graphic-bar__tooltip" style="bottom:8px;left:368px;">'+
                              '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                              '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                              '<p class="ocbc-graphic-bar__label">'+valD.label+'</p>'+
                          '</div>'+
                      '</div>';

        } else if (idxD == 2) {
          _type = 'bar';
          _target = 1;
          _color = _colors[1];
          _posBar = 'right:128px;';
          _heightBar = 'height: 86px;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _tootipClass = ' ocbc-graphic-bar__visual__bar--left-tooltip';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__percentage">'+
                            '<p class="ocbc-graphic-bar__percentage__txt">'+valD.percentage+'</p>'+
                        '</div>'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';

        } else if (idxD == 3) {
          _type = 'bar';
          _target = 2;
          _color = _colors[3];
          _posBar = 'right: 0;';
          _heightBar = 'height: 100%;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _tootipClass = ' ocbc-graphic-bar__visual__bar--left-tooltip';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';

        } else if (idxD == 4) {
          _type = 'bar';
          _target = 3;
          _color = _colors[2];
          _posBar = 'right:64px;';
          _heightBar = 'height:116px;';

          // has dot
          _classDot = ' ocbc-card--legend--dot';
          _dotEl = '<span class="ocbc-card__dot" style="background-color:'+_color+';"></span>';

          _tootipClass = ' ocbc-graphic-bar__visual__bar--left-tooltip';

          _barItem = '<div class="ocbc-graphic-bar__visual__bar'+_tootipClass+'" style="'+_posBar+_heightBar+'background-color: '+_color+';">'+
                        '<div class="ocbc-graphic-bar__percentage">'+
                            '<p class="ocbc-graphic-bar__percentage__txt">'+valD.percentage+'</p>'+
                        '</div>'+
                        '<div class="ocbc-graphic-bar__tooltip">'+
                            '<p class="ocbc-graphic-bar__title">'+valD.title+'</p>'+
                            '<p class="ocbc-graphic-bar__amount">'+_amount+'</p>'+
                        '</div>'+
                    '</div>';
        }

        _areaChart += _areaItem;
        _barChart += _barItem;

      }

      if (valD.label != undefined) {
        _legendLabel = '<p class="ocbc-card__label">'+valD.label+'</p>';
      }

      // -- legend
      _legendItem = '<div class="ocbc-card ocbc-card--legend'+_classDot+'" data-type="'+_type+'" data-target="'+_target+'">'+
                        '<div class="ocbc-card__box">'+_dotEl+
                            '<div class="ocbc-card__txt">'+
                                '<p class="ocbc-card__title">'+valD.title+'</p>'+
                                '<p class="ocbc-card__amount">'+_amount+'</p>'+_legendLabel+
                            '</div>'+
                        '</div>'+
                    '</div>';
      _legends += _legendItem;
      // -- /legend

    });

      if (data.chart != undefined) {
        _chartTarget = '[data-chart="'+data.chart+'"]';
      }

    // dom legend
    $('.js-calc-graphic-chart'+_chartTarget+' .ocbc-card-legend').html(_legends);
    // dom area
    $('.js-calc-graphic-chart'+_chartTarget+' .ocbc-graphic-bar__visual__area-wrapper').html(_areaChart);
    // dom bar
    $('.js-calc-graphic-chart'+_chartTarget+' .ocbc-graphic-bar__visual__bar-wrapper').html(_barChart);

    // range
    $.each(data.label, function(idxR, valR) {
      var _rangeItem = '',
      _align = '';

      // label 2
      if (data.label.length > 2 && idxR == 1) {
        _align = 'style="right:420px;"';
      }

      if (idxR == (data.label.length - 1)) {
        _align = 'style="right:0;"';
      }

      _rangeItem = '<div class="ocbc-graphic-bar__range__item" '+_align+'>'+
        '<p class="ocbc-graphic-bar__range__txt">'+valR.title+'</p>'+
        '<h4 class="ocbc-graphic-bar__range__number">'+valR.number+'</h4>'+
      '</div>';
      _ranges += _rangeItem;
    });

    $('.js-calc-graphic-chart'+_chartTarget+' .ocbc-graphic-bar__range').html(_ranges);
    // end range
  }

  // --- handleCardLegend
  var handleCardLegend = {
    show: function(selector, parent) {
      var  _type = selector.attr('data-type'),
      _target = selector.attr('data-target'),
      _getPosLeft = 0;

      if (!selector.hasClass('ocbc-card--legend--selected')) {
        selector.addClass('ocbc-card--legend--selected');
        if(_type == 'bar') {
          parent.find('.ocbc-graphic-bar__visual__bar').eq(_target).addClass('ocbc-graphic-bar__visual__bar--show-tooltip');
          _getPosLeft = document.querySelector('.ocbc-graphic-bar__visual__bar--show-tooltip').offsetLeft - 16;
        } else {
          parent.find('.ocbc-graphic-bar__visual__area').eq(_target).addClass('ocbc-graphic-bar__visual__area--show-tooltip');
          if ($('.ocbc-input-range-slider').length && _target == 1) {
            _getPosLeft = document.querySelector('.ocbc-graphic-bar__visual__area--show-tooltip').offsetLeft + 48;
          } else {
            _getPosLeft = document.querySelector('.ocbc-graphic-bar__visual__area--show-tooltip .ocbc-graphic-bar__tooltip').offsetLeft - 48;
          }
        }

        if ($(window).width() <= 992) {
          parent.animate({
            scrollLeft: _getPosLeft
          }, 300);
        }
      }
    },

    hide: function() {
      $('.js-calc-graphic-chart').find('.ocbc-card').removeClass('ocbc-card--legend--selected');
      $('.js-calc-graphic-chart').find('.ocbc-graphic-bar__visual__bar').removeClass('ocbc-graphic-bar__visual__bar--show-tooltip');
      $('.js-calc-graphic-chart').find('.ocbc-graphic-bar__visual__area').removeClass('ocbc-graphic-bar__visual__area--show-tooltip');
    }

  }

  // --- handleBar
  var handleBar = {
    show: function(selector) {
      var _index = selector.index(),
      _parent = selector.parents('.js-calc-graphic-chart');

      _parent.find('.ocbc-card[data-type="bar"][data-target="'+_index+'"]').addClass('ocbc-card--legend--selected');
      _parent.find('.ocbc-graphic-bar__visual__bar').eq(_index).addClass('ocbc-graphic-bar__visual__bar--show-tooltip');
    },

    hide: function() {
      $('.js-calc-graphic-chart').find('.ocbc-card').removeClass('ocbc-card--legend--selected');
      $('.js-calc-graphic-chart').find('.ocbc-graphic-bar__visual__bar').removeClass('ocbc-graphic-bar__visual__bar--show-tooltip');
    }

  }

  // --- handleEvent
  var handleEvent = function() {
    // card legend
    $('body').on('click', function(e) {
      if ($('.ocbc-card--legend--selected').length && $(window).width() <= 992) {
        handleCardLegend.hide();
      }

    // hover and click
    }).on('mouseenter mouseleave click', '.js-calc-graphic-chart .ocbc-card--legend', function(e) {
      var _this = $(this),
      _parent = _this.parents('.js-calc-graphic-chart');

      if ($(window).width() > 992) {
        if (e.type === 'mouseenter') {
          handleCardLegend.hide();
          handleCardLegend.show(_this, _parent);
        } else if (e.type === 'mouseleave') {
          handleCardLegend.hide();
        }

      } else {
        if (e.type === 'click') {
          handleCardLegend.hide();
          handleCardLegend.show(_this, _parent);
        }
      }

    // stopPropagation
    }).on('click', '.ocbc-card-legend', function(e) {
      e.stopPropagation();
    });

    // graphic bar
    $('body').on('click', function(e) {
      if ($('.ocbc-card--legend--selected').length && $(window).width() <= 992) {
        handleBar.hide();
      }

    // hover and click
    }).on('mouseenter mouseleave click', '.js-calc-graphic-chart .ocbc-graphic-bar__visual__bar', function(e) {
      var _this = $(this);

      if ($(window).width() > 992) {
        if (e.type === 'mouseenter') {
          handleBar.hide();
          handleBar.show(_this);
        } else if (e.type === 'mouseleave') {
          handleBar.hide();
        }

      } else {
        if (e.type === 'click') {
          handleBar.hide();
          handleBar.show(_this);
        }
      }

    // stopPropagation
    }).on('click', '.ocbc-graphic-bar__visual__bar', function(e) {
      e.stopPropagation();
    });
  }

  // --- initialize
  var init = function () {
    createElement();
    handleEvent();
  }

  return {
    init: init,
    draw: draw
  }
})();


/****************************** CALL ALL FUNCTION ******************************/
calculatorChart.init();
