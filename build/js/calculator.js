/* ------------------------------------------------------------------------------
@name: Calculator
@description: Calculator dummy JS
--------------------------------------------------------------------------------- */

// -- REGEX
var WHITESPACE = /^ *$/;
var NUMREG = /^[0-9]+$/;

/**** GLOBAL FUNCTION
 * --------------------------------------------------------------------------------- */
// -- handleFormatIDR
var handleFormatIDR = function (number, prefix = 'Rp') {
  var _split = String(number).split('.'),
    _mod = _split[0].length % 3,
    _idr = _split[0].substr(0, _mod),
    _thousands = _split[0].substr(_mod).match(/\d{3}/gi),
    _separator = '',
    _result;

  // if thousands
  if (_thousands) {
    _separator = _mod ? ',' : '';
    _idr += _separator + _thousands.join(',');
  }

  _idr = (_split[1] != undefined ? _idr + '.' + _split[1] : _idr);
  _result = (prefix != undefined ? prefix + _idr : _idr);
  return _result;
};

// --- handleCleancurrency
var handleCleancurrency = function (string) {
  var _rp = string.split('Rp'),
    _numberFormat = _rp[0],
    _numberClean;

  if (_rp[1] != undefined) {
    _numberFormat = _rp[1];
  }
  _numberClean = parseInt(_numberFormat.split(',').join(''));
  if (Number.isNaN(_numberClean)) {
    _numberClean = 0;
  }
  return _numberClean;
};

// -- handleCleanPercentage
var handleCleanPercentage = function (string) {
  var _split = parseInt(string.split('%').join('')),
    _result = _split;
  if (Number.isNaN(_result)) {
    _result = 0;
  }
  return _result;
};

// -- handleFormatPercentage
var handleFormatPercentage = function (number) {
  var _result = number + '%';
  return _result;
};

// --- handleStartRangeSlider
var handleStartRangeSlider = function (data) {
  var _classname = data.slider[0].className,
    _target = _classname.replace(/\s/g, '.');

  if (data.min === data.from) {
    $(`.${_target.concat(' .irs-single')}`).css('opacity', '0');
  } else {
    $(`.${_target.concat(' .irs-single')}`).css('opacity', '1');
  }
};

// --- handleChangeRangeSlider
var handleChangeRangeSlider = function (data) {
  var _classname = data.slider[0].className,
    _target = _classname.replace(/\s/g, '.');

  var _max = parseInt(data.max) - (data.max / 20),
    _min = parseInt(data.min) + (data.max / 20);

  $(`.${_target.concat(' .irs-single')}`).css('opacity', '1');

  // left align
  if (_min > data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 0,
      'right': 'auto',
    });
  }
  // right align
  else if (_max < data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 'auto',
      'right': 0
    });
  } else {
    $(`.${_target.concat(' .irs-single')}`).css('right', 'auto');
  }
};

// --- handleFinishRangeSlider
var handleFinishRangeSlider = function (data) {
  var _classname = data.slider[0].className,
    _target = _classname.replace(/\s/g, '.');

  // --- if user select min value
  if (data.min === data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 0,
      'right': 'auto',
    });
  }
  // --- if user select max value
  else if (data.max === data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 'auto',
      'right': 0
    });
  }
};

// --- handleSwitchSection
var handleSwitchSection = function (selector, target) {
  var _parent = selector.parents('.ocbc-calc__form__section');

  $("html, body").animate({
    scrollTop: 0
  }, 250);

  _parent.addClass('ocbc-calc__form__section--hide');
  _parent.parents('.ocbc-calc').addClass('ocbc-calc--img-hide');
  setTimeout(function () {
    _parent.removeClass('ocbc-calc__form__section--hide ocbc-calc__form__section--active');
    $(`.ocbc-calc__form__section[data-step="${target}"]`).addClass('ocbc-calc__form__section--active');
    _parent.parents('.ocbc-calc').removeClass('ocbc-calc--img-hide');
  }, 250);

}

/**** CalculatorPensiun
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorPensiun = (function () {

  // --- handleValidationStep1
  var handleValidationStep1 = function () {
    var _isError = false,
      _usiaAndaEl = $('.js-usia-anda'),
      _usiaAndaVal = parseInt(_usiaAndaEl.val()),
      _usiaPensiunEl = $('.js-usia-pensiun'),
      _usiaPensiunVal = parseInt(_usiaPensiunEl.val()),
      _masaPensiunEl = $('.js-masa-pensiun'),
      _masaPensiunVal = parseInt(_masaPensiunEl.val());

    _usiaPensiunEl.parents('.ocbc-calc__flds__item').removeClass('ocbc-calc__flds__item--error-desc');
    _masaPensiunEl.parents('.ocbc-calc__flds__item').removeClass('ocbc-calc__flds__item--error-desc');
    if (_usiaPensiunVal <= _usiaAndaVal) {
      setTimeout(function () {
        _usiaPensiunEl.parents('.ocbc-calc__flds__item').addClass('ocbc-calc__flds__item--error-desc');
      }, 5);
      _isError = true;
    } else if (_masaPensiunVal <= _usiaPensiunVal) {
      setTimeout(function () {
        _masaPensiunEl.parents('.ocbc-calc__flds__item').addClass('ocbc-calc__flds__item--error-desc');
      }, 5);
      _isError = true;
    }

    if (_usiaAndaVal < 1) {
      _isError = true;
    } else if (_usiaPensiunVal < 1) {
      _isError = true;
    } else if (_masaPensiunVal < 1) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-next').prop('disabled', true);
    } else {
      $('.js-form-next').prop('disabled', false);
    }

  }

  // --- handleValidationStep2
  var handleValidationStep2 = function () {
    var _isError = false,
      _danaSaatIniVal = $('.js-dana-saat-ini').val(),
      _cleanDanaSaatIniVal = handleCleancurrency(_danaSaatIniVal),
      _biayaPerbulan = $('.js-biaya-perbulan').val(),
      _biayaPerbulanClean = handleCleancurrency(_biayaPerbulan);
      /*_pengembalianTahunanEl = $('.js-pengembalian-tahunan'),
      _pengembalianTahunanNumber = handleCleanPercentage(_pengembalianTahunanEl.val()),
      _pengembalianTahunanParent = _pengembalianTahunanEl.parents('.ocbc-field'),
      _errorElementPengembalianTahunan = _pengembalianTahunanParent.find('.ocbc-field__error');*/

    // cek dana saat ini
    if (_cleanDanaSaatIniVal < 1) {
      _isError = true;
    }

    // cek biaya perbulan
    if (_biayaPerbulanClean < 1) {
      _isError = true;
    }

    // error state _pengembalianTahunan > 100
    /*
    if (_pengembalianTahunanNumber > 100) {
      _isError = true;
      _pengembalianTahunanParent.addClass('ocbc-field--is-error');
      _errorElementPengembalianTahunan.text(_errorElementPengembalianTahunan.attr('data-invalid'));
    }
  
    // pengembalian tahunan < 1
    else if (_pengembalianTahunanNumber < 1) {
      _isError = true;
    } else {
      _pengembalianTahunanParent.removeClass('ocbc-field--is-error');
    }
 */
    if (_isError) {
      $('.js-form-submit').prop('disabled', true);
    } else {
      $('.js-form-submit').prop('disabled', false);
    }
  }

  // --- handleValidationStep5
  var handleValidationStep5 = function () {

    var _isError = false,
      _apaSudahPernahStructuredProduct = $('.js-apa-sudah-pernah-structuredproduct').find(":selected").val();

    
    if (!Boolean(_apaSudahPernahStructuredProduct)) {
      _isError = true;
    
    }

    //  sudah pernah structured product
    if (_apaSudahPernahStructuredProduct == '1') {
      // show pernah structured product
      $('.ocbc-calc__flds__item[data-item="sudah-pernah-structuredproduct"]').removeClass('ocbc-calc__flds__item--hide');
    }  else {
      // hide pernah structured product
      $('.ocbc-calc__flds__item[data-item="sudah-pernah-structuredproduct"]').addClass('ocbc-calc__flds__item--hide');

    }

    if (_isError) {
      $('[data-step="step-5"] .js-form-next').prop('disabled', true);
    } else {
      $('[data-step="step-5"] .js-form-next').prop('disabled', false);
    }
  }

  // --- handleRangeSlider
  var handleRangeSlider = function () {
    // initialize
    $('.js-usia-anda, .js-usia-pensiun, .js-masa-pensiun').ionRangeSlider({
      skin: "round",
      step: 1,
      grid_snap: false,
      onStart: function (data) {
        // kebutuhan UI
        handleStartRangeSlider(data);
      },
      onChange: function (data) {
        // kebutuhan UI
        handleChangeRangeSlider(data);
        // Validate
        handleValidationStep1();
      },
      onFinish: function (data) {
        // kebutuhan UI
        handleFinishRangeSlider(data);
      }
    });
  }

  // --- handleClick
  var handleClick = function () {
    // Next
    $('.js-form-next').on('click', function (e) {
      var _this = $(this),
        _next = _this.attr('data-next');
      handleSwitchSection(_this, _next);
      e.preventDefault();
    });

    // Back
    $('.js-back-form').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      handleSwitchSection(_this, _back);

    });

  }

  // --- handleInput
  var handleInput = function () {

    // Dana Saat Ini + Biaya Perbulan
    $('.js-dana-saat-ini, .js-biaya-perbulan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidationStep2();

      // set val
      _this.val(_formatCurrency);

    });

    // apa sudah pernah structured product
    $('.js-apa-sudah-pernah-structuredproduct').on('change', function (e) {
      var _this = $(this);

      // validation
      handleValidationStep5();

    });


    // Pengembalian Tahunan
    /*
    $('.js-pengembalian-tahunan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // validation
      handleValidationStep2();

      // set val
      _this.val(_percentage);

    });

    // Pengembalian Tahunan Backspace
    $('.js-pengembalian-tahunan').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });*/

    // Dana Saat Ini + Biaya Perbulan
    $('.js-dana-saat-ini, .js-biaya-perbulan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _lengthVal = e.target.value.length;
      if (!NUMREG.test(_key) || _lengthVal > 3) return false;
    });

  }

  // --- initialize
  var init = function () {
    if ($('[data-form="pensiun"]').length) {
      handleRangeSlider();
      handleInput();
      handleClick();
    }

  }

  return {
    init: init
  }

})();


/**** CalculatorAsuransi
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorAsuransi = (function () {

  // --- handleValidation
  var handleValidation = function () {

    var _isError = false,
      _asuransiUntuk = $('.js-select-insurance select').val(),
      _usiaAnda = parseInt($('.js-usia-anda').val());

    // keluarga
    if (_asuransiUntuk == 'keluarga') {
      var _errorBool = 0;
      $('.js-usia-tertanggung').each(function (index, value) {
        var _this = $(this),
          _val = _this.val();
        if (_val < 1) {
          _errorBool += 1;
        }
      });
      // keluarga
      _isError = Boolean(Number(_errorBool));
      // anda
      if (_usiaAnda < 1) {
        _isError = true;
      }
    }

    // pasangan
    else if (_asuransiUntuk == 'pasangan') {
      var _usiaPasangan = parseInt($('.js-usia-pasangan').val());
      // pasangan
      if (_usiaPasangan < 1) {
        _isError = true;
      }
      // anda
      if (_usiaAnda < 1) {
        _isError = true;
      }
    }
    // anda
    else {
      if (_usiaAnda < 1) {
        _isError = true;
      }
    }

    if (_isError) {
      $('.js-form-submit').prop('disabled', true);
    } else {
      $('.js-form-submit').prop('disabled', false);
    }

  }

  // --- handleRangeSlider
  var handleRangeSlider = function () {
    // initialize
    $('.js-usia-anda, .js-usia-pasangan, .js-usia-tertanggung').ionRangeSlider({
      skin: "round",
      step: 1,
      grid_snap: false,
      onStart: function (data) {
        // kebutuhan UI
        handleStartRangeSlider(data);
      },
      onChange: function (data) {
        // kebutuhan UI
        handleChangeRangeSlider(data);
        // Validate
        handleValidation();
      },
      onFinish: function (data) {
        // kebutuhan UI
        handleFinishRangeSlider(data);
      }
    });
  }

  // --- handleChange
  var handleChange = function () {
    $('.js-select-insurance select').on('change', function (e) {
      var _this = $(this),
        _value = _this.val();

      // Validate
      handleValidation();

      $('.ocbc-calc__pane').removeClass('ocbc-calc__pane--active');
      if (_value != 'saya') {
        $(`.ocbc-calc__pane[data-pane=${_value}]`).addClass('ocbc-calc__pane--active');
      }

    });

  }

  // --- handleClone
  var handleClone = function () {
    $('.js-clone-family-insurance').on('click', function (e) {
      var _this = $(this),
        _label = _this.attr('data-label'),
        _parent = _this.parents('.ocbc-calc__flds__add'),
        _pane = _this.parents('.ocbc-calc__pane'),
        _multipleWrapper = _pane.find('.ocbc-calc__flds__multiple'),
        _lengthItem = _multipleWrapper.find('.ocbc-calc__flds__item').length,
        _templateClose = $('.ocbc-calc__flds__del').html();

      if (_lengthItem < 5) {

        if (_lengthItem == 4) {
          _parent.addClass('ocbc-calc__flds__add--disabled');
        }

        _multipleWrapper.find('.ocbc-calc__flds__item').eq(0).clone().appendTo(_multipleWrapper);
        _multipleWrapper.find('.ocbc-calc__flds__item').eq(_lengthItem).find('.ocbc-input-range-slider__label').append(_templateClose).find('.ocbc-input-range-slider__label__txt').text(`${_label} ${_lengthItem + 1}`);
        _multipleWrapper.find('.ocbc-calc__flds__item').eq(_lengthItem).find('.irs').remove();
        _multipleWrapper.find('.ocbc-calc__flds__item').eq(_lengthItem).find('.js-usia-tertanggung').removeClass('irs-hidden-input');
        _multipleWrapper.find('.ocbc-calc__flds__item').eq(_lengthItem).find('.js-usia-tertanggung').ionRangeSlider({
          skin: "round",
          step: 1,
          grid_snap: false,
          onStart: function (data) {
            // kebutuhan UI
            handleStartRangeSlider(data);
          },
          onChange: function (data) {
            // kebutuhan UI
            handleChangeRangeSlider(data);
            // Validate
            handleValidation();
          },
          onFinish: function (data) {
            // kebutuhan UI
            handleFinishRangeSlider(data);
          }
        });

        // Validate
        handleValidation();

      }

    });

  }

  // --- handleDelete
  var handleDelete = function () {
    $('body').on('click', function (e) {
      // --- click delete
    }).on('click', '.js-delete-family-insurance', function (e) {
      var _this = $(this),
        _pane = _this.parents('.ocbc-calc__pane'),
        _multipleWrapper = _pane.find('.ocbc-calc__flds__multiple'),
        _btnAddWrapper = _pane.find('.ocbc-calc__flds__add'),
        _label = _btnAddWrapper.find('.ocbc-calc__flds__add__btn').attr('data-label'),
        _lengthItem = _multipleWrapper.find('.ocbc-calc__flds__item').length;

      _this.parents('.ocbc-calc__flds__item').remove();

      if (_lengthItem <= 5) {
        _btnAddWrapper.removeClass('ocbc-calc__flds__add--disabled');
      }

      for (var i = 0; i < _lengthItem; i++) {
        _multipleWrapper.find('.ocbc-calc__flds__item').eq(i).find('.ocbc-input-range-slider__label__txt').text(`${_label} ${i + 1}`);
      }

    });

  }

  // --- initialize
  var init = function () {
    if ($('[data-form="asuransi"]').length) {
      handleRangeSlider();
      handleChange();
      handleClone();
      handleDelete();
    }

  }

  return {
    init: init
  }

})();


/**** CalculatorKPM
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorKPM = (function () {

  // --- handleValidation
  var handleValidation = function () {
    var _isError = false,
      _zoneVal = $('.js-select-zone select').val(),
      _OTDVal = $('.js-input-otd').val(),
      _cleanOTDVal = handleCleancurrency(_OTDVal),
      _tenorVal = $('.js-select-tenor select').val(),
      _DPNumber = handleCleanPercentage($('.js-input-dp').val()),
      _DPTotal = (_DPNumber * _cleanOTDVal) / 100,
      _costTotal = _cleanOTDVal - _DPTotal,
      _costPercent = (_costTotal / _cleanOTDVal) * 100;

    // cek value wilayah
    if (WHITESPACE.test(_zoneVal)) {
      _isError = true;
    }

    // cek value tenor
    if (WHITESPACE.test(_tenorVal)) {
      _isError = true;
    }

    // cek value OTD
    if (_cleanOTDVal < 1) {
      _isError = true;
    }

    // error state DP < 25%
    if (_DPNumber < 25) {
      _isError = true;
    }

    // error state cost < 50%
    if (_costPercent < 50) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-submit').prop('disabled', true);
    } else {
      $('.js-form-submit').prop('disabled', false);
    }

  }

  // --- handleChange
  var handleChange = function () {
    $('.js-select-zone select, .js-select-tenor select').on('change', function (e) {
      // validation
      handleValidation();
    });

  }

  // --- handleInput
  var handleInput = function () {

    // OTD
    $('.js-input-otd').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      console.log("Break It");
      // calculate
      handleCalculate();

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);

    });


    // DP
    $('.js-input-dp').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // calculate
      handleCalculate();

      // validation
      handleValidation();

      // set val
      _this.val(_percentage);

    });

    // DP Backspace
    $('.js-input-dp').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // Installment
    $('.js-input-installment').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);
      _this.val(_formatCurrency);
    });

    // OTD + DP + Installment
    $('.js-input-otd, .js-input-dp, .js-input-installment').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

  }


  // --- handleCalculate
  var handleCalculate = function () {
    var _OTDElement = $('.js-input-otd'),
      _DPElement = $('.js-input-dp'),
      _DPTotalElement = $('.js-input-dp-total'),
      _costTotalElement = $('.js-input-cost-total'),
      _OTDVal = _OTDElement.val(),
      _cleanOTD = handleCleancurrency(_OTDVal),
      _DPNumber = handleCleanPercentage(_DPElement.val()),
      _DPTotal = (_DPNumber * _cleanOTD) / 100,
      _costTotal = _cleanOTD - _DPTotal,
      _costPercent = (_costTotal / _cleanOTD) * 100,
      _DPTotalFormat, _costTotalFormat;


    // if angsuran (kita nggak handle)
    if ($('.js-payment-type:checked').val() == '2') {
      _DPTotal = 0;
      _costTotal = 0;
    }

    _DPTotalFormat = handleFormatIDR(_DPTotal);
    _costTotalFormat = handleFormatIDR(_costTotal);

    // error state DP < 25%
    if (_DPNumber < 25) {
      _DPElement.parents('.ocbc-field').addClass('ocbc-field--error-note');
    } else {
      _DPElement.parents('.ocbc-field').removeClass('ocbc-field--error-note');
    }

    // error state cost < 50%
    if (_costPercent < 50) {
      _costTotalElement.parents('.ocbc-field').addClass('ocbc-field--error-note');
    } else {
      _costTotalElement.parents('.ocbc-field').removeClass('ocbc-field--error-note');
    }

    // set
    _DPTotalElement.val(_DPTotalFormat);
    _costTotalElement.val(_costTotalFormat);
  }

  // --- handleClickRadio
  var handleClickRadio = function () {
    $('.js-payment-type').on('click', function () {
      var _this = $(this),
        _value = _this.val();

      // assumsi Uang Muka
      if (_value == '1') {
        $('input[data-payment="uang-muka"]').prop('readonly', false);
        $('input[data-payment="angsuran"]').prop('readonly', true);
      }
      // assumsi angsuran
      else {
        $('input[data-payment="angsuran"]').prop('readonly', false);
        $('input[data-payment="uang-muka"]').prop('readonly', true);
      }
    });
  }

  // --- initialize
  var init = function () {
    if ($('[data-form="kpm"]').length) {
      handleChange();
      handleInput();
      handleClickRadio();
    }

  }

  return {
    init: init
  }

})();

/**** CalculatorKPR
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorKPR = (function () {

  // --- handleValidation
  var handleValidation = function () {
    var _isError = false,
      _danaSaatIniVal = $('.js-dana-saat-ini').val(),
      _cleanDanaSaatIniVal = handleCleancurrency(_danaSaatIniVal),
      _biayaPerbulan = $('.js-biaya-perbulan').val(),
      _biayaPerbulanClean = handleCleancurrency(_biayaPerbulan),
      _pengembalianTahunanEl = $('.js-pengembalian-tahunan'),
      _pengembalianTahunanNumber = handleCleanPercentage(_pengembalianTahunanEl.val()),
      _rencanaKepemilikan = parseInt($('.js-rencana-kepemilikan').val());

    // cek value rencana kepemilikan
    if (_rencanaKepemilikan < 1) {
      _isError = true;
    }

    // cek value dana saat ini
    if (_cleanDanaSaatIniVal < 1) {
      _isError = true;
    }

    // pengembalian tahunan < 1
    if (_pengembalianTahunanNumber < 1) {
      _isError = true;
    }

    // cek value uang per bulan
    if (_biayaPerbulanClean < 1) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-submit').prop('disabled', true);
    } else {
      $('.js-form-submit').prop('disabled', false);
    }
  }

  // --- handleRangeSlider
  var handleRangeSlider = function () {
    // initialize
    $('.js-rencana-kepemilikan').ionRangeSlider({
      skin: "round",
      step: 1,
      grid_snap: false,
      onStart: function (data) {
        // kebutuhan UI
        handleStartRangeSlider(data);
      },
      onChange: function (data) {
        // kebutuhan UI
        handleChangeRangeSlider(data);
        // Validate
        handleValidation();
      },
      onFinish: function (data) {
        // kebutuhan UI
        handleFinishRangeSlider(data);
      }
    });
  }

  // --- handleInput
  var handleInput = function () {

    // Dana Saat Ini + Biaya Perbulan
    $('.js-dana-saat-ini, .js-biaya-perbulan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);

    });


    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // validation
      handleValidation();

      // set val
      _this.val(_percentage);

    });

    // Pengembalian Tahunan Backspace
    $('.js-pengembalian-tahunan').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // Dana Saat Ini + Biaya Perbulan
    $('.js-dana-saat-ini, .js-biaya-perbulan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _lengthVal = e.target.value.length;
      if (!NUMREG.test(_key) || _lengthVal > 3) return false;
    });

  }

  // --- initialize
  var init = function () {
    if ($('[data-form="kpr"]').length) {
      handleInput();
      handleRangeSlider();
    }

  }

  return {
    init: init
  }

})();

/**** CalculatorPendidikan
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorPendidikan = (function () {

  // --- handleValidationStep1
  var handleValidationStep1 = function () {
    var _isError = false,
      _persiapanDanaEl = $('.js-persiapan-dana'),
      _persiapanDanaVal = parseInt(_persiapanDanaEl.val()),
      _apaSudahTahuBiaya = $('.js-apa-sudah-tahu-biaya:checked').val();

    if (_persiapanDanaVal < 1) {
      _isError = true;
    }

    if (!Boolean(_apaSudahTahuBiaya)) {
      _isError = true;
    }

    //  sudah tahu biaya
    if (_apaSudahTahuBiaya == '1') {
      // next step 3
      $('[data-step="step-1"] .js-form-next').attr('data-next', 'step-3');

      // back step 3
      $('[data-step="step-3"] .js-back-form').attr('data-back', 'step-1');
/*
      // change action hanya untuk simulasi
      $('[data-form="pendidikan"]').attr('action', 'hasil-perhitungan-kalkulator-pendidikan-tahu-dana.html');
*/
      // show tahu biaya sekolah
      $('.ocbc-calc__flds__item[data-item="sudah-tahu"]').removeClass('ocbc-calc__flds__item--hide');

      var _biayaSekolahVal = $('.js-biaya-sekolah').val(),
        _cleanbiayaSekolahVal = handleCleancurrency(_biayaSekolahVal);

      // cek biaya
      if (_cleanbiayaSekolahVal < 1) {
        _isError = true;
      }

    }

    //  tidak tahu biaya
    else {
      // next step 2
      $('[data-step="step-1"] .js-form-next').attr('data-next', 'step-2');

      // back step 3
      $('[data-step="step-3"] .js-back-form').attr('data-back', 'step-2');
/*
      // change action hanya untuk simulasi
      $('[data-form="pendidikan"]').attr('action', 'hasil-perhitungan-kalkulator-pendidikan-tidak-tahu-dana.html');
*/
      // hide tahu biaya sekolah
      $('.ocbc-calc__flds__item[data-item="sudah-tahu"]').addClass('ocbc-calc__flds__item--hide');

    }


    if (_isError) {
      $('[data-step="step-1"] .js-form-next').prop('disabled', true);
    } else {
      $('[data-step="step-1"] .js-form-next').prop('disabled', false);
    }
  }

    // --- handleValidationStep4
    var handleValidationStep4 = function () {
      var _isError = false,
        _riskprofileVal = $('.js-input-riskprofile:checked').val();

      if (!Boolean(_riskprofileVal)) {
        _isError = true;
      }

      //  sudah tahu riskprofile
      if (_riskprofileVal != '0') {
        // next step di step4 lsg finish
        $('[data-step="step-4"] .js-form-next').removeAttr('data-next');


      }

      //  tidak tahu riskprofile
      else {
        // next step di step 4 ke 5
        $('[data-step="step-4"] .js-form-next').attr('data-next', 'step-5');

      }



      if (_isError) {
        $('[data-step="step-4"] .js-form-next').prop('disabled', true);
      } else {
        $('[data-step="step-4"] .js-form-next').prop('disabled', false);
      }
    }

  // --- handleValidationStep5
  var handleValidationStep5 = function () {

    var _isError = false,
      _apaSudahPernahStructuredProduct = $('.js-apa-sudah-pernah-structuredproduct').find(":selected").val();

    
    if (!Boolean(_apaSudahPernahStructuredProduct)) {
      _isError = true;
    
    }

    //  sudah pernah structured product
    if (_apaSudahPernahStructuredProduct == '1') {
      // show pernah structured product
      $('.ocbc-calc__flds__item[data-item="sudah-pernah-structuredproduct"]').removeClass('ocbc-calc__flds__item--hide');
    }  else {
      // hide pernah structured product
      $('.ocbc-calc__flds__item[data-item="sudah-pernah-structuredproduct"]').addClass('ocbc-calc__flds__item--hide');

    }

    if (_isError) {
      $('[data-step="step-5"] .js-form-submit').prop('disabled', true);
    } else {
      $('[data-step="step-5"] .js-form-submit').prop('disabled', false);
    }
  }

  // --- handleValidationStep2
  var handleValidationStep2 = function () {
    var _isError = false,
      _countryVal = $('.js-input-country:checked').val();

    if (!Boolean(_countryVal)) {
      _isError = true;
    }

    if (_isError) {
      $('[data-step="step-2"] .js-form-next').prop('disabled', true);
    } else {
      $('[data-step="step-2"] .js-form-next').prop('disabled', false);
    }
  }

  // --- handleValidationStep3
  var handleValidationStep3 = function () {
    var _isError = false,
      _danaSaatIniVal = $('.js-dana-saat-ini').val(),
      _cleanDanaSaatIniVal = handleCleancurrency(_danaSaatIniVal);
      /*
      _pengembalianTahunanEl = $('.js-pengembalian-tahunan'),
      _pengembalianTahunanNumber = handleCleanPercentage(_pengembalianTahunanEl.val());
      */

    // cek dana saat ini
    if (_cleanDanaSaatIniVal < 1) {
      _isError = true;
    }
/*
    // pengembalian tahunan < 1
    if (_pengembalianTahunanNumber < 1) {
      _isError = true;
    }
    */

    if (_isError) {
      $('[data-step="step-3"] .js-form-next').prop('disabled', true);
    } else {
      $('[data-step="step-3"] .js-form-next').prop('disabled', false);
    }


    /*
    if (_isError) {
      $('.js-form-submit').prop('disabled', true);
    } else {
      $('.js-form-submit').prop('disabled', false);
    }
    */

  }

 

  // --- handleRangeSlider
  var handleRangeSlider = function () {
    // initialize
    $('.js-persiapan-dana').ionRangeSlider({
      skin: "round",
      step: 1,
      grid_snap: false,
      onStart: function (data) {
        // kebutuhan UI
        handleStartRangeSlider(data);
      },
      onChange: function (data) {
        // kebutuhan UI
        handleChangeRangeSlider(data);
        // Validate
        handleValidationStep1();
      },
      onFinish: function (data) {
        // kebutuhan UI
        handleFinishRangeSlider(data);
      }
    });
  }

  // --- handleInput
  var handleInput = function () {

    // apa sudah tahu biaya
    $('.js-apa-sudah-tahu-biaya').on('click', function (e) {
      var _this = $(this);

      // validation
      handleValidationStep1();

    });

    // apa sudah pernah structured product
    $('.js-apa-sudah-pernah-structuredproduct').on('change', function (e) {
      var _this = $(this);

      // validation
      handleValidationStep5();

    });

    // biaya sekolah
    $('.js-biaya-sekolah').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidationStep1();

      // set val
      _this.val(_formatCurrency);

    });

    // dana saat ini
    $('.js-dana-saat-ini').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidationStep3();

      // set val
      _this.val(_formatCurrency);

    });

    // biaya sekolah + dana saat ini
    $('.js-biaya-sekolah, .js-dana-saat-ini').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

    // pengembalian tahunan backspace
    $('.js-pengembalian-tahunan').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // pengembalian tahunan
    $('.js-pengembalian-tahunan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // validation
      handleValidationStep3();

      // set val
      _this.val(_percentage);

    });

    // pengembalian tahunan
    $('.js-pengembalian-tahunan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _lengthVal = e.target.value.length;
      if (!NUMREG.test(_key) || _lengthVal > 3) return false;
    });

  }

  // --- handleClick
  var handleClick = function () {
    // next step 1
    $('[data-step="step-1"] .js-form-next').on('click', function (e) {
      var _this = $(this),
        _next = _this.attr('data-next');
      handleSwitchSection(_this, _next);
      e.preventDefault();
    });

    // next step 2
    $('[data-step="step-2"] .js-form-next').on('click', function (e) {
      var _this = $(this),
        _next = _this.attr('data-next');
      handleSwitchSection(_this, _next);
      e.preventDefault();
    });

    // next step 3
    $('[data-step="step-3"] .js-form-next').on('click', function (e) {
      var _this = $(this),
        _next = _this.attr('data-next');
      handleSwitchSection(_this, _next);
      e.preventDefault();
    });

    // next step 4
    $('[data-step="step-4"] .js-form-next').on('click', function (e) {
      var _this = $(this),
        _next = _this.attr('data-next');

        if( _next) {

          handleSwitchSection(_this, _next);
          e.preventDefault();
        }


    });



    // back step 2 tidak tahu biaya
    $('[data-step="step-2"] .js-back-form').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      handleSwitchSection(_this, _back);

    });

    // input country
    $('.js-input-country').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      // validation
      handleValidationStep2();

    });

    // input riskprofile
    $('.js-input-riskprofile').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      // validation
      handleValidationStep4();

    });

    // back step 3
    $('[data-step="step-3"] .js-back-form').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      handleSwitchSection(_this, _back);

    });

    // back step 4
    $('[data-step="step-4"] .js-back-form').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      handleSwitchSection(_this, _back);

    });
     // back step 5
     $('[data-step="step-5"] .js-back-form').on('click', function (e) {
      var _this = $(this),
        _back = _this.attr('data-back');

      handleSwitchSection(_this, _back);

    });

  }

  // --- initialize
  var init = function () {
    if ($('[data-form="pendidikan"]').length) {
      handleRangeSlider();
      handleInput();
      handleClick();
    }

  }

  return {
    init: init
  }

})();

/****************************** CALL ALL FUNCTION ******************************/
CalculatorPensiun.init();
CalculatorAsuransi.init();
CalculatorKPM.init();
CalculatorKPR.init();
CalculatorPendidikan.init();
