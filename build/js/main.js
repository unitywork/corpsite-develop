(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/* ------------------------------------------------------------------------------
@name: OCBCMain
@description: Initialize all function
--------------------------------------------------------------------------------- */

/* WHATS WRONG WITH THIS ES6 import syntax??? - JOSEPH, somebody help me fix this please T.T


// --- utilities
import {
  Scrolllable,
  BrowserCheck
} from 'utilities';

// --- components
import {
  OCBCWindowScroll,
  OCBCHeaderSearch,
  OCBCHeaderLogin,
  OCBCFooterAccordion,
  OCBCMegaMenu,
  OCBCMobileMenu,
  OCBCMainBanner,
  OCBCPromoBanner,
  OCBCCardHeadProduct,
  OCBCFormSelect,
  OCBCSectionScroll,
  OCBCCardNeeds,
  OCBCAutoSlide,
  OCBCFullPageTab,
  OCBCTaggingTab,
  OCBCVerticalTab,
  OCBCFaqDropdown,
  OCBCMobileTabCurrency,
  OCBCCardNyala,
  OCBCCardNyalaBenefit,
  OCBCCardRecommendation,
  OCBCCardAchievement,
  OCBCCardProductCategory,
  OCBCSliderTestimonial,
  OCBCTabs,
  OCBCFilter,
  OCBCRangeSlider,
  OCBCAboutUs,
  OCBCRachelHowTo,
  OCBCCalcResult,
  OCBCOneMobileScroll,
  OCBCCardFloating,
  OCBCOneMobileBanner,
  OCBCPopupGeneral,
  OCBCFilterTablePopup,
  OCBCCookiesFloating,
  OCBCHowTo,
  OCBCContentDetail,
  OCBCFAQAccordion,
  OCBCPremierService,
  OCBCContactUs,
  OCBCCareerAboutContent,
  OCBCProductComparison,
  OCBCScrollSection
} from 'components';

// --- OCBCMain
const OCBCMain = (() => {
  // --- run transition
  const runTransition = () => {
    $('body').removeClass('ocbc-hold-transition');
  }

  // --- show site content
  const showSiteContent = () => {
    $('.js-site-content').removeClass('ocbc-site-content--hide');
    // --- disable scroll
    Scrolllable.enable();

  }

  // --- ready
  const ready = () => {

    (($) => {

      // --- disable scroll
      Scrolllable.disable();

      // --- image finished loading
      $('body').imagesLoaded(() => {
        // --- Global
        runTransition();
        showSiteContent();
        BrowserCheck.init();

        // --- OCBC Project
        OCBCWindowScroll.init();
        OCBCHeaderSearch.init();
        OCBCHeaderLogin.init();
        OCBCMegaMenu.init();
        OCBCMobileMenu.init();
        OCBCFooterAccordion.init();
        OCBCMainBanner.init();
        OCBCPromoBanner.init();
        OCBCCardHeadProduct.init();
        OCBCFormSelect.init();
        OCBCSectionScroll.init();
        OCBCCardNeeds.init();
        OCBCFullPageTab.init();
        OCBCTaggingTab.init();
        OCBCVerticalTab.init();
        OCBCFaqDropdown.init();
        OCBCMobileTabCurrency.init();
        OCBCCardNyala.init();
        OCBCCardNyalaBenefit.init();
        OCBCCardRecommendation.init();
        OCBCCardAchievement.init();
        OCBCCardProductCategory.init();
        OCBCSliderTestimonial.init();
        OCBCTabs.init();
        OCBCFilter.init();
        OCBCRangeSlider.init();
        OCBCAboutUs.init();
        OCBCCalcResult.init();
        OCBCRachelHowTo.init();
        OCBCOneMobileScroll.init();
        OCBCCardFloating.init();
        OCBCOneMobileBanner.init();
        OCBCPopupGeneral.init();
        OCBCFilterTablePopup.init();
        OCBCCookiesFloating.init();
        OCBCFilterTablePopup.init();
        OCBCHowTo.init();
        OCBCContentDetail.init();
        OCBCFAQAccordion.init();
        OCBCPremierService.init();
        OCBCContactUs.init();
        OCBCCareerAboutContent.init();
        OCBCProductComparison.init();
        OCBCScrollSection.init();
      });

    })(jQuery);

  }

  // --- load
  const load = () => {
    (($) => {
      $(window).on("load", () => {
        OCBCAutoSlide.init();
      });

    })(jQuery);

  }

  // --- init
  const init = () => {
    load();
    ready();
  }

  // --- return
  return {
    init
  }

})();

// ---  run main js
OCBCMain.init();
*/
"use strict";

},{}]},{},[1])

//# sourceMappingURL=maps/main.js.map
