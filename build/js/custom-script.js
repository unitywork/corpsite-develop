/* ------------------------------------------------------------------------------
@name: Custom Script
@description: If want custom script in here
--------------------------------------------------------------------------------- */

// -- REGEX
var WHITESPACE = /^ *$/;
var EMAIL = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
var PHONE_NUMBER = /^(0|\+62)+([0-9]){4,16}/i;
var PERSON_NAME = /^[a-zA-Z][a-zA-Z\-' ]*[a-zA-Z ]$/i;
var NUMBERIC = /[0-9]+$/i;

/**** GLOBAL FUNCTION
 * --------------------------------------------------------------------------------- */
// -- ScrolllableUtil
var ScrolllableUtil = (function () {
  // --- handleEnable
  var handleEnable = function () {
    $('body').removeClass('ocbc-rm-scroll');
    // --- vendor scrollLock for solve (position changed when on hover) in window/mac show scrollbar
    scrollLock.enablePageScroll();
  }

  // --- handleDisable
  var handleDisable = function () {
    if ($(window).width() <= 1200) {
      $('body').addClass('ocbc-rm-scroll');
    } else {
      // --- vendor scrollLock for solve (position changed when on hover) in window/mac show scrollbar
      scrollLock.setFillGapMethod('padding');
      var _fillGapHeader = document.querySelector('.ocbc-header');
      scrollLock.addFillGapTarget(_fillGapHeader);

      var _fillGapMegaMenu = document.querySelector('.ocbc-header__mega-menu');
      scrollLock.addFillGapTarget(_fillGapMegaMenu);

      scrollLock.disablePageScroll();

    }
  }

  // --- return
  return {
    enable: handleEnable,
    disable: handleDisable
  }

})();

// -- handleFormatCurrency
var handleFormatCurrency = function (number, separator) {
  var _numberString = String(number).replace(/[^,\d]/g, '').replace(/,/g, '').toString(),
    _mod = _numberString.length % 3,
    _currency = _numberString.substr(0, _mod),
    _thousand = _numberString.substr(_mod).match(/\d{3}/gi),
    _separator = '';

  // _thousand
  if (_thousand) {
    _separator = _mod ? separator : '';
    _currency += _separator + _thousand.join(separator);
  }
  return _currency;
}

// -- getValueParameter
var getValueParameter = function(param){
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
  sURLVariables = sPageURL.split('&'),
  sParameterName,
  i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === param) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
}

/**** DUMMYSEARCH
temen-temen blend tinggal sesuaikan aja yaa dengan kebutuhan, ini hanya dummy datanya kita simpan di variable supaya cepat
--------------------------------------------------------------------------------- */
var DummySearch = (function () {
  var suggestionList = '',
    searchList = '';

  // --- handleGetData
  var handleGetData = function () {
    var _urlSuggestion = $('.ocbc-search-section__form').attr('data-api-suggestion'),
      _urlSearchList = $('.ocbc-search-section__form').attr('data-api-list');

    // -- call data suggestion
    $.ajax({
      url: _urlSuggestion,
      type: "GET",
      dataType: 'JSON',
      success: function (data) {
        suggestionList = data;
        handleSearchQuickLinks('', 'init');
      },
      error: function () {
        suggestionList = '';
      }
    });

    // -- call data list
    $.ajax({
      url: _urlSearchList,
      type: "GET",
      dataType: 'JSON',
      success: function (data) {
        searchList = data;
      },
      error: function () {
        searchList = '';
      }
    });

  }

  // --- handleClick
  var handleClick = function () {
    // --- hide
    $('.js-header-search-close, body').on('click', function () {
      if ($('body').hasClass('ocbc-search-form--show')) {
        // reinit list
        setTimeout(function () {
          handleSearchQuickLinks('', 'init');
        }, 1000);
      }
    });

  }

  // --- handleInput
  var handleInput = function () {
    // --- focus
    $('.js-search-input').on('focus', function (e) {
      var _this = $(this);
      if (_this.parents('.ocbc-search-section').hasClass('ocbc-search-section--inline')) {
        _this.trigger('keyup');
        $('body').addClass('ocbc-search-form--show-inline');
      }
    });

    // --- blur
    $('.js-search-input').on('blur', function (e) {
      var _this = $(this);
      if (_this.parents('.ocbc-search-section').hasClass('ocbc-search-section--inline')) {
        $('body').removeClass('ocbc-search-form--show-inline');
      }
    });

    // --- keyup
    $('.js-search-input').on('keyup', function (e) {
      var _this = $(this),
        _val = _this.val();

      if (e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40 && e.which != 13) {
        if (!WHITESPACE.test(_val)) {
          handleSearchQuickLinks(_this, 'search');
        } else {
          handleSearchQuickLinks('', 'init');
        }
      }

    });

    // --- keypress
    $('.js-search-input').on('keypress', function (e) {
      var _this = $(this),
        _val = _this.val();

      if (e.which == 13 && _val.length < 3) {
        e.preventDefault();
      }


    });

  }

  // --- handleSearchListScrollbar: function untuk custom scrollbar
  var handleSearchListScrollbar = function () {
    if ($('.js-search-list').length) {
      $('.js-search-list').each(function (idx, val) {
        var _this = $(this),
          _heightList = _this.find('.ocbc-search-section__list').outerHeight() - 4,
          _scrolloverList = _this.outerHeight();
        if (_heightList > _scrolloverList) {
          _this.mCustomScrollbar({
            autoHideScrollbar: false
          });
        } else {
          _this.mCustomScrollbar('destroy');
        }
      });
    }
  }

  // --- handleSearchQuickLinks
  var handleSearchQuickLinks = function (selector, status) {
    var _selector = '';
    if (selector == '') {
      _selector = $('.js-search-input');
    } else {
      _selector = selector;
    }

    var _keyword = _selector.val(),
      _listSearch = '',
      _indexSearch = 0,
      _boxLists = _selector.parents('.ocbc-search-section').find('.js-search-list .ocbc-search-section__list'),
      _boxWrapper = _selector.parents('.ocbc-search-section').find('.ocbc-search-section__quick-links__wrapper'),
      _noResult = _selector.parents('.ocbc-search-section').find('.ocbc-search-section__no-result'),
      _textMatch = _selector.parents('.ocbc-search-section').find('.ocbc-search-section__title').attr('data-text-match'),
      _textShortcut = _selector.parents('.ocbc-search-section').find('.ocbc-search-section__title').attr('data-text-shortcut'),
      _searchData,
      _search,
      _keywordSearch,
      _title;

    if (status == 'search' && _keyword.length > 2) {
      _searchData = searchList;
      _selector.parents('.ocbc-search-section').find('.ocbc-search-section__title').text(_textMatch);
    } else {
      _searchData = suggestionList;
      _selector.parents('.ocbc-search-section').find('.ocbc-search-section__title').text(_textShortcut);
    }

    _boxLists.html('');

    $.each(_searchData, function (index, value) {
      _title = value.title;

      if (status == 'search' && _keyword.length > 2) {
        _title = _title.toLowerCase();
        _search = _title;
        _keywordSearch = _keyword.toLowerCase();
        if (_search.search(_keywordSearch) > -1) {
          _indexSearch += 1;
          _title = _title.replace(new RegExp(_keywordSearch, 'g'), '<span class="ocbc-search-section__highlight">' + _keywordSearch + '</span>');
          _listSearch += '<li class="ocbc-search-section__item"><a class="ocbc-lnk" href="' + value.link + '">' + _title + '</a></li>';
        }
      } else {
        _listSearch += '<li class="ocbc-search-section__item"><a class="ocbc-lnk" href="' + value.link + '">' + _title + '</a></li>';
      }

    });

    if (_listSearch == '') {
      _boxWrapper.hide(0);
      _noResult.show(0);
    } else {
      _noResult.hide(0);
      _boxLists.append(_listSearch);
      _boxWrapper.show(0);
      handleSearchListScrollbar();
    }

  }

  // --- initialize
  var init = function () {
    handleGetData();
    handleClick();
    handleInput();
    handleSearchListScrollbar();
  }

  return {
    init: init
  }

})();


/**** DUMMYSORT
function untuk toggle sort, kita hanya pakai intuk kebutuhan UI saja, untuk kebutuhan data silakan temen-temen blend combine yaa
--------------------------------------------------------------------------------- */
var DummySort = (function () {

  // --- init
  var handleClickToggle = function () {
    $('.js-sort').on('click', function (e) {
      var _this = $(this),
        _newestText = _this.attr('data-text-newest'),
        _oldestText = _this.attr('data-text-oldest'),
        _oldestClass = 'ocbc-sort--oldest';

      if (_this.hasClass(_oldestClass)) {
        _this.removeClass(_oldestClass).find('.ocbc-sort__text').text(_newestText);
      } else {
        _this.addClass(_oldestClass).find('.ocbc-sort__text').text(_oldestText);
      }

    });

  }

  // --- init
  var init = function () {
    if ($('.js-sort')) {
      handleClickToggle();
    }

  }

  // --- return
  return {
    init: init
  }

})();


/**** DUMMYFILTER
function filter untuk checkbox, kita hanya pakai intuk kebutuhan UI saja, untuk kebutuhan data silakan temen - temen blend combine yaa
--------------------------------------------------------------------------------- */
var DummyFilter = (function () {

  // --- handleClick
  var handleClick = function () {
    // --- close button
    $('.js-close-filter').on('click', function () {
      if ($(window).width() < 768) {
        if ($('.ocbc-search-result').length) {
          handleApplyFilter();
        }
      }
    });

    // --- checkbox
    $('.js-filter-checkbox').on('click', function (e) {
      var _this = $(this),
        _dataCheckbox = _this.attr('data-checkbox'),
        _parent = _this.parents('.ocbc-filter__item'),
        _dataFilter = _parent.attr('data-filter'),
        _checkboxLength = _parent.find('.js-filter-checkbox').length - 1,
        _existAll = Boolean(Number(_parent.find('.js-filter-checkbox[data-checkbox="all"]').length));

      // --- mas ricky ini untuk detect dia search filter atau bukan yaa
      if (_dataFilter == 'filter-search') {
        console.log('Yes ini filter search yaa');
      }
      // --- /mas ricky ini untuk detect dia search filter atau bukan yaa

      if (_existAll) {
        // checkbox = all
        if (_dataCheckbox == 'all') {
          if (!_this.is(':checked')) {
            return false
          } else {
            _parent.find('.js-filter-checkbox').prop('checked', false);
            _parent.find('.js-filter-checkbox[data-checkbox="all"]').prop('checked', true);
          }
          // checkbox != all
        } else {
          if (_parent.find('.js-filter-checkbox[data-checkbox="all"]').is(':checked')) {
            _parent.find('.js-filter-checkbox[data-checkbox="all"]').prop('checked', false);
            if ((_parent.find('.js-filter-checkbox:checked').length) == _checkboxLength) {
              _parent.find('.js-filter-checkbox').prop('checked', false);
              _parent.find('.js-filter-checkbox[data-checkbox="all"]').prop('checked', true);
            }
          } else if (_parent.find('.js-filter-checkbox:checked').length == 0) {
            _parent.find('.js-filter-checkbox[data-checkbox="all"]').prop('checked', true);
          } else if ((_parent.find('.js-filter-checkbox:checked').length) == _checkboxLength) {
            _parent.find('.js-filter-checkbox').prop('checked', false);
            _parent.find('.js-filter-checkbox[data-checkbox="all"]').prop('checked', true);
          }
        }
      }

    });

    // --- apply button
    $('.js-apply-filter').on('click', function () {
      if ($(window).width() < 768) {
        if ($('.ocbc-search-result').length) {
          handleApplyFilter();
        }
        $('.ocbc-filter').removeClass('ocbc-filter--show');
        // ini fungsi untuk enable scroll
        ScrolllableUtil.enable();
        // end ini fungsi untuk enable scroll
      }
    });

  }

  // --- handleApplyFilter
  var handleApplyFilter = function () {
    var _checkboxList = $('.ocbc-filter .js-filter-checkbox:checked'),
      _filterItemsElement = $('.ocbc-result-list__filtered'),
      _filterItemsText = '';

    $.each(_checkboxList, function (index, element) {
      var _this = $(element),
        _parent = _this.parents('.ocbc-filter-checkbox'),
        _text = _parent.find('.ocbc-filter-checkbox__label').text();
      if (index < (_checkboxList.length - 1)) {
        _filterItemsText += _text + ', ';
      } else {
        _filterItemsText += _text;
      }
    });

    _filterItemsElement.text(_filterItemsText);
  }


  // --- handleWindowResize
  var handleWindowResize = function () {
    $(window).resize(function () {
      // trigger apply
      $('.js-apply-search-filter').trigger('click');
    });
  }

  // --- init
  var init = function () {
    if ($('.ocbc-filter').length) {
      handleClick();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init: init
  }

})();


/**** DUMMYFORM
function untuk handle validation, kita hanya pakai intuk kebutuhan UI saja, untuk kebutuhan data silakan temen - temen blend combine yaa
--------------------------------------------------------------------------------- */
var DummyForm = (function () {

  // --- handleCheckForm
  var handleCheckForm = function () {
    var _isNotValid = $('.js-form')[0].elements.length - 1;

    $.each($('.js-form')[0].elements, function (index, element) {
      var _element = $(element),
        _parent = _element.parents('.ocbc-field'),
        _dataValidation = _parent.attr('data-validation');

      if (_dataValidation != undefined) {
        if (!_element.hasClass('g-recaptcha-response') && !_element.hasClass('ocbc-input--checkbox')) {
          var _isError = handleCheckValidation(_element.val(), _parent);
          if (!_isError) {
            _isNotValid -= 1;
          }
        } else if (_element.hasClass('ocbc-input--checkbox')) {
          var _isChecked = _element.is(':checked');
          if (_isChecked) {
            _isNotValid -= 1;
          }
        } else if (_element.hasClass('g-recaptcha-response')) {
          if (grecaptcha.getResponse().length) {
            _isNotValid -= 1;
          }
        }
      }
    });

    if (_isNotValid) {
      $('.js-form-submit').prop('disabled', true);
    } else {
      $('.js-form-submit').prop('disabled', false);
    }

  }

  // --- handleInputValidation
  var handleInputValidation = function () {
    $('.js-input-validation .ocbc-input').each(function (index, element) {
      var _inputElement = $(element);

      _inputElement.on('keyup blur', function (e) {
        var _this = $(this),
          _value = _this.val(),
          _parent = _this.parents('.ocbc-field'),
          _isError = handleCheckValidation(_value, _parent);

        handleCheckError(_parent, _isError);
        handleCheckForm();

      });

    });

  }

  // --- handleSelectValidation
  var handleSelectValidation = function () {

    // -- on change
    $('.js-select-city').on('select2:close', function(e) {
      var _this = $(this),
        _value = _this.val(),
        _parent = _this.parents('.ocbc-field'),
        _isError = handleCheckValidation(_value, _parent);

      handleCheckError(_parent, _isError);
      handleCheckForm();

    });

  }

  // --- handleRecaptchaValidation
  var handleRecaptchaValidation = function () {
    $('body').on('click', function () {
      if (!$('div[style*="top: -10000px"]').length) {
        var _this = $('.js-form .js-recaptcha-validation'),
          _errorElement = _this.find('.ocbc-field__error'),
          _googleResponse = grecaptcha.getResponse().length;

        if (!_googleResponse) {
          _errorElement.text(_errorElement.attr('data-required'));
          _this.addClass('ocbc-field--is-error');
        } else {
          _this.removeClass('ocbc-field--is-error');
        }
      }
    }).on('click', '.js-form .js-recaptcha-validation .g-recaptcha > div > div', function () {
      $('.js-form .js-recaptcha-validation').removeClass('ocbc-field--is-error');
      handleCheckForm();
    });
  }

  // --- handleAggreementValidation
  var handleAggreementValidation = function () {
    $('.js-aggreement-validation .ocbc-input').on('change', function (e) {
      var _this = $(this),
        _isChecked = _this.is(':checked'),
        _parent = _this.parents('.ocbc-field'),
        _errorElement = _parent.find('.ocbc-field__error'),
        _isError = !_isChecked;

      if (_isError) {
        _errorElement.text(_errorElement.attr('data-required'));
      }

      handleCheckError(_parent, _isError);
      handleCheckForm();

    });
  }

  // --- handleCheckValidation
  var handleCheckValidation = function (value, parent) {

    var _dataValidation = parent.attr('data-validation').replace(/ /g, '').split(','),
      _errorElement = parent.find('.ocbc-field__error'),
      _isError = false;

    $.each(_dataValidation, function (idx, val) {

      if (val == 'phone-number') {
        if (!PHONE_NUMBER.test(value) && !WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-invalid'));
          _isError = true;
        }
      }

      if (val == 'name') {
        if (!PERSON_NAME.test(value) && !WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-invalid'));
          _isError = true;
        }
      }

      if (val == 'email') {
        if (!EMAIL.test(value) && !WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-invalid'));
          _isError = true;
        }
      }

      if (val == 'required') {
        if (WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-required'));
          _isError = true;
        }
      }

    });
    return _isError;

  }

  // --- handleCheckError
  var handleCheckError = function (element, status) {
    if (status) {
      element.addClass('ocbc-field--is-error');
    } else {
      element.removeClass('ocbc-field--is-error');
    }
  }

  // --- handleFormSubmit
  var handleFormSubmit = function() {
    var _isValid = false;
    $('.js-form').on('submit', function(e){

      // ini hanya simulasi silakan sesuaikan dengan kebutuhan
      if (!_isValid) {

        // show preloader
        $('.ocbc-form-preloader').fadeIn(250);

        // simulasi setelah 5 detik baru ke halaman sukses
        _isValid = true;
        setTimeout(function() {
          $('.js-form').submit();
        }, 5000);

        e.preventDefault();
      }

    });

  }

  // --- handleAllValidation
  var handleAllValidation = function () {
    if ($('.js-form').length) {
      handleCheckForm();
      handleSelectValidation();
      handleInputValidation();
      handleRecaptchaValidation();
      handleAggreementValidation();
      handleFormSubmit();
    }
  }

  // --- init
  var init = function () {
    $(window).on('load', function () {
      handleAllValidation();
    });
  }

  // --- return
  return {
    init: init
  }

})();


/**** DUMMYPopupReport
function filter untuk checkbox, kita hanya pakai intuk kebutuhan UI saja, untuk kebutuhan data silakan temen - temen blend combine yaa
--------------------------------------------------------------------------------- */
var DummyPopupReport = (function () {

  var _urlAPIPrefixReport = 'http://webdev-ocbc.xituz.com/js/data/report-';

  // --- handleShowPopupFileReport
  var handleShowPopupFileReport = function () {
    $('.js-show-popup-file-report').on('click', function (e) {
      var _this = $(this),
        _target = _this.attr('data-target');

      // trigger select
      $('.js-report-select-year').find('option[value='+_target+']').prop('selected', true);
      $('.js-report-select-year').find('select').trigger('change');

      // show popup
      $('body').addClass('ocbc-popup-file-report-show');

      // disable scroll
      ScrolllableUtil.disable();

    });

  }

  // --- handleHidePopupFileReport
  var handleHidePopupFileReport = function () {
    $('.js-hide-popup-file-report').on('click', function (e) {
      $('body').removeClass('ocbc-popup-file-report-show');
      ScrolllableUtil.enable();
    });

  }

  // --- handleTagSection
  var handleTagSection = function (title) {
    var _tagSection = '<div class="ocbc-popup-file-report__section">'+
                        '<h3 class="ocbc-popup-file-report__title">'+title+'</h3>'+
                        '<div class="ocbc-popup-file-report__card">'+
                            '<div class="ocbc-row ocbc-card-file-report__list">';
    return _tagSection;
  }

  // --- handleTagItem
  var handleTagItem = function (data) {
    var _tagItem = '<div class="ocbc-col-xl-4 ocbc-col-md-6 ocbc-col-sm-12 ocbc-card-file-report__item">'+
                      '<div class="ocbc-card ocbc-card--file-report">'+
                          '<div class="ocbc-card__box">'+
                              '<div class="ocbc-card__icon">'+
                                  '<img src="img/icon/' + data.icon + '" alt="' + data.icon + '" />' +
                              '</div>'+
                              '<div class="ocbc-card__txt">'+
                                  '<p class="ocbc-card__title">' + data.title + '</p>' +
                                  '<div class="ocbc-card__btn">'+
                                      '<a class="ocbc-btn-txt-lnk" href="' + data.to + '">Baca</a>' +
                                  '</div>'+
                              '</div>'+
                          '</div>'+
                      '</div>'+
                  '</div>';
    return _tagItem;

  }

  // --- handleChange
  var handleChange = function () {
    $('.js-report-select-year select').on('change', function (e) {
      var _this = $(this),
        _value = _this.val();

      if(!WHITESPACE.test(_value)) {
        // -- call data
        $.ajax({
          url: _urlAPIPrefixReport + _value + '.json',
          type: "GET",
          dataType: 'JSON',
          success: function (data) {
            var _contentAll = '';
            $.each(data, function (index, value) {
              var _contentSection = '';
                _contentSection = handleTagSection(value.title);
              $.each(value.list, function (idx, val) {
                var _item = '';
                  _item = handleTagItem(val);
                _contentSection += _item;
              });
              _contentSection += '</div>'+
                          '</div>'+
                      '</div>';
              _contentAll += _contentSection;
            });

            // Content to HTML
            $('.ocbc-popup-file-report__box').html(_contentAll);

          },
          error: function () {
            alert('Data tidak ada, run di webserver atau ubah URL dummy API');
          }
        });
      }

    });
  }

  // --- init
  var init = function () {
    if ($('.ocbc-popup-file-report').length) {
      handleShowPopupFileReport();
      handleHidePopupFileReport();
      handleChange();
    }
  }

  // --- return
  return {
    init: init
  }
})();

/**** OCBCDummyWidget
function filter untuk checkbox, kita hanya pakai intuk kebutuhan UI saja, untuk kebutuhan data silakan temen - temen blend combine yaa
--------------------------------------------------------------------------------- */
var OCBCDummyWidget = (function () {

  // --- handleClickRefreshCardInfo
  var handleClickRefreshCardInfo = function () {
    $('.js-refresh-dummy-card-info').on('click', function (e) {
      var _this = $(this),
      _overlay = _this.parents('.ocbc-card-info').find('.ocbc-card-info__overlay');
      handleOverlay(_overlay);
    });

  }

  // --- handleClickRefreshXRCalcTable
  var handleClickRefreshXRCalcTable = function () {
    $('.js-dummy-refresh-xr-calc-tbl').on('click', function (e) {
      var _this = $(this),
      _overlay = _this.parents('.ocbc-xr-calc-tbl').find('.ocbc-widget-xr-tbl__overlay');
      handleOverlay(_overlay);
    });

  }

  // --- handleOverlay
  var handleOverlay = function (target) {
    target.fadeIn(350);
    setTimeout(function () {
      target.fadeOut(350);
    }, 2500);

  }

  // --- handleInputCurrency
  var handleInputCurrency = function (target) {
    $('.js-dummy-input-currency').on('keypress', function (e) {
      var _key = parseInt(e.key);
      if (!NUMBERIC.test(_key)) return false;

    });

    $('.js-dummy-input-currency').on('input', function (e) {
      var _this = $(this),
      _value = _this.val(),
      _currency = handleFormatCurrency(_value, ',');
      _this.val(_currency);

    });

    // Currency From
    $('.js-dummy-input-currency-from').on('input', function (e) {
      var _this = $(this),
      _value = _this.val();
      console.log('Currency From: ' + _value);
    });

    // Currency To
    $('.js-dummy-input-currency-to').on('input', function (e) {
      var _this = $(this),
      _value = _this.val();
      console.log('Currency To: ' + _value);
    });

  }

  // --- handleSwitchCurrency
  var handleSwitchCurrency = function (target) {
    $('.js-dummy-switch-currency').on('click', function (e) {
      var _this = $(this),
      _parent = _this.parents('.ocbc-widget-xr-calc'),
      _selectFrom = _parent.find('select[name="currency_from"]'),
      _selectFromVal = _selectFrom.val(),
      _selectTo = _parent.find('select[name="currency_to"]'),
      _selectToVal = _selectTo.val(),
      _inputFrom = $('.js-dummy-input-currency-from'),
      _inputFromVal = _inputFrom.val(),
      _inputTo = $('.js-dummy-input-currency-to'),
      _inputToVal = _inputTo.val();

      _this.toggleClass('ocbc-widget-xr-calc__switch--switched');

      _inputFrom.val(_inputToVal);
      _inputTo.val(_inputFromVal);

      _selectFrom.val(_selectToVal).trigger('change');
      _selectTo.val(_selectFromVal).trigger('change');

    });
  }

  // --- init
  var init = function () {
    handleClickRefreshCardInfo();
    handleClickRefreshXRCalcTable();
    handleInputCurrency();
    handleSwitchCurrency();
  }

  // --- return
  return {
    init
  }

})();


/**** DUMMYFORMSECTION
function untuk handle validation, kita hanya pakai intuk kebutuhan UI saja, untuk kebutuhan data silakan temen - temen blend combine yaa
--------------------------------------------------------------------------------- */
var DummyFormSection = (function () {

  // --- handleCheckForm
  var handleCheckForm = function () {
    var _isNotValid = $('.js-form-section')[0].elements.length - 1;

    $.each($('.js-form-section')[0].elements, function (index, element) {
      var _element = $(element),
        _parent = _element.parents('.ocbc-field'),
        _dataValidation = _parent.attr('data-validation');

      if (_dataValidation != undefined) {
        if (!_element.hasClass('g-recaptcha-response') && !_element.hasClass('ocbc-input--checkbox')) {
          var _isError = handleCheckValidation(_element.val(), _parent);
          if (!_isError) {
            _isNotValid -= 1;
          }
        } else if (_element.hasClass('g-recaptcha-response')) {
          if (grecaptcha.getResponse().length) {
            _isNotValid -= 1;
          }
        }
      }
    });

    if (_isNotValid) {
      $('.js-form-section-submit').prop('disabled', true);
    } else {
      $('.js-form-section-submit').prop('disabled', false);
    }

  }

  // --- handleInputValidation
  var handleInputValidation = function () {
    $('.js-input-validation .ocbc-input').each(function (index, element) {
      var _inputElement = $(element);

      _inputElement.on('keyup blur', function (e) {
        var _this = $(this),
          _value = _this.val(),
          _parent = _this.parents('.ocbc-field'),
          _isError = handleCheckValidation(_value, _parent);

        handleCheckError(_parent, _isError);
        handleCheckForm();

      });

    });

  }


  // --- handleCheckValidation
  var handleCheckValidation = function (value, parent) {

    var _dataValidation = parent.attr('data-validation').replace(/ /g, '').split(','),
      _errorElement = parent.find('.ocbc-field__error'),
      _isError = false;

    $.each(_dataValidation, function (idx, val) {

      if (val == 'phone-number') {
        if (!PHONE_NUMBER.test(value) && !WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-invalid'));
          _isError = true;
        }
      }

      if (val == 'name') {
        if (!PERSON_NAME.test(value) && !WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-invalid'));
          _isError = true;
        }
      }

      if (val == 'email') {
        if (!EMAIL.test(value) && !WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-invalid'));
          _isError = true;
        }
      }

      if (val == 'required') {
        if (WHITESPACE.test(value)) {
          _errorElement.text(_errorElement.attr('data-required'));
          _isError = true;
        }
      }

    });
    return _isError;

  }

  // --- handleCheckError
  var handleCheckError = function (element, status) {
    if (status) {
      element.addClass('ocbc-field--is-error');
    } else {
      element.removeClass('ocbc-field--is-error');
    }
  }

  // --- handleFormSubmit
  var handleFormSubmit = function() {
    var _isValid = false;
    $('.js-form-section').on('submit', function(e){

      // ini hanya simulasi silakan sesuaikan dengan kebutuhan
      if (!_isValid) {

        // show preloader
        $('.ocbc-form-preloader').fadeIn(250);

        // simulasi setelah 5 detik baru ke halaman sukses
        _isValid = true;
        setTimeout(function() {
          $('.js-form-section').submit();
        }, 5000);

        e.preventDefault();
      }

    });

  }

  // --- handleAllValidation
  var handleAllValidation = function () {
    if ($('.js-form-section').length) {
      handleCheckForm();
      handleInputValidation();
      handleRecaptchaValidation();
      handleFormSubmit();
    }
  }

  // --- handleRecaptchaValidation
  var handleRecaptchaValidation = function () {
    $('body').on('click', function () {
      if (!$('div[style*="top: -10000px"]').length) {
        var _this = $('.js-form-section .js-recaptcha-validation'),
          _errorElement = _this.find('.ocbc-field__error'),
          _googleResponse = grecaptcha.getResponse().length;

        if (!_googleResponse) {
          _errorElement.text(_errorElement.attr('data-required'));
          _this.addClass('ocbc-field--is-error');
        } else {
          _this.removeClass('ocbc-field--is-error');
        }
      }
    }).on('click', '.js-form-section .js-recaptcha-validation .g-recaptcha > div > div', function () {
      $('.js-form-section .js-recaptcha-validation').removeClass('ocbc-field--is-error');
      handleCheckForm();
    });
  }

  // --- init
  var init = function () {
    $(window).on('load', function () {
      handleAllValidation();
    });
  }

  // --- return
  return {
    init: init
  }

})();


/**** DUMMYSEARCHLOCATION
temen-temen blend tinggal sesuaikan aja yaa dengan kebutuhan, ini hanya dummy datanya kita simpan di variable supaya cepat
--------------------------------------------------------------------------------- */
var DummySearchLocation = (function () {
  var suggestionList = '',
    searchList = '';

  // --- handleGetData
  var handleGetData = function () {
    var _urlSuggestion = $('.ocbc-location-search-section__form').attr('data-api-suggestion'),
      _urlSearchList = $('.ocbc-location-search-section__form').attr('data-api-list');

    // -- call data suggestion
    $.ajax({
      url: _urlSuggestion,
      type: "GET",
      dataType: 'JSON',
      success: function (data) {
        suggestionList = data;
        handleSearchQuickLinks('', 'init');
      },
      error: function () {
        suggestionList = '';
      }
    });

    // -- call data list
    $.ajax({
      url: _urlSearchList,
      type: "GET",
      dataType: 'JSON',
      success: function (data) {
        searchList = data;
      },
      error: function () {
        searchList = '';
      }
    });

  }

  // --- handleInput
  var handleInput = function () {
    // --- focus
    $('.js-location-search-input').on('focus', function (e) {
      var _this = $(this);
      if (_this.parents('.ocbc-location-search-section').hasClass('ocbc-location-search-section--inline')) {
        _this.trigger('keyup');
        $('body').addClass('ocbc-search-form--show-inline');
      }
    });

    // --- blur
    $('.js-location-search-input').on('blur', function (e) {
      var _this = $(this);
      if (_this.parents('.ocbc-location-search-section').hasClass('ocbc-location-search-section--inline')) {
        $('body').removeClass('ocbc-search-form--show-inline');
      }
    });

    // --- keyup
    $('.js-location-search-input').on('keyup', function (e) {
      var _this = $(this),
        _val = _this.val();

      if (e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40 && e.which != 13) {
        if (!WHITESPACE.test(_val)) {
          handleSearchQuickLinks(_this, 'search');
        } else {
          handleSearchQuickLinks('', 'init');
        }
      }

    });

    // --- keypress
    $('.js-location-search-input').on('keypress', function (e) {
      var _this = $(this),
        _val = _this.val();

      if (e.which == 13 && _val.length < 3) {
        e.preventDefault();
      }


    });

  }

  // --- handleSearchListScrollbar: function untuk custom scrollbar
  var handleSearchListScrollbar = function () {
    if ($('.js-location-search-list').length) {
      $('.js-location-search-list').each(function (idx, val) {
        var _this = $(this),
          _heightList = _this.find('.ocbc-location-search-section__list').outerHeight() - 4,
          _scrolloverList = _this.outerHeight();
        if (_heightList > _scrolloverList) {
          _this.mCustomScrollbar({
            autoHideScrollbar: false
          });
        } else {
          _this.mCustomScrollbar('destroy');
        }
      });
    }
  }

  // --- handleSearchQuickLinks
  var handleSearchQuickLinks = function (selector, status) {
    var _selector = '';
    if (selector == '') {
      _selector = $('.js-location-search-input');
    } else {
      _selector = selector;
    }

    var _keyword = _selector.val(),
      _listSearch = '',
      _indexSearch = 0,
      _boxLists = _selector.parents('.ocbc-location-search-section').find('.js-location-search-list .ocbc-location-search-section__list'),
      _boxWrapper = _selector.parents('.ocbc-location-search-section').find('.ocbc-location-search-section__quick-links__wrapper'),
      _noResult = _selector.parents('.ocbc-location-search-section').find('.ocbc-location-search-section__no-result'),
      _textMatch = _selector.parents('.ocbc-location-search-section').find('.ocbc-location-search-section__title').attr('data-text-match'),
      _textShortcut = _selector.parents('.ocbc-location-search-section').find('.ocbc-location-search-section__title').attr('data-text-shortcut'),
      _searchData,
      _search,
      _keywordSearch,
      _title;

    if (status == 'search' && _keyword.length > 2) {
      _searchData = searchList;
      _selector.parents('.ocbc-location-search-section').find('.ocbc-location-search-section__title').text(_textMatch);
    } else {
      _searchData = suggestionList;
      _selector.parents('.ocbc-location-search-section').find('.ocbc-location-search-section__title').text(_textShortcut);
    }

    _boxLists.html('');

    $.each(_searchData, function (index, value) {
      _title = value.title;

      if (status == 'search' && _keyword.length > 2) {
        _title = _title.toLowerCase();
        _search = _title;
        _keywordSearch = _keyword.toLowerCase();
        if (_search.search(_keywordSearch) > -1) {
          _indexSearch += 1;
          _title = _title.replace(new RegExp(_keywordSearch, 'g'), '<span class="ocbc-location-search-section__highlight">' + _keywordSearch + '</span>');
          _listSearch += '<li class="ocbc-location-search-section__item"><a class="ocbc-lnk" href="' + value.link + '">' + _title + '</a></li>';
        }
      } else {
        _listSearch += '<li class="ocbc-location-search-section__item"><a class="ocbc-lnk" href="' + value.link + '">' + _title + '</a></li>';
      }

    });

    if (_listSearch == '') {
      _boxWrapper.hide(0);
      _noResult.show(0);
    } else {
      _noResult.hide(0);
      _boxLists.append(_listSearch);
      _boxWrapper.show(0);
      handleSearchListScrollbar();
    }

  }

  // --- initialize
  var init = function () {
    handleGetData();
    handleInput();
    handleSearchListScrollbar();
  }

  return {
    init: init
  }

})();


/**** DUMMYTRIGGERFROMPARAMETER
function untuk handle trigger click checkbox filter, ini hanya contoh function tim blend silakan sesuaikan dengan kebutuhan integrasi ya
--------------------------------------------------------------------------------- */
var DummyTriggerFromParameter = (function () {

  // --- handleTriggerFilterArticle
  var handleTriggerFilterArticle = function () {
    var _getCategory = getValueParameter('kategori'),
    _getTheme = getValueParameter('tema');

    // if not undefined trigger click
    if (_getCategory != undefined) {
      $('.js-filter-checkbox[name="'+_getCategory+'"]').trigger('click');
    }

    // if not undefined trigger click
    if (_getTheme != undefined) {
      $('.js-filter-checkbox[name="'+_getTheme+'"]').trigger('click');
    }

  }

  // --- init
  var init = function () {
    handleTriggerFilterArticle();
  }

  // --- return
  return {
    init: init
  }

})();

/**** DUMMYACCEPTCOOKIES
function untuk handle trigger click accept cookies
--------------------------------------------------------------------------------- */
var DummyAcceptCookies = (function () {

  // --- handleClickAccept
  var handleClickAccept = function () {
    $('body').on('click', function() {
    // click accept
    }).on('click', '.js-accept-cookies', 'click', function() {
      var _this = $(this),
      _parents = _this.parents('.ocbc-cookies');

      // hide cookies block
      $('body').removeClass('ocbc-show-cookies');

      setTimeout(function() {
        console.log('Accept Cookies');
        // remove cookies block
        _parents.remove();

      }, 1000);

    });

  }

  // --- init
  var init = function () {
    if ($('.ocbc-cookies').length) {
      handleClickAccept();
    }
  }

  // --- return
  return {
    init: init
  }

})();


/**** DUMMYSEARCHFAQ
temen-temen blend tinggal sesuaikan aja yaa dengan kebutuhan, ini hanya dummy datanya kita simpan di variable supaya cepat
--------------------------------------------------------------------------------- */
var DummySearchFAQ = (function () {
  var suggestionList = '',
    searchList = '';

  // --- handleGetData
  var handleGetData = function () {
    var _urlSuggestion = $('.ocbc-faq-search-section__form').attr('data-api-suggestion'),
      _urlSearchList = $('.ocbc-faq-search-section__form').attr('data-api-list');

    // -- call data suggestion
    $.ajax({
      url: _urlSuggestion,
      type: "GET",
      dataType: 'JSON',
      success: function (data) {
        suggestionList = data;
        handleSearchQuickLinks('', 'init');
      },
      error: function () {
        suggestionList = '';
      }
    });

    // -- call data list
    $.ajax({
      url: _urlSearchList,
      type: "GET",
      dataType: 'JSON',
      success: function (data) {
        searchList = data;
      },
      error: function () {
        searchList = '';
      }
    });

  }

  // --- handleInput
  var handleInput = function () {
    // --- focus
    $('.js-faq-search-input').on('focus', function (e) {
      var _this = $(this);
      if (_this.parents('.ocbc-faq-search-section').hasClass('ocbc-faq-search-section--inline')) {
        _this.trigger('keyup');
        $('body').addClass('ocbc-search-form--show-inline');
      }
    });

    // --- blur
    $('.js-faq-search-input').on('blur', function (e) {
      var _this = $(this);
      if (_this.parents('.ocbc-faq-search-section').hasClass('ocbc-faq-search-section--inline')) {
        $('body').removeClass('ocbc-search-form--show-inline');
      }
    });

    // --- keyup
    $('.js-faq-search-input').on('keyup', function (e) {
      var _this = $(this),
        _val = _this.val();

      if (e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40 && e.which != 13) {
        if (!WHITESPACE.test(_val)) {
          handleSearchQuickLinks(_this, 'search');
        } else {
          handleSearchQuickLinks('', 'init');
        }
      }

    });

    // --- keypress
    $('.js-faq-search-input').on('keypress', function (e) {
      var _this = $(this),
        _val = _this.val();

      if (e.which == 13 && _val.length < 3) {
        e.preventDefault();
      }


    });

  }

  // --- handleSearchListScrollbar: function untuk custom scrollbar
  var handleSearchListScrollbar = function () {
    if ($('.js-faq-search-list').length) {
      $('.js-faq-search-list').each(function (idx, val) {
        var _this = $(this),
          _heightList = _this.find('.ocbc-faq-search-section__list').outerHeight() - 4,
          _scrolloverList = _this.outerHeight();
        if (_heightList > _scrolloverList) {
          _this.mCustomScrollbar({
            autoHideScrollbar: false
          });
        } else {
          _this.mCustomScrollbar('destroy');
        }
      });
    }
  }

  // --- handleSearchQuickLinks
  var handleSearchQuickLinks = function (selector, status) {
    var _selector = '';
    if (selector == '') {
      _selector = $('.js-faq-search-input');
    } else {
      _selector = selector;
    }

    var _keyword = _selector.val(),
      _listSearch = '',
      _indexSearch = 0,
      _boxLists = _selector.parents('.ocbc-faq-search-section').find('.js-faq-search-list .ocbc-faq-search-section__list'),
      _boxWrapper = _selector.parents('.ocbc-faq-search-section').find('.ocbc-faq-search-section__quick-links__wrapper'),
      _noResult = _selector.parents('.ocbc-faq-search-section').find('.ocbc-faq-search-section__no-result'),
      _textMatch = _selector.parents('.ocbc-faq-search-section').find('.ocbc-faq-search-section__title').attr('data-text-match'),
      _textShortcut = _selector.parents('.ocbc-faq-search-section').find('.ocbc-faq-search-section__title').attr('data-text-shortcut'),
      _searchData,
      _search,
      _keywordSearch,
      _title;

    if (status == 'search' && _keyword.length > 2) {
      _searchData = searchList;
      _selector.parents('.ocbc-faq-search-section').find('.ocbc-faq-search-section__title').text(_textMatch);
    } else {
      _searchData = suggestionList;
      _selector.parents('.ocbc-faq-search-section').find('.ocbc-faq-search-section__title').text(_textShortcut);
    }

    _boxLists.html('');

    $.each(_searchData, function (index, value) {
      _title = value.title;

      if (status == 'search' && _keyword.length > 2) {
        _title = _title.toLowerCase();
        _search = _title;
        _keywordSearch = _keyword.toLowerCase();
        if (_search.search(_keywordSearch) > -1) {
          _indexSearch += 1;
          _title = _title.replace(new RegExp(_keywordSearch, 'g'), '<span class="ocbc-faq-search-section__highlight">' + _keywordSearch + '</span>');
          _listSearch += '<li class="ocbc-faq-search-section__item"><a class="ocbc-lnk" href="' + value.link + '">' + _title + '</a></li>';
        }
      } else {
        _listSearch += '<li class="ocbc-faq-search-section__item"><a class="ocbc-lnk" href="' + value.link + '">' + _title + '</a></li>';
      }

    });

    if (_listSearch == '') {
      _boxWrapper.hide(0);
      _noResult.show(0);
    } else {
      _noResult.hide(0);
      _boxLists.append(_listSearch);
      _boxWrapper.show(0);
      handleSearchListScrollbar();
    }

  }

  // --- initialize
  var init = function () {
    handleGetData();
    handleInput();
    handleSearchListScrollbar();
  }

  return {
    init: init
  }

})();


/****************************** CALL ALL FUNCTION ******************************/
DummySearch.init();
DummySort.init();
DummyFilter.init();
DummyForm.init();
DummyPopupReport.init();
OCBCDummyWidget.init();
DummyFormSection.init();
DummySearchLocation.init();
DummyTriggerFromParameter.init();
DummyAcceptCookies.init();
DummySearchFAQ.init();
