/* Gulp Core
--------------------------------------------------- */
import {
  series,
  parallel,
  src,
  dest,
  watch
} from "gulp";

/* Plugins
--------------------------------------------------- */
// colors
import c from "ansi-colors";

// server
import browserSync from "browser-sync";

// error handling
import notify from "gulp-notify";
import errorHandle from "gulp-error-handle";

// pug
import pug from "gulp-pug";
import htmlBeautify from "gulp-html-beautify";

// style
import sass from "gulp-sass";
import postcss from "gulp-postcss";
import postcssScss from "postcss-scss";
import autoprefixer from "autoprefixer";

// optimize style
import stripCssComments from "gulp-strip-css-comments";
import combineMq from "gulp-combine-mq";
import cleanCSS from "gulp-clean-css";

// js
import babelify from "babelify";
import browserify from "browserify";
import source from "vinyl-source-stream";
import buffer from "vinyl-buffer";
import watchify from "watchify";
import concat from "gulp-concat";

// optimize js
import uglify from "gulp-uglify";

// optimize image
import imagemin from "gulp-imagemin";
import imageminPngquant from "imagemin-pngquant";
import imageminMozjpeg from "imagemin-mozjpeg";
import imageminGiflossy from "imagemin-giflossy";

// other
import rename from "gulp-rename";
import sourcemaps from "gulp-sourcemaps";
import changed from "gulp-changed";
import del from "del";

// custom OCBC project
import deleteFile from "gulp-delete-file";
import multiDest from "gulp-multi-dest";


/* ------------------------------------------------------------------------------
@name: Config Directory
@description: Config Directory for SRC (development) and BUILD (production)
--------------------------------------------------------------------------------- */
// root
const SRC = "src";
const BUILD = "build";
const fileName = 'main';

const optionsHTML = {
  indent_size: 4,
  indent_char: ' ',
  eol: '\n',
  end_with_newline: true
};

// dev
const DEV_PATH = {
  view: {
    watch: `${SRC}/pug/**/*.pug`,
    pages: `${SRC}/pug/pages/*.pug`,
  },
  style: `${SRC}/scss/**/*.scss`,
  script: {
    vendor: {
      order: [`${SRC}/scripts/vendors/jquery-3.4.1.js`, `${SRC}/scripts/vendors/*.js`],
      concat: 'vendor.js'
    },
    main: `${SRC}/scripts/${fileName}.js`
  },
  image: `${SRC}/images/**/*`,
  fonts: `${SRC}/fonts/*`,
  others: {
    forbidden: `${SRC}/others/html/403.html`,
    data: `${SRC}/others/data/*.json`,
    customStyle: `${SRC}/others/css/*.css`,
    webChat: `${SRC}/others/webchat/**/**/*`,
    dummies: `${SRC}/others/dummy/index.php`,
    customScript: `${SRC}/others/js/*.js`,
    designSystem: {
      css: `${SRC}/others/design-system/css/min/*.css`,
      js: `${SRC}/others/design-system/js/min/*.js`
    }
  },
  videos: `${SRC}/videos/*`,
};

// build
const BUILD_PATH = {
  view: `${BUILD}`,
  style: {
    dir: `${BUILD}/css`,
    main: `${BUILD}/css/${fileName}.min.css`
  },
  script: {
    dir: `${BUILD}/js`,
    main: `${BUILD}/js/${fileName}.min.js`
  },
  image: `${BUILD}/img`,
  fonts: `${BUILD}/fonts`,
  videos: `${BUILD}/videos`,
  // custom OCBC project
  maps: [
    `${BUILD}/css/maps`,
    `${BUILD}/js/maps`
  ],
  // custom OCBC project
  readMe: `${BUILD}/**/**/*.md`,
  webChat: `${BUILD}/webchat`,
  dummies: `${BUILD}`,
  others: {
    forbidden: [
      `${BUILD}/css`,
      `${BUILD}/css/design-system`,
      `${BUILD}/fonts`,
      `${BUILD}/img`,
      `${BUILD}/img/badge`,
      `${BUILD}/img/bg`,
      `${BUILD}/img/default`,
      `${BUILD}/img/dummy`,
      `${BUILD}/img/dummy/banner`,
      `${BUILD}/img/dummy/banner/achievement`,
      `${BUILD}/img/dummy/banner/error`,
      `${BUILD}/img/dummy/banner/how-to`,
      `${BUILD}/img/dummy/banner/main`,
      `${BUILD}/img/dummy/banner/one-mobile`,
      `${BUILD}/img/dummy/banner/product-category`,
      `${BUILD}/img/dummy/banner/product-detail`,
      `${BUILD}/img/dummy/banner/promo`,
      `${BUILD}/img/dummy/banner/search-result`,
      `${BUILD}/img/dummy/banner/section`,
      `${BUILD}/img/dummy/banner/success`,
      `${BUILD}/img/dummy/banner/tentang-kami`,
      `${BUILD}/img/dummy/calculator`,
      `${BUILD}/img/dummy/card`,
      `${BUILD}/img/dummy/card/advisor`,
      `${BUILD}/img/dummy/card/apply`,
      `${BUILD}/img/dummy/card/article`,
      `${BUILD}/img/dummy/card/article-program`,
      `${BUILD}/img/dummy/card/bod`,
      `${BUILD}/img/dummy/card/chosen-product`,
      `${BUILD}/img/dummy/card/head-product`,
      `${BUILD}/img/dummy/card/needs`,
      `${BUILD}/img/dummy/card/nyala`,
      `${BUILD}/img/dummy/card/partner`,
      `${BUILD}/img/dummy/card/premier-product`,
      `${BUILD}/img/dummy/card/product-category`,
      `${BUILD}/img/dummy/card/product-comparison`,
      `${BUILD}/img/dummy/card/product-sme`,
      `${BUILD}/img/dummy/card/promo`,
      `${BUILD}/img/dummy/card/upselling`,
      `${BUILD}/img/dummy/fragment`,
      `${BUILD}/img/dummy/fragment/csr-description`,
      `${BUILD}/img/dummy/fragment/csr-program`,
      `${BUILD}/img/dummy/fragment/hubungi-kami`,
      `${BUILD}/img/dummy/fragment/image-circle`,
      `${BUILD}/img/dummy/fragment/informasi-saham-obligasi`,
      `${BUILD}/img/dummy/fragment/internet-banking-how-to`,
      `${BUILD}/img/dummy/fragment/jadi-nasabah-how-to`,
      `${BUILD}/img/dummy/fragment/kepemilikan-saham`,
      `${BUILD}/img/dummy/fragment/layanan-premier`,
      `${BUILD}/img/dummy/fragment/networks`,
      `${BUILD}/img/dummy/fragment/one-mobile-how-to`,
      `${BUILD}/img/dummy/fragment/pelaporan-whistleblowing`,
      `${BUILD}/img/dummy/fragment/rachel`,
      `${BUILD}/img/dummy/fragment/rachel-how-to`,
      `${BUILD}/img/dummy/fragment/section-image-description`,
      `${BUILD}/img/dummy/fragment/struktur-organisasi`,
      `${BUILD}/img/dummy/fragment/tax-amnesty-section`,
      `${BUILD}/img/dummy/fragment/wealth-management`,
      `${BUILD}/img/dummy/how-to`,
      `${BUILD}/img/dummy/mega-menu`,
      `${BUILD}/img/dummy/one-mobile`,
      `${BUILD}/img/dummy/page`,
      `${BUILD}/img/dummy/page/detail`,
      `${BUILD}/img/dummy/page/karir`,
      `${BUILD}/img/dummy/page/one-mobile`,
      `${BUILD}/img/dummy/partners`,
      `${BUILD}/img/dummy/placeholder`,
      `${BUILD}/img/dummy/popup`,
      `${BUILD}/img/dummy/product-section`,
      `${BUILD}/img/dummy/testimonials`,
      `${BUILD}/img/ds`,
      `${BUILD}/img/flag`,
      `${BUILD}/img/homescreen`,
      `${BUILD}/img/icon`,
      `${BUILD}/img/logo`,
      `${BUILD}/img/regulator`,
      `${BUILD}/js`,
      `${BUILD}/js/data`,
      `${BUILD}/js/design-system`,
      `${BUILD}/videos`,
      `${BUILD}/webchat`,
      `${BUILD}/webchat/css`,
      `${BUILD}/webchat/fonts`,
      `${BUILD}/webchat/img`,
      `${BUILD}/webchat/js`,
      `${BUILD}/webchat/js/cipher`,
      `${BUILD}/webchat/js/cryptojs`
    ],
    data: `${BUILD}/js/data`,
    designSystem: {
      css: `${BUILD}/css/design-system`,
      js: `${BUILD}/js/design-system`
    },
    dummies: `${BUILD}`
  }
};

// colors
const COLORS = {
  error: 'red',
  success: 'green',
  build: {
    name: 'magenta',
    size: 'cyan'
  }
};

// renameOptions
const renameOptions = {
  suffix: '.min'
};


/* ------------------------------------------------------------------------------
@name: cleanBuild
@description delete build folder
--------------------------------------------------------------------------------- */
export const cleanBuild = () => {
  return del(BUILD, {
    force: true
  }).then(() => {
    console.log(c[COLORS.success].bold('--------- Build cleaned! ---------'));
  });
};


/* ------------------------------------------------------------------------------
@name: cleanMaps
@description delete maps directory
--------------------------------------------------------------------------------- */
export const cleanMaps = () => {
  return del(BUILD_PATH.maps, {
    force: true
  }).then(() => {
    console.log(c[COLORS.success].bold('--------- Maps cleaned! ---------'));
  });
};


/* ------------------------------------------------------------------------------
@name: cleanMd
@description delete md files
--------------------------------------------------------------------------------- */
export const cleanMd = () => {
  return src(BUILD_PATH.readMe)
    .pipe(deleteFile({
      reg: '/([/|.|\w|\s|-])*\.(?:md)/g',
      deleteMatch: false
    }))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Md Files cleaned! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: Clean Temporary Task
@description: Clean maps directory and md files
--------------------------------------------------------------------------------- */
export const cleanTemporary = parallel(cleanMaps, cleanMd);


/* ------------------------------------------------------------------------------
@name: Server
@description: Config Server, Reload and devServer browserSync
--------------------------------------------------------------------------------- */
const server = browserSync.create();

const reload = done => {
  server.reload();
  done();
};

export const devServer = done => {
  server.init({
    ghostMode: true,
    notify: false,
    server: {
      baseDir: BUILD_PATH.view,
    },
    port: 80,
    open: true
  });
  done();
};


/* ------------------------------------------------------------------------------
@name: CompilePug
@description: Compiles Pug files to HTML
--------------------------------------------------------------------------------- */
export const compilePug = () => {
  return src(DEV_PATH.view.pages)
    .pipe(pug())
    .on("error", notify.onError(
      (err) => {
        return '\nProblem file : ' + c[COLORS.error].bold(err.message, err.path);
      }
    ))
    .pipe(htmlBeautify(optionsHTML))
    .pipe(dest(BUILD_PATH.view))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Pug finished compiling! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: compileStyle
@description: Compiles Pug files to HTML
--------------------------------------------------------------------------------- */
export const compileStyle = () => {
  return src(DEV_PATH.style)
    .pipe(errorHandle())
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", notify.onError(
      (err) => {
        return '\nProblem file : ' + c[COLORS.error].bold(err.message, err.path);
      }
    )))
    .pipe(postcss([autoprefixer()], {
      syntax: postcssScss
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(dest(BUILD_PATH.style.dir))
    .pipe(rename(renameOptions))
    .pipe(dest(BUILD_PATH.style.dir))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Style finished compiling! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: compileJS
@description: Compiles ES6 files to ES5 (javascripts)
--------------------------------------------------------------------------------- */
const bundle = bundler => {
  return bundler
    .transform(babelify, {
      presets: ["es2015"]
    })
    .bundle()
    .on("error", notify.onError(
      (err) => {
        return '\nProblem file : ' + c[COLORS.error].bold(err.message, err.path);
      }
    ))
    .pipe(source(`${fileName}.js`))
    .pipe(buffer())
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(sourcemaps.write("./maps"))
    .pipe(dest(BUILD_PATH.script.dir))
    .pipe(rename(renameOptions))
    .pipe(dest(BUILD_PATH.script.dir))
    .on('end', (done) => {
      console.log(c[COLORS.success].bold('--------- JS finished compiling! ---------'));
    });
};

export const compileJS = () => {
  return bundle(browserify(DEV_PATH.script.main, {
    debug: true
  }));
};


/* ------------------------------------------------------------------------------
@name: compressImage
@description: Optimize image
--------------------------------------------------------------------------------- */
export const compressImage = () => {
  return src(DEV_PATH.image)
    .pipe(changed(BUILD_PATH.image))
    .pipe(imagemin([
      // jpg
      imageminMozjpeg({
        quality: 76.75
      }),
      // png
      imageminPngquant({
        speed: 1,
        quality: [0.225, 0.425]
      }),
      // svg
      imagemin.svgo({
        plugins: [{
            removeViewBox: true
          },
          {
            cleanupIDs: false
          }
        ]
      }),
      // gif
      imageminGiflossy({
        optimizationLevel: 3,
        optimize: 3,
        lossy: 2
      })
    ]))
    .pipe(dest(BUILD_PATH.image))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Image finished compressing! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: copyVendorJS
@description: Copy files to production directory
--------------------------------------------------------------------------------- */
export const copyVendorJS = () => {
  return src(DEV_PATH.script.vendor.order)
    .pipe(concat(DEV_PATH.script.vendor.concat))
    .pipe(dest(BUILD_PATH.script.dir))
    .pipe(rename(renameOptions))
    .pipe(uglify({
      mangle: false
    }))
    .pipe(dest(BUILD_PATH.script.dir))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Vendor JS finished copying! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: copyFonts
@description: Copy files to production directory
--------------------------------------------------------------------------------- */
export const copyFonts = () => {
  return src(DEV_PATH.fonts)
    .pipe(changed(BUILD_PATH.fonts))
    .pipe(dest(BUILD_PATH.fonts))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Fonts finished copying! ---------'));
    });
};

/* ------------------------------------------------------------------------------
@name: copyVideos
@description: Copy files to production directory
--------------------------------------------------------------------------------- */
export const copyVideos = () => {
  return src(DEV_PATH.videos)
    .pipe(changed(BUILD_PATH.videos))
    .pipe(dest(BUILD_PATH.videos))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Videos finished copying! ---------'));
    });
};

/* ------------------------------------------------------------------------------
@name: copyOthers
@description: Copy files to production directory
--------------------------------------------------------------------------------- */
export const copyData = () => {
  return src(DEV_PATH.others.data)
    .pipe(dest(BUILD_PATH.others.data))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Data finished copying! ---------'));
    });
};

export const copyForbidden = () => {
  return src(DEV_PATH.others.forbidden)
    .pipe(rename('index.html'))
    .pipe(multiDest(BUILD_PATH.others.forbidden))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Forbidden finished copying! ---------'));
    });
};

export const copyCustomStyle = () => {
  return src(DEV_PATH.others.customStyle)
    .pipe(dest(BUILD_PATH.style.dir))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Custom Style finished copying! ---------'));
    });
};

export const copyCustomScript = () => {
  return src(DEV_PATH.others.customScript)
    .pipe(dest(BUILD_PATH.script.dir))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Custom Script finished copying! ---------'));
    });
};

export const copyCSSDesignSystem = () => {
  return src(DEV_PATH.others.designSystem.css)
    .pipe(dest(BUILD_PATH.others.designSystem.css))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- CSS Design System finished copying! ---------'));
    });
};

export const copyJSDesignSystem = () => {
  return src(DEV_PATH.others.designSystem.js)
    .pipe(dest(BUILD_PATH.others.designSystem.js))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- JS Design System finished copying! ---------'));
    });
};

export const copyWebChat = () => {
  return src(DEV_PATH.others.webChat)
    .pipe(dest(BUILD_PATH.webChat))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Web Chat finished copying! ---------'));
    });
};

export const copyDummies = () => {
  return src(DEV_PATH.others.dummies)
    .pipe(dest(BUILD_PATH.dummies))
    .pipe(server.stream())
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Web Chat finished copying! ---------'));
    });
};

export const copyOthers = parallel(
  copyData,
  copyForbidden,
  copyCSSDesignSystem,
  copyJSDesignSystem,
  copyCustomStyle,
  copyCustomScript,
  copyWebChat,
  copyDummies
);


/* ------------------------------------------------------------------------------
@name: Task Watch
@description: Watches for changes to files
--------------------------------------------------------------------------------- */
export const devWatch = (done) => {
  // Shows that run "watch" mode
  global.watch = true;

  // PUG
  watch(DEV_PATH.view.watch, series(compilePug));

  // SCSS
  watch(DEV_PATH.style, series(compileStyle));

  // JS
  watchify.args.debug = true;
  const watcher = watchify(browserify(DEV_PATH.script.main, watchify.args));
  bundle(watcher);
  watcher.on("update", () => {
    bundle(watcher);
    server.reload();
  });

  // Image
  watch(DEV_PATH.image, series(compressImage));

  // Custom Script
  watch(DEV_PATH.others.customScript, series(copyCustomScript));

  // Custom Style
  watch(DEV_PATH.others.customStyle, series(copyCustomStyle));

  // Fonts
  watch(DEV_PATH.fonts, series(copyFonts));

  // Videos
  watch(DEV_PATH.videos, series(copyVideos));

  // Web Chat
  watch(DEV_PATH.others.webChat, series(copyWebChat));

  watch(DEV_PATH.others.dummies, series(copyDummies));

  return done();

};


/* ------------------------------------------------------------------------------
@name: Default Task
@description: Default Task will be run at run gulp
--------------------------------------------------------------------------------- */
exports.default = series(
  compressImage,
  parallel(cleanMd, copyVendorJS, copyFonts, copyVideos),
  parallel(compileStyle, compileJS),
  copyOthers,
  compilePug,
  devServer,
  devWatch
);



/* ===============================================================================
---------------------------- PRODUCTION TASK -------------------------------------
================================================================================== */


/* ------------------------------------------------------------------------------
@name: optimizeStyle
@description: Optimize style
--------------------------------------------------------------------------------- */
export const optimizeStyle = () => {
  return src(BUILD_PATH.style.main)
    .pipe(stripCssComments({
      preserve: false
    }))
    .pipe(cleanCSS({
      debug: true
    }, (details) => {
      console.log(
        c[COLORS.success].bold('--------- Original Size ') +
        c[COLORS.build.name].bold(`(${details.name}) `) +
        c[COLORS.build.size].bold(details.stats.originalSize) +
        c[COLORS.success].bold(' ---------')
      );
      console.log(
        c[COLORS.success].bold('--------- Minified Size ') +
        c[COLORS.build.name].bold(`(${details.name}) `) +
        c[COLORS.build.size].bold(details.stats.minifiedSize) +
        c[COLORS.success].bold(' ---------')
      );
    }))
    .pipe(combineMq({
      beautify: false
    }))
    .pipe(dest(BUILD_PATH.style.dir))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- Style finished optimizing! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: optimizeJS
@description: Optimize JS
--------------------------------------------------------------------------------- */
export const optimizeJS = () => {
  return src(BUILD_PATH.script.main)
    .pipe(uglify({
      mangle: false
    }))
    .pipe(dest(BUILD_PATH.script.dir))
    .on('end', () => {
      console.log(c[COLORS.success].bold('--------- JS finished optimizing! ---------'));
    });
};


/* ------------------------------------------------------------------------------
@name: Build Task
@description: Build Task will be run at run gulp
--------------------------------------------------------------------------------- */
exports.build = series(
  cleanBuild,
  compressImage,
  parallel(copyVendorJS, copyFonts, copyVideos),
  parallel(
    series(compileStyle, optimizeStyle),
    series(compileJS, optimizeJS)
  ),
  copyOthers,
  compilePug,
  cleanTemporary
);
