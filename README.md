# OCBC PUBLIC SITE
------

### Pre-requisite
* NodeJS
* NPM
* Yarn
* Gulp 4 (installed globally or locally)

### How To
* run `yarn install` or `npm install`
* open terminal and run `gulp` or run `yarn start` or run `npm start`. This will compile PUG, SCSS, JS, Images, and fonts files to build folder and start the dev server.
* for build the project run `gulp build` or run `yarn build` or run `npm build`.

### Folder Structures
```
PROJECT_FOLDER
├-- build/
|   ├-- css/
|   |   ├-- index.html
|   |   └-- *.css
|   ├-- fonts/
|   |   ├-- index.html
|   |   ├-- *.ttf
|   |   ├-- *.otf
|   |   ├-- *.woff
|   |   └-- *.woff2
|   ├-- img/
|   |   ├-- logo/
|   |   |   ├-- *.*
|   |   |   └-- index.html
|   |   ├-- icon/
|   |   |   ├-- *.*
|   |   |   └-- index.html
|   |   ├-- default/
|   |   |   ├-- *.*
|   |   |   └-- index.html
|   |   ├-- banner/
|   |   |   ├-- *.*
|   |   |   └-- index.html
|   |   ├-- dummy/
|   |   |   ├-- *.*
|   |   |   └-- index.html
|   |   └-- index.html
|   ├-- js/
|   |   ├-- index.html
|   |   └-- *.js
|   ├-- index.html
|   ├-- 403.html
|   ├-- 404.html
|   ├-- 500.html
|   └-- *.html
├-- src/
|   ├-- fonts/
|   ├-- images/
|   ├-- pug/
|   |   ├-- components/
|   |   |   └-- *.pug
|   |   ├-- config/
|   |   ├-- pages/
|   |   |   ├-- 404.pug
|   |   |   ├-- 403.pug
|   |   |   ├-- 500.pug
|   |   |   └-- *.pug
|   |   └-- templates/
|   |       └-- *.pug
|   ├-- scss/
|   |   ├-- base/
|   |   ├-- helper/
|   |   ├-- project/
|   |   |   ├-- init/
|   |   |   ├-- elements/
|   |   |   ├-- components/
|   |   |   ├-- layouts/
|   |   |   └-- pages/
|   |   ├-- temp/
|   |   ├-- vendor/
|   |   └-- main.scss
|   └-- scripts/
|       ├-- components/
|       ├-- dummies/
|       ├-- pages/
|       ├-- services/
|       ├-- utilities/
|       ├-- variables/
|       └-- vendors/
├-- .babelrc
├-- .browserslistrc
├-- .gitignore
├-- README.md
├-- bitbucket-pipelines.yml
├-- gulpfile.babel.js
└-- package.json
```

### Others
* [Best Sitecore Practises for Front-End Developers](http://www.digital.voyage/2014/03/16/best-sitecore-practises-for-front-end-developers/)
