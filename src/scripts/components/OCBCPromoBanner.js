/* ------------------------------------------------------------------------------
@name: OCBC Promo Banner
@description: OCBC Promo Banner
--------------------------------------------------------------------------------- */

// --- OCBCPromoBanner
const OCBCPromoBanner = (() => {

  // selector
  const _selector = $('.js-promo-banner');
  const _itemLength = $('.js-promo-banner .ocbc-promo-banner__item').length;

  // --- itemsCheck
  const itemsCheck = () => {
    // --- run carousel
    if (_itemLength >= 2) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      // --- set carousel
      /*
        timeout = number - 1050 (timer delay)
        if  timeout = 8050
        then real timeout = 7000
       */
      _selector.owlCarousel({
        loop: true,
        items: 1,
        mouseDrag: false,
        touchDrag: true,
        pullDrag: true,
        nav: true,
        rewind: false,
        dots: false,
        autoHeight: true,
        autoplay: true,
        autoplayTimeout: 8050,
        autoplayHoverPause: true,
        autoplaySpeed: 750,
        navSpeed: 750,
        dragEndSpeed: 750,
        responsive: {
          0: {
            nav: false
          },
          768: {
            nav: true
          }
        }
      });

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');
      setTimeout(() => {
        _selector.addClass('ocbc-promo-banner--show');
      }, 50);

    }

  }

  // --- init
  const init = () => {
    if ($('.js-promo-banner').length) {
      itemsCheck();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCPromoBanner;
