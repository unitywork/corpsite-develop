/* ------------------------------------------------------------------------------
@name: OCBC Karir About Content
@description: OCBC Karir About Content
--------------------------------------------------------------------------------- */

// --- OCBCCareerAboutContent
const OCBCCareerAboutContent = (() => {

  // handleClickedTab
  const handleClickedTab = () => {
    $('.js-career-about-content').on('click', (e) => {
      const _this = $(e.currentTarget);
      $('.ocbc-vertical-tab--about').show(0).addClass('ocbc-vertical-tab--about__active');
      $('.ocbc-vertical-tab--about').slideDown(350);
    });
  }

  // --- init
  const init = () => {
    if ($('.js-career-about-content').length) {
      handleClickedTab();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCCareerAboutContent;
