/* ------------------------------------------------------------------------------
@name: OCBC Auto Slide
@description: OCBC Auto Slide Images on Suggestion Card
--------------------------------------------------------------------------------- */

// --- OCBCAutoSlide
const OCBCAutoSlide = (() => {

  let _htmlSlide = '';

  // --- handleEndlessAnimation
  const handleEndlessAnimation = (targetElement, totalDuration, initialPositionE, endPositionE, trigger) => {
    $(targetElement).css({
      left: initialPositionE
    });
    $(targetElement).animate({
      'left': endPositionE
    }, {
      duration: totalDuration,
      easing: 'linear',
      complete: () => {
        const clone = $(targetElement).clone();
        $(targetElement).last().after(clone);
        if (trigger === 1) {
          handleEndlessAnimation(clone, totalDuration * 1.5, initialPositionE * 2, endPositionE);
        } else if (trigger === 3) {
          handleEndlessAnimation(clone, (totalDuration / 2) * 1.5, (initialPositionE / 3) * 2, endPositionE);
        } else {
          handleEndlessAnimation(clone, totalDuration, initialPositionE, endPositionE);
        }
        $(targetElement).first().remove();
      }
    }, );
  };

  // --- handleInitialAnimation
  const handleInitialAnimation = (location, speed) => {
    const _targetElement = $(`.js-auto-slide-${location}`);
    const _circleWrapperWidth = $(`.ocbc-card__${location}`).width();

    const _initialPositionE = _circleWrapperWidth;
    const _endPositionE = _initialPositionE * -1;

    const _initialPositionI = 0;
    const _endPositionI = _endPositionE;

    const _totalDuration = _circleWrapperWidth / (speed / 100);

    $(_targetElement).css({
      left: _initialPositionI
    });
    $(_targetElement).animate({
      'left': _endPositionI
    }, {
      duration: _totalDuration / 2,
      easing: 'linear',
      start: () => {
        const _clone1 = $(_targetElement).clone();
        const _clone2 = $(_targetElement).clone();
        const _clone3 = $(_targetElement).clone();

        $(_targetElement).last().after(_clone1);
        $(_targetElement).last().after(_clone2);
        $(_targetElement).last().after(_clone3);

        handleEndlessAnimation(_clone1, _totalDuration, _initialPositionE, _endPositionE, 1);
        handleEndlessAnimation(_clone2, _totalDuration * 1.5, _initialPositionE * 2, _endPositionE);
        handleEndlessAnimation(_clone3, _totalDuration * 2, _initialPositionE * 3, _endPositionE, 3);
      },
      complete: () => {
        $(_targetElement).first().remove();
      }
    }, );
  };

  // --- handleEndlessLoop
  const handleEndlessLoop = () => {
    let _speed = 1;
    if ($(window).width() < 768) {
      _speed = 0.35;
    }

    handleInitialAnimation('top', _speed);
    handleInitialAnimation('bottom', _speed);

  }

  // --- handleRestructure
  const handleRestructure = () => {
    const _getChild = $('.js-auto-slide').children();
    $('.js-auto-slide').html('<div class="ocbc-card__top js-auto-slide-top"></div><div class="ocbc-card__bottom js-auto-slide-bottom"></div>');
    $.each(_getChild, (index, value) => {
      if (index < (_getChild.length / 2)) {
        $('.js-auto-slide').find('.ocbc-card__top').append(value);
      } else {
        $('.js-auto-slide').find('.ocbc-card__bottom').append(value);
      }
    });

    _htmlSlide = $('.js-auto-slide').html();

    if ($('.ocbc-ds').length) {
      handleEndlessLoop();
    }

  }

  // --- handleWindow
  const handleWindow = () => {
    $(window).focus(() => {
      handleRun();
    });
  }

  // --- handleRun
  const handleRun = () => {
    $('.js-auto-slide').html(_htmlSlide);
    handleEndlessLoop();
  }

  // --- handleDestroy
  const handleDestroy = () => {
    $('.js-auto-slide').html('');
  }

  // --- init
  const init = () => {
    if ($('.ocbc-ds').length) {
      setTimeout(() => {
        handleRestructure();
      }, 5500);
      handleWindow();
    } else {
      handleRestructure();
    }
  }

  // --- return
  return {
    run: handleRun,
    destroy: handleDestroy,
    init
  }

})();

export default OCBCAutoSlide;
