/* ------------------------------------------------------------------------------
@name: OCBC Filter Table Popup
@description: OCBC Filter Table Popup
--------------------------------------------------------------------------------- */

// --- OCBCFilterTablePopup
const OCBCFilterTablePopup = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // handleChange
  const handleChange = () => {
    $('body').on('click', () => {
      // --- click item
    }).on('change', '.js-filter-table-popup select', (e) => {
      const _this = $(e.currentTarget);
      const _value = _this.val();

      $('.ocbc-popup-general__tbl-select__item.ocbc-popup-general__tbl-select__item--active').fadeOut(200);
      setTimeout(() => {
        $('.ocbc-popup-general__tbl-select__item').removeClass('ocbc-popup-general__tbl-select__item--active');
        $(`.ocbc-popup-general__tbl-select__item[data-table=${_value}]`).fadeIn(300);
        $(`.ocbc-popup-general__tbl-select__item[data-table=${_value}]`).addClass('ocbc-popup-general__tbl-select__item--active');
      }, 150);
    })
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        if ($(window).width() < 768) {
          $('.js-filter-table-popup select').trigger('change');
        } else {
          $('.ocbc-popup-general__tbl-select__item').removeAttr('style');
        }
        _windowTemp = $(window).width();
      }
    });

  }

  // --- init
  const init = () => {
    if ($('.js-filter-table-popup').length) {
      handleChange();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCFilterTablePopup;
