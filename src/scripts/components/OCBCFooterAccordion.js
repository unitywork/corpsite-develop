/* ------------------------------------------------------------------------------
@name: OCBC Footer Accordion
@description: OCBC Footer Accordion
--------------------------------------------------------------------------------- */

// --- OCBCFooterAccordion
const OCBCFooterAccordion = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleClick
  const handleClick = () => {

    $('.js-footer-accordion .ocbc-footer-nav__title').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _selectorSiblings = $('.js-footer-accordion .ocbc-footer-nav__title');
      const _parentSiblingsClass = '.js-footer-accordion';
      const _parent = _this.parents(_parentSiblingsClass);
      const _targetClass = '.ocbc-nav--footer';
      const _target = _parent.find(_targetClass);

      if ($(window).width() < 768) {
        if (_selectorSiblings.hasClass('ocbc--showed')) {
          $(_parentSiblingsClass).removeClass('ocbc-accordion--expand');
          setTimeout(() => {
            $(_targetClass).slideUp(250);
            if (_this.hasClass('ocbc--showed')) {
              _this.removeClass('ocbc--showed');
            } else {
              _selectorSiblings.removeClass('ocbc--showed');
              _this.addClass('ocbc--showed');
              _target.slideDown(250, () => {
                _parent.addClass('ocbc-accordion--expand');
              });
            }
          }, 200);
        } else {
          _this.addClass('ocbc--showed');
          _target.slideDown(250, () => {
            _parent.addClass('ocbc-accordion--expand');
          });
        }
        e.preventDefault();
      }

    });

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      // --- trigger show
      if ($(window).width() !== _windowTemp) {
        if ($(window).width() >= 768) {
          $('.js-footer-accordion').removeClass('ocbc-accordion--expand');
          $('.ocbc-nav--footer').removeAttr('style');
          $('.js-footer-accordion .ocbc-footer-nav__title').removeClass('ocbc--showed');
        }
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    handleClick();
    handleWindowResize();

  }

  // --- return
  return {
    init
  }

})();

export default OCBCFooterAccordion;
