/* ------------------------------------------------------------------------------
@name: OCBC Card Floating
@description: OCBC Card Floating
--------------------------------------------------------------------------------- */

// --- OCBCCardFloating
const OCBCCardFloating = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handlePaddingFooter
  const handlePaddingFooter = () => {
    let _moreSpace = 16;
    if ($(window).width() < 768) {
      _moreSpace = 8;
    }

    const _heightCardCA = $('.ocbc-card--ca__prod-detail').height() + _moreSpace;
    $('.ocbc-footer').css('padding-bottom', `${_heightCardCA}px`);

  }

  // --- handleFloatingCard
  const handleFloatingCard = () => {
    const _scrollTop = $(window).scrollTop();
    const _bannerHeight = $('.ocbc-prod-detail-banner').height();
    if ($('.ocbc-card--ca').is(':visible')) {
      if (_scrollTop > _bannerHeight) {
        $('.ocbc-card--ca__prod-detail').addClass('ocbc-card--ca--floating');
      } else {
        $('.ocbc-card--ca__prod-detail').removeClass('ocbc-card--ca--floating');
      }
    } else {
      return false;
    }
  }

  // --- handleScroll
  const handleScroll = () => {
    let _didScroll;

    $(window).scroll(() => {
      _didScroll = true;
      setInterval(() => {
        if (_didScroll) {
          handleFloatingCard();
          _didScroll = false;
        }
      }, 200);
    });

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {

        handlePaddingFooter();
        handleFloatingCard();
        _windowTemp = $(window).width();
      }
    });

  }


  // --- init
  const init = () => {
    if ($('.ocbc-prod-detail-banner__btn').length) {
      handleScroll();
      handleWindowResize();
      handlePaddingFooter();
      handleFloatingCard();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCCardFloating;
