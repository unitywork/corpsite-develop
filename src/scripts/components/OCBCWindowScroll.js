/* ------------------------------------------------------------------------------
@name: OCBC Window Scroll
@description: OCBC Window Scroll
--------------------------------------------------------------------------------- */

// --- OCBCWindowScroll
const OCBCWindowScroll = (() => {
  let _lastScrollTop = 0;
  let _lastScrollTopCalc = 0;
  let _delta = 4;
  let _deltaCalc = 4;
  let _headerHeight = $('.ocbc-header').height() / 2;
  let _headerHeightCalc = $('.ocbc-header-calc').height() / 2;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleHeaderScroll
  const handleHeaderScroll = () => {

    // --- _scrollTop
    const _scrollTop = $(window).scrollTop();

    // --- Make sure they scroll more than _delta
    if (Math.abs(_lastScrollTop - _scrollTop) <= _delta) {
      return;
    }

    // --- Scroll Down
    if (_scrollTop > _lastScrollTop && _scrollTop > _headerHeight) {
      if (!$('.ocbc-show-navigation').length) {
        $('body').addClass('ocbc-scroll-down');
        if ($('.ocbc-header__log-lnk__wrapper').is(':visible')) {
          $('.ocbc-header__log-lnk__wrapper').css({
            'visibility': 'hidden',
            'opacity': '0'
          });
        }
      }
    } else {
      // --- Scroll Up
      if (_scrollTop + $(window).height() < $(document).height()) {
        // special case comparison page
        if ($('.ocbc-saving-comparison').length) {
          const _spaceOptionStart = $('.ocbc-saving-comparison').offset().top - $('.ocbc-header').height();
          const _spaceOptionEnd = $('.ocbc-saving-comparison').offset().top + $('.ocbc-saving-comparison').height();
          // if !between content saving comparison
          if (_scrollTop < _spaceOptionStart || _scrollTop > _spaceOptionEnd || $(window).width() <= 1200) {
            $('body').removeClass('ocbc-scroll-down');
            $('.ocbc-header__log-lnk__wrapper').removeAttr('style');
          }
        } else {
          $('body').removeClass('ocbc-scroll-down');
          $('.ocbc-header__log-lnk__wrapper').removeAttr('style');
        }
      }

    }

    _lastScrollTop = _scrollTop;

  }

  // --- handleChatBot
  const handleChatBot = () => {
    const _scrollTop = $(window).scrollTop();
    const _bannerHeight = $('.ocbc-main-banner').height();
    if (_scrollTop > _bannerHeight) {
      $('body').addClass('show-chat-bot');
    } else {
      $('body').removeClass('show-chat-bot');
    }
  }

  // --- handleBannerCalculator
  const handleBannerCalculator = () => {
    const _scrollTop = $(window).scrollTop();
    const _windowsHeight = $(window).height();
    const _startAbsolute = $('.ocbc-footer').position().top - _windowsHeight;
    const _heightBannerImage = $('.ocbc-calc .ocbc-calc__img-wrapper').height() + $('.ocbc-header-calc').height() + 32;
    if ((_scrollTop > _startAbsolute) || (_windowsHeight < _heightBannerImage)) {
      $('.ocbc-calc').addClass('ocbc-calc--img-scrolled');
    } else {
      $('.ocbc-calc').removeClass('ocbc-calc--img-scrolled');
    }

  }

  // --- handleShareFloating
  const handleShareFloating = () => {
    if ($(window).width() > 1272) {
      const _scrollTop = $(window).scrollTop();
      let _startShow = $('.ocbc-content-detail__head').offset().top + $('.ocbc-content-detail__head').height() + 16;
      let _startAbsolute = $('.ocbc-content-detail').outerHeight();
      if (!$('.ocbc-scroll-down').length) {
        _startShow -= $('.ocbc-header').height();
        _startAbsolute -= ($('.ocbc-header').height() + 48 + $('.ocbc-content-detail__share-floating').height());
      } else {
        _startAbsolute -= ($('.ocbc-header').height() + 40 + 48);
      }
      if (_scrollTop >= _startAbsolute) {
        $('.ocbc-content-detail').removeClass('ocbc-content-detail--show-share-floating').addClass('ocbc-content-detail--absolute-share-floating');
      } else if (_scrollTop >= _startShow) {
        $('.ocbc-content-detail').removeClass('ocbc-content-detail--absolute-share-floating').addClass('ocbc-content-detail--show-share-floating');
      } else {
        $('.ocbc-content-detail').removeClass('ocbc-content-detail--show-share-floating');
      }
    }
  }

  // --- handleScrollIndicator
  const handleScrollIndicator = () => {
    let _scrollTop = $(window).scrollTop();
    let _heightDetail = $('.ocbc-content-detail').outerHeight();
    let _heightContent = $('.ocbc-content-detail__content').outerHeight();
    let _maxContent = _heightDetail - $(window).height();
    if (!$('.ocbc-scroll-down').length) {
      _scrollTop += $('.ocbc-header').height();
      _maxContent += $('.ocbc-header').height();
      _heightDetail += $('.ocbc-header').height();
      _heightContent += $('.ocbc-header').height();
    }
    let _startScroll = _heightDetail - _heightContent;
    let _percentage = (_scrollTop - _startScroll) / (_maxContent - _startScroll) * 100;
    if (_percentage < 0) {
      _percentage = 0;
    } else if (_percentage > 100) {
      _percentage = 100;
    }
    $('.ocbc-scroll-indicator').css('width', `${_percentage}%`);
  }

  // --- handleScroll
  const handleScroll = () => {
    let _didScroll;

    $(window).scroll(() => {
      _didScroll = true;
      setInterval(() => {
        if (_didScroll) {
          if ($('.ocbc-header').length) {
            handleHeaderScroll();
          }
          if ($('.dolphin-chat-icon').length) {
            handleChatBot();
          }
          if ($('.ocbc-calc').length) {
            handleBannerCalculator();
          }
          if ($('.ocbc-content-detail__share-floating').length) {
            handleShareFloating();
          }
          if ($('.js-scroll-indicator').length) {
            handleScrollIndicator();
          }
          _didScroll = false;
        }
      }, 200);
    });
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        if ($('.ocbc-header').length) {
          handleHeaderScroll();
        }
        if ($('.dolphin-chat-icon').length) {
          handleChatBot();
        }
        if ($('.ocbc-calc').length) {
          handleBannerCalculator();
        }
        if ($('.ocbc-content-detail__share-floating').length) {
          handleShareFloating();
        }
        _windowTemp = $(window).width();
      }
    });
  }


  // --- init
  const init = () => {
    handleScroll();
    if ($('.ocbc-header').length) {
      handleHeaderScroll();
    }
    if ($('.dolphin-chat-icon').length) {
      handleChatBot();
    }
    if ($('.ocbc-calc').length) {
      handleBannerCalculator();
    }
    handleWindowResize();
  }

  // --- return
  return {
    init
  }

})();

export default OCBCWindowScroll;
