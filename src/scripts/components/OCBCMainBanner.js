/* ------------------------------------------------------------------------------
@name: OCBC Main Banner
@description: OCBC Main Banner
--------------------------------------------------------------------------------- */

// --- OCBCMainBanner
const OCBCMainBanner = (() => {

  // --- selector
  const _selector = $('.js-main-banner');
  const _itemLength = $('.js-main-banner .ocbc-main-banner__item').length;

  // --- itemsCheck
  const itemsCheck = () => {
    // --- run carousel
    if (_itemLength >= 2) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      // --- set carousel
      /*
        timeout = number - 1500 (timer delay)
        if  timeout = 8500
        then real timeout = 7000
       */
      _selector.owlCarousel({
        loop: false,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        nav: false,
        rewind: true,
        dots: false,
        autoplay: true,
        checkVisible: false,
        autoplayTimeout: 8500,
        autoplayHoverPause: false,
        autoplaySpeed: 750,
        dragEndSpeed: 750,
        animateOut: "fadeOut"
      });

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');
      setTimeout(() => {
        _selector.addClass('ocbc-main-banner--show');
      }, 50);

    }

  }

  // --- init
  const init = () => {
    if ($('.js-main-banner').length) {
      itemsCheck();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCMainBanner;
