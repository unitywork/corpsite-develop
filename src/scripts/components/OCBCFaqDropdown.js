/* ------------------------------------------------------------------------------
@name: OCBC FaqDropdown
@description: OCBC FaqDropdown Event (Click, Input, etc)
--------------------------------------------------------------------------------- */

// --- OCBCFaqDropdown
const OCBCFaqDropdown = (() => {

  // handleFAQDropdownContent
  const handleFAQDropdownContent = () => {
    $('body').on('click', () => {
      // --- click item
    }).on('click', '.js-select-faq-dropdown .ocbc-select__item', (e) => {
      const _this = $(e.currentTarget);
      const _value = _this.attr('data-value');
      const _answer = $('.ocbc-faq-dd__ans-list');

      _answer.removeClass('ocbc-faq-dd__ans-list--active');
      $(`.ocbc-faq-dd__ans-list[data-id='${_value}']`).addClass('ocbc-faq-dd__ans-list--active');

    })
  }

  // --- init
  const init = () => {
    if ($('.js-select-faq-dropdown').length) {
      handleFAQDropdownContent();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCFaqDropdown;
