/* ------------------------------------------------------------------------------
@name: OCBC Mobile Menu
@description: OCBC Mobile Menu
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// -- import components
import {
  OCBCMegaMenu
} from "components";

// --- OCBCMobileMenu
const OCBCMobileMenu = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleClick
  const handleClick = () => {
    // --- burger-menu
    $('.js-burger-menu').on('click', (e) => {
      const _this = $(e.currentTarget);
      // --- if window with <= 1200
      if ($(window).width() <= 1200) {
        if (_this.hasClass('ocbc-burger-menu--close')) {
          handleHideNav();
          // --- trigger collapse
          OCBCMegaMenu.collapseAccordtionMegaMenu('mobile-menu');
        } else {
          _this.addClass('ocbc-burger-menu--close');
          $('body').addClass('ocbc-show-navigation');
          Scrolllable.disable();
        }
      }

    });

    // --- side-menu
    $('.js-side-menu').on('click', (e) => {
      // --- if window with <= 1200
      if ($(window).width() <= 1200) {
        if ($('.js-burger-menu').hasClass('ocbc-burger-menu--close')) {
          handleHideNav();
          // --- trigger collapse
          OCBCMegaMenu.collapseAccordtionMegaMenu('mobile-menu');
        }
      }

    });

    $('.ocbc-header__side-menu__scrollover').on('click', (e) => {
      // --- if window with <= 1200
      if ($(window).width() <= 1200) {
        e.stopPropagation();
      }
    });

  }

  // --- handleHideNav
  const handleHideNav = () => {
    $('body').removeClass('ocbc-show-navigation');
    Scrolllable.enable();
    $('.js-burger-menu').removeClass('ocbc-burger-menu--close');
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        if ($(window).width() >= 1200) {
          handleHideNav();
        }
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    handleClick();
    handleWindowResize();

  }

  // --- return
  return {
    init,
    handleHideNav
  }

})();

export default OCBCMobileMenu;
