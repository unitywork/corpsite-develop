/* ------------------------------------------------------------------------------
@name: OCBC Slider Testimonial
@description: OCBC Slider Testimonial
--------------------------------------------------------------------------------- */

// --- OCBCSliderTestimonial
const OCBCSliderTestimonial = (() => {

  // --- selector
  const _selector = $('.js-slider-testimonial');
  const _itemLength = $('.js-slider-testimonial .ocbc-slider-testimonial__item').length;

  // --- itemsCheck
  const itemsCheck = () => {
    // --- run carousel
    if (_itemLength >= 2) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      // --- set carousel
      _selector.owlCarousel({
        loop: true,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        nav: true,
        rewind: false,
        dots: false,
        autoplay: false,
        checkVisible: false,
        autoplayTimeout: 8500,
        autoplayHoverPause: false,
        autoplaySpeed: 750,
        dragEndSpeed: 750,
        navSpeed: 750
      });

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');
      setTimeout(() => {
        _selector.addClass('ocbc-slider-testimonial--show');
      }, 50);

    }

  }

  // --- init
  const init = () => {
    if ($('.js-slider-testimonial').length) {
      itemsCheck();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCSliderTestimonial;
