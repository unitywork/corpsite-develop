/* ------------------------------------------------------------------------------
@name: OCBC Vertical Tab
@description: OCBC Vertical Tab
--------------------------------------------------------------------------------- */

// --- OCBCVerticalTab
const OCBCVerticalTab = (() => {
  // --- handleClickTab
  const handleClickTab = () => {
    $('.js-vertical-tab').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-target');
      const _tab = _this.parents('.ocbc-vertical-tab').find('.ocbc-vertical-tab__wrapper');
      const _activeTab = 'ocbc-vertical-tab__wrapper--active';
      const _subcategory = _this.find('.ocbc-vertical-tab__sub');
      const _hasSubcategory = _subcategory.length;
      const _content = _this.parents('.ocbc-vertical-tab__page').find('.ocbc-vt-content');
      const _activeContent = 'ocbc-vt-content--active';

      if (_this.hasClass(_activeTab)) {
        return false;
      }

      // if conditional about content
      if (!_this.hasClass('js-career-about-content')) {
        $('.ocbc-vertical-tab--about').slideUp(350, () => {
          $('.ocbc-vertical-tab--about').removeClass('ocbc-vertical-tab--about__active').removeAttr('style');
        });
      }

      // Change active tab
      _tab.removeClass(_activeTab);
      _this.addClass(_activeTab);

      setTimeout(() => {
        if (!_hasSubcategory) {
          _content.removeClass(_activeContent);
        }
      }, 150);

      if (!_hasSubcategory) {
        // Change active content
        _content.fadeOut(0);
        $(`.ocbc-vt-content[data-pane=${_target}]`).fadeIn(300);

        // Sync active state with Dropdown
        $('.js-vt-dd').find(`option[value="${_target}"]`).prop('selected', true);
        $('.js-vt-dd').find(`option[value="${_target}"]`).parents('.js-vt-dd').change();
      } else {
        // trigger click first child
        _this.find('.js-sub-vertical-tab:first').trigger('click');
      }

      e.stopPropagation();

    });
  }

  // --- handleClickSubTab
  const handleClickSubTab = () => {
    $('.js-sub-vertical-tab').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-target');
      const _tab = _this.parents('.ocbc-vertical-tab').find('.ocbc-vertical-tab__sub');
      const _activeTab = 'ocbc-vertical-tab__sub--active';

      const _content = _this.parents('.ocbc-vertical-tab__page').find('.ocbc-vt-content');
      const _activeContent = 'ocbc-vt-content--active';

      if (_this.hasClass(_activeTab)) {
        return false;
      }

      // if conditional about content
      if (!_this.hasClass('js-career-about-content')) {
        $('.ocbc-vertical-tab--about').slideUp(350, () => {
          $('.ocbc-vertical-tab--about').removeClass('ocbc-vertical-tab--about__active').removeAttr('style');
        });
      }

      // Change active tab
      _tab.removeClass(_activeTab);
      _this.addClass(_activeTab);

      // Change active content
      _content.fadeOut(0);
      $(`.ocbc-vt-content[data-pane=${_target}]`).fadeIn(300);
      setTimeout(() => {
        _content.removeClass(_activeContent);
      }, 150);

      // Sync active state with Dropdown
      $('.js-vt-dd').find(`option[value="${_target}"]`).prop('selected', true);
      $('.js-vt-dd').find(`option[value="${_target}"]`).parents('.js-vt-dd').change();

      e.stopPropagation();

    });
  }

  // --- handleClickDropdown
  const handleClickDropdown = () => {
    let _targetTemp = $('.ocbc-vertical-tab__dd__select .ocbc-select__element').val();
    if ($('.ocbc-vertical-tab__dd__select').length) {
      $('body').on('click', () => {
        // --- click item
      }).on('click', '.ocbc-vertical-tab__dd__select .ocbc-select__item', (e) => {
        const _this = $(e.currentTarget);
        const _target = _this.parents('.js-select').find('.js-vt-dd').val();
        const _content = _this.parents('.ocbc-row').find('.ocbc-vt-content');
        const _activeContent = 'ocbc-vt-content--active';
        const _tab = _this.parents('.ocbc-row').find('.ocbc-vertical-tab__wrapper');
        const _activeTab = 'ocbc-vertical-tab__wrapper--active';

        if (_this.hasClass('ocbc-select__item--sub-label')) {
          return false;
        }

        if (_target != _targetTemp) {

          if (_target == 'about') {
            $('.ocbc-vertical-tab--about').show(0).addClass('ocbc-vertical-tab--about__active');
            $('.ocbc-vertical-tab--about').slideDown(350);
          } else {
            $('.ocbc-vertical-tab--about').slideUp(350, () => {
              $('.ocbc-vertical-tab--about').removeClass('ocbc-vertical-tab--about__active').removeAttr('style');
            });
          }

          // Change active content
          _content.fadeOut(0);
          $(`.ocbc-vt-content[data-pane=${_target}]`).fadeIn(300);

          setTimeout(() => {
            _content.removeClass(_activeContent);
          }, 150);

          // Sync active state with Tab
          // _tab.removeClass(_activeTab);
          $(`.ocbc-vertical-tab__wrapper[data-target=${_target}]`).addClass(_activeTab);
          _targetTemp = _target;
        }

      });
    }
  }

  // --- init
  const init = () => {
    if ($('.js-vertical-tab')) {
      handleClickTab();
      handleClickSubTab();
      handleClickDropdown();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCVerticalTab;
