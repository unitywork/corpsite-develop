/* ------------------------------------------------------------------------------
@name: OCBC Header Search
@description: OCBC Header Search
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCHeaderSearch
const OCBCHeaderSearch = (() => {

  // --- handleClick
  const handleClick = () => {
    // --- show
    $('.js-header-search-show').on('click', () => {
      Scrolllable.disable();
      setTimeout(() => {
        $('body').addClass('ocbc-search-form--show');
        $('.ocbc-header__search .ocbc-search-section__form .ocbc-input').val('');
      }, 5);
      setTimeout(() => {
        $('.ocbc-header__search .ocbc-search-section__form .ocbc-input').focus();
        if ($(window).width() <= 1200) {
          $('body').removeClass('ocbc-show-navigation');
          $('.js-burger-menu').removeClass('ocbc-burger-menu--close');
        }
      }, 500);
    });

    // --- hide
    $('.js-header-search-close, body').on('click', () => {
      if ($('body').hasClass('ocbc-search-form--show')) {
        $('.ocbc-header__search .ocbc-search-section__form .ocbc-input').blur();
        $('body').removeClass('ocbc-search-form--show');
        Scrolllable.enable();
      }
    });

    // --- stop propagation
    $('.ocbc-header__search .ocbc-search-section__form-wrapper, .ocbc-search-section__quick-links').on('click', (e) => {
      e.stopPropagation();
    });

  }

  // --- init
  const init = () => {
    handleClick();

  }

  // --- return
  return {
    init
  }

})();

export default OCBCHeaderSearch;
