/* ------------------------------------------------------------------------------
@name: OCBC FAQ Accordion
@description: OCBC FAQ Accordion
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCFAQAccordion
const OCBCFAQAccordion = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleClick
  const handleClick = () => {
    // --- show button
    $('.js-faq-acc').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-faq-ac');
      const _parentSiblingsClass = $('.ocbc-faq-ac');

      if (_parentSiblingsClass.hasClass('ocbc-faq-ac--expand')) {
        _parentSiblingsClass.removeClass('ocbc-faq-ac--expand');
        $('.ocbc-faq-ac .ocbc-faq-ac__body').slideUp(250);
        if (_this.hasClass('ocbc--showed')) {
          _this.removeClass('ocbc--showed');
        } else {
          $('.js-faq-acc').removeClass('ocbc--showed');
          _this.addClass('ocbc--showed');
          _parent.find('.ocbc-faq-ac__body').slideDown(250, () => {
            _parent.addClass('ocbc-faq-ac--expand');
          });
        }
      } else {
        _this.addClass('ocbc--showed');
        _parent.find('.ocbc-faq-ac__body').slideDown(250, () => {
          _parent.addClass('ocbc-faq-ac--expand');
        });
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.ocbc-faq-ac').length) {
      handleClick();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCFAQAccordion;
