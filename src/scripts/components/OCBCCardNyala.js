/* ------------------------------------------------------------------------------
@name: OCBC Card Nyala
@description: OCBC Card Nyala
--------------------------------------------------------------------------------- */

// --- OCBCCardNyala
const OCBCCardNyala = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleFocus
  const handleFocus = () => {
    if ($('.js-focus-nyala-card').length) {
      if ($(window).width() < 992) {
        const _getPosLeft = document.querySelector(
          '.js-focus-nyala-card'
        ).offsetLeft;
        const _scrollLeft = _getPosLeft - (($(window).width() - _getPosLeft + 8) / 2);
        $('.ocbc-card-nyala-c__list').animate({
          scrollLeft: _scrollLeft
        }, 300);
      }
    }
  }

  // --- handleSetHeight
  const handleSetHeight = () => {
    if ($('.js-focus-nyala-card').length) {
      // reset height
      $('.ocbc-card-nyala-c .ocbc-card-nyala-c__item .ocbc-card__box').removeAttr('style');

      // search heighest card
      let _getHeighestCard = 0;
      $('.ocbc-card-nyala-c .ocbc-card-nyala-c__item').each((index, element) => {
        const _getHeightCard = $(element).find('.ocbc-card__box').height();
        if (_getHeighestCard < _getHeightCard) {
          _getHeighestCard = _getHeightCard;
        }
      });

      // set height base on heighest card
      $('.ocbc-card-nyala-c .ocbc-card-nyala-c__item .ocbc-card__box').height(_getHeighestCard);

    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        handleFocus();
        handleSetHeight();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    handleFocus();
    handleSetHeight();
    handleWindowResize();
  }

  // --- return
  return {
    init
  }

})();

export default OCBCCardNyala;
