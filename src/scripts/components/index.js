/* ------------------------------------------------------------------------------
@name: Components Index
@description: Indexing all components
--------------------------------------------------------------------------------- */

import OCBCWindowScroll from "./OCBCWindowScroll";
import OCBCHeaderSearch from "./OCBCHeaderSearch";
import OCBCHeaderLogin from "./OCBCHeaderLogin";
import OCBCFooterAccordion from "./OCBCFooterAccordion";
import OCBCMegaMenu from "./OCBCMegaMenu";
import OCBCMobileMenu from "./OCBCMobileMenu";
import OCBCMainBanner from "./OCBCMainBanner";
import OCBCPromoBanner from "./OCBCPromoBanner";
import OCBCCardNeeds from "./OCBCCardNeeds";
import OCBCCardHeadProduct from "./OCBCCardHeadProduct";
import OCBCFormSelect from "./OCBCFormSelect";
import OCBCSectionScroll from "./OCBCSectionScroll";
import OCBCAutoSlide from "./OCBCAutoSlide";
import OCBCFullPageTab from "./OCBCFullPageTab";
import OCBCTaggingTab from "./OCBCTaggingTab";
import OCBCVerticalTab from "./OCBCVerticalTab";
import OCBCFaqDropdown from "./OCBCFaqDropdown";
import OCBCMobileTabCurrency from "./OCBCMobileTabCurrency";
import OCBCCardNyala from "./OCBCCardNyala";
import OCBCCardNyalaBenefit from "./OCBCCardNyalaBenefit";
import OCBCCardRecommendation from "./OCBCCardRecommendation";
import OCBCCardAchievement from "./OCBCCardAchievement";
import OCBCCardProductCategory from "./OCBCCardProductCategory";
import OCBCSliderTestimonial from "./OCBCSliderTestimonial";
import OCBCTabs from "./OCBCTabs";
import OCBCFilter from "./OCBCFilter";
import OCBCAboutUs from "./OCBCAboutUs";
import OCBCRangeSlider from "./OCBCRangeSlider";
import OCBCCalcResult from "./OCBCCalcResult";
import OCBCRachelHowTo from "./OCBCRachelHowTo";
import OCBCOneMobileScroll from "./OCBCOneMobileScroll";
import OCBCCardFloating from "./OCBCCardFloating";
import OCBCOneMobileBanner from "./OCBCOneMobileBanner";
import OCBCPopupGeneral from "./OCBCPopupGeneral";
import OCBCFilterTablePopup from "./OCBCFilterTablePopup";
import OCBCCookiesFloating from "./OCBCCookiesFloating";
import OCBCHowTo from "./OCBCHowTo";
import OCBCContentDetail from "./OCBCContentDetail";
import OCBCFAQAccordion from "./OCBCFAQAccordion";
import OCBCPremierService from "./OCBCPremierService";
import OCBCContactUs from "./OCBCContactUs";
import OCBCCareerAboutContent from "./OCBCCareerAboutContent";
import OCBCProductComparison from "./OCBCProductComparison";
import OCBCScrollSection from "./OCBCScrollSection";

export {
  OCBCWindowScroll,
  OCBCHeaderSearch,
  OCBCHeaderLogin,
  OCBCFooterAccordion,
  OCBCMegaMenu,
  OCBCMobileMenu,
  OCBCMainBanner,
  OCBCPromoBanner,
  OCBCCardNeeds,
  OCBCCardHeadProduct,
  OCBCFormSelect,
  OCBCSectionScroll,
  OCBCAutoSlide,
  OCBCFullPageTab,
  OCBCTaggingTab,
  OCBCVerticalTab,
  OCBCFaqDropdown,
  OCBCMobileTabCurrency,
  OCBCCardNyala,
  OCBCCardNyalaBenefit,
  OCBCCardRecommendation,
  OCBCCardAchievement,
  OCBCCardProductCategory,
  OCBCSliderTestimonial,
  OCBCTabs,
  OCBCFilter,
  OCBCAboutUs,
  OCBCRangeSlider,
  OCBCRachelHowTo,
  OCBCCalcResult,
  OCBCOneMobileScroll,
  OCBCCardFloating,
  OCBCOneMobileBanner,
  OCBCPopupGeneral,
  OCBCFilterTablePopup,
  OCBCCookiesFloating,
  OCBCHowTo,
  OCBCContentDetail,
  OCBCFAQAccordion,
  OCBCPremierService,
  OCBCContactUs,
  OCBCCareerAboutContent,
  OCBCProductComparison, 
  OCBCScrollSection,
};
