/* ------------------------------------------------------------------------------
@name: OCBC How To
@description: OCBC How To
--------------------------------------------------------------------------------- */

// --- OCBCRachelHowTo
const OCBCRachelHowTo = (() => {

  // --- selector
  const _selector = $('.js-rachel-how-to-carousel');
  const _itemLength = $('.js-rachel-how-to-carousel .ocbc-rachel-how-to__item').length;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- itemsCheck
  const itemsCheck = () => {
    let _animationOut = '';
    // --- destroy carousel
    _selector.owlCarousel('destroy');

    // --- run carousel
    if (_itemLength > 1) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      if ($(window).width() >= 992) {
        _animationOut = 'fadeOut';
      }

      // --- set carousel
      _selector.owlCarousel({
        loop: false,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        nav: false,
        rewind: true,
        dots: true,
        autoplay: false,
        checkVisible: false,
        autoplayTimeout: 7500,
        autoplayHoverPause: false,
        autoplaySpeed: 750,
        dragEndSpeed: 750,
        navSpeed: 750,
        dotsSpeed: 750,
        autoHeight: true,
        animateOut: _animationOut,
        onTranslate: (e) => {
          handleStepActive(e.item.index);
        },
        responsive: {
          0: {
            loop: true,
            touchDrag: true
          },
          992: {
            touchDrag: false
          }
        }
      });

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');

    }

  }

  // --- handleClickStep
  const handleClickStep = () => {
    $('.js-rachel-how-to-step .ocbc-rachel-how-to__step__item').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _index = _this.index();

      _selector.trigger('to.owl.carousel', [_index, 750]);
    });
  }

  // --- handleClickNext
  const handleClickNext = () => {
    $('.js-rachel-how-to-next').on('click', (e) => {
      _selector.trigger('next.owl.carousel', [750]);
    });
  }

  // --- handleStepActive
  const handleStepActive = (index) => {
    $('.ocbc-rachel-how-to__step__item').removeClass('ocbc-rachel-how-to__step__item--active');
    $('.ocbc-rachel-how-to__step__item').eq(index).addClass('ocbc-rachel-how-to__step__item--active');
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        itemsCheck();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-rachel-how-to-carousel').length) {
      handleWindowResize();
      itemsCheck();
      handleClickStep();
      handleClickNext();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCRachelHowTo;
