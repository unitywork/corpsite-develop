/* ------------------------------------------------------------------------------
@name: OCBC About Us
@description: About Us
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCPopupGeneral
const OCBCPopupGeneral = (() => {

  // --- handleMoveElement
  const handleMoveElement = () => {
    // move general popup
    $.each($('.ocbc-popup-general'), (index, element) => {
      const _this = $(element);
      const _class = _this.attr('class');
      const _getHTML = `<div class="${_class}">${_this.html()}</div>`;
      _this.remove();
      $('body').append(_getHTML);
    });

  }

  // --- handleEsc
  const handleEsc = () => {
    $(document).on('keydown', (e) => {
      if (e.which == 27) {
        if ($('.ocbc-popup-general').hasClass('ocbc-popup-general--show')) {
          $('.ocbc-popup-general').removeClass('ocbc-popup-general--show');
          Scrolllable.enable();
        }
        if ($('body').hasClass('ocbc-popup-bod-show')) {
          $('body').removeClass('ocbc-popup-bod-show');
          Scrolllable.enable();
        }
        if ($('body').hasClass('ocbc-popup-file-report-show')) {
          $('body').removeClass('ocbc-popup-file-report-show')
          Scrolllable.enable();
        }
        if ($('body').hasClass('ocbc-search-form--show')) {
          $('.js-header-search-close').trigger('click');
        }
      }
    });
  }

  // --- handleShow
  const handleShow = () => {
    $('.js-show-popup-general').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-target');
      $(_target).addClass('ocbc-popup-general--show');
      Scrolllable.disable();
    });

  }

  // --- handleHide
  const handleHide = () => {
    $('.js-hide-popup-general').on('click', (e) => {
      $('.ocbc-popup-general').removeClass('ocbc-popup-general--show');
      Scrolllable.enable();
    });

  }

  // --- init
  const init = () => {

    handleEsc();

    if ($('.ocbc-popup-general').length) {
      handleMoveElement();
      handleShow();
      handleHide();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCPopupGeneral;
