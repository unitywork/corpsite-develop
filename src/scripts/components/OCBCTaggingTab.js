/* ------------------------------------------------------------------------------
@name: OCBC Tagging Tab
@description: OCBC Tagging Tab
--------------------------------------------------------------------------------- */

// --- OCBCTaggingTab
const OCBCTaggingTab = (() => {

  const initTab = () => {
    $('.ocbc-tagging-tab__item').first().addClass('ocbc-tagging-tab__item--active');
  }

  const onTabClick = () => {
    $('.ocbc-tagging-tab__item').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _item = $('.ocbc-tagging-tab__item');

      _item.removeClass('ocbc-tagging-tab__item--active');
      _this.addClass('ocbc-tagging-tab__item--active');
    });

  }

  // --- init
  const init = () => {
    if($('.ocbc-tagging-tab')) {
      initTab();
      onTabClick();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCTaggingTab;
