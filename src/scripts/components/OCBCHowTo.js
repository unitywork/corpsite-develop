/* ------------------------------------------------------------------------------
@name: OCBC How To
@description: OCBC How To
--------------------------------------------------------------------------------- */

// --- OCBCHowTo
const OCBCHowTo = (() => {

  // --- selector
  const _selector = $('.js-how-to-carousel');
  const _itemLength = $('.js-how-to-carousel .ocbc-how-to__item').length;
  const _loopCarouselMobile = true;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- itemsCheck
  const itemsCheck = () => {
    let _animationOut = '';
    // --- destroy carousel
    _selector.owlCarousel('destroy');

    // --- run carousel
    if (_itemLength > 1) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      // --- set carousel
      _selector.owlCarousel({
        loop: false,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        nav: false,
        rewind: true,
        dots: true,
        autoplay: false,
        checkVisible: false,
        autoplayTimeout: 7500,
        autoplayHoverPause: false,
        autoplaySpeed: 750,
        dragEndSpeed: 750,
        navSpeed: 750,
        dotsSpeed: 750,
        autoHeight: true,
        animateOut: 'fadeOut',
        onDragged: (e) => {
          handleSetSelected();
        },
        responsive: {
          0: {
            loop: true,
            touchDrag: true,
            animateOut: ''
          },
          992: {
            touchDrag: false
          }
        }
      });

      // if mobile
      if ($(window).width() < 992) {
        handleInitializeDots();
      }

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');

    }

  }

  // --- handleClickCategory
  const handleClickCategory = () => {
    $('body').on('click', '.js-how-to-filter .ocbc-select__item', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-value');
      const _subCategory = $(`.ocbc-how-to__sub-filter__pane[data-category=${_target}]`);

      $('.ocbc-how-to__sub-filter__pane').slideUp(150).removeClass('ocbc-how-to__sub-filter__pane--active');

      // hold transition on tablet portrait
      $('.ocbc-feature').addClass('ocbc-feature--hold-transition');

      if (Boolean(Number(_subCategory.length))) {
        $('.ocbc-feature').addClass('ocbc-feature--has-sub');
        _subCategory.slideDown(250);

        let _subCategoryActive = _subCategory.find('.ocbc-how-to__sub-filter__item--active');
        if (!_subCategoryActive.length) {
          _subCategoryActive = _subCategory.find('.ocbc-how-to__sub-filter__item:first');
        }
        // trigger click sub
        _subCategoryActive.trigger('click');

      } else {
        $('.ocbc-feature').removeClass('ocbc-feature--has-sub');
        const _index = handleGetIndex('main', _target);

        // carousel trigger
        handleCarouselTrigger(_index);

        // hold transition on tablet portrait
        setTimeout(() => {
          $('.ocbc-feature').removeClass('ocbc-feature--hold-transition');
        }, 750);

      }

    });
  }

  // --- handleClickSubCategory
  const handleClickSubCategory = () => {
    $('body').on('click', '.js-how-to-filter .ocbc-how-to__sub-filter__item', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-subcategory');
      const _dataCategory = _this.parents('.ocbc-how-to__sub-filter__pane').attr('data-category');
      const _index = handleGetIndex('sub', _target);

      // hold transition on tablet portrait
      $('.ocbc-feature').addClass('ocbc-feature--hold-transition');

      // set active
      _this.siblings('.ocbc-how-to__sub-filter__item').removeClass('ocbc-how-to__sub-filter__item--active');
      _this.addClass('ocbc-how-to__sub-filter__item--active');

      // carousel trigger
      handleCarouselTrigger(_index);

      // scroll left
      handleSubScrollLeft(`[data-category=${_dataCategory}] .ocbc-how-to__sub-filter__item--active`);

      // hold transition on tablet portrait
      setTimeout(() => {
        $('.ocbc-feature').removeClass('ocbc-feature--hold-transition');
      }, 750);

    });
  }

  // --- handleInitializeDots
  const handleInitializeDots = () => {
    let _target = $('.js-how-to-filter select').val();
    if ($(`[data-category="${_target}"]`).hasClass('ocbc-how-to__sub-filter__pane')) {
      _target = $(`[data-category="${_target}"]`).find('.ocbc-how-to__sub-filter__item--active').attr('data-subcategory');
    }

    const _index = handleGetIndex('init-dots', _target);
    // carousel trigger
    handleCarouselTrigger(_index);

  }

  // --- handleGetIndex
  const handleGetIndex = (status, target) => {
    const _item = $('.ocbc-how-to__item');
    let _indexMin = 0; // minus beacause carousel duplicate elements
    let _targetSelector = '';
    let _targetLength = 0;
    let _index = {};

    // if loop true
    if ($(window).width() < 992 && _loopCarouselMobile) {
      _indexMin = 2;
    }

    _targetSelector = _selector.find('.owl-item:not(".cloned")').find(`.ocbc-how-to__item[data-category=${target}]`);
    _index.start = _item.index(_targetSelector) - _indexMin;

    _targetLength = _targetSelector.length;
    _index.end = _index.start + _targetLength;

    return _index;

  }

  // --- handleCarouselTrigger
  const handleCarouselTrigger = (index, dotOnly=false) => {
    _selector.find('.owl-dot').hide(0).slice(index.start, index.end).show(0);
    if (!dotOnly) {
      _selector.trigger('to.owl.carousel', [index.start, 750]);
    }
  }

  // --- handleSetSelected
  const handleSetSelected = () => {
    if ($(window).width() < 992) {
      const _activeCategory = _selector.find('.owl-item.active').find('.ocbc-how-to__item').attr('data-category');
      const _categoryItem = 'ocbc-select__item';
      const _subCategoryItem = 'ocbc-how-to__sub-filter__item';
      const _subCategoryPane = 'ocbc-how-to__sub-filter__pane';

      const _subSelector = $(`.${_subCategoryItem}[data-subcategory="${_activeCategory}"]`);
      const _dataCategory = _subSelector.parents(`.${_subCategoryPane}`).attr('data-category');
      const _isSubCategory = Boolean(Number($(`.ocbc-how-to__sub-filter__pane[data-category="${_dataCategory}"]`).length));

      // sub category
      if (_isSubCategory) {
        const _getValSelect = $('.js-how-to-filter select').val();

        // set auto select
        if (_dataCategory != _getValSelect) {
          $(`.js-how-to-filter option[value="${_dataCategory}"]`).prop('selected', true);
          $('.js-how-to-filter select').trigger('change');

          if (!$('.ocbc-feature').hasClass('ocbc-feature--has-sub')) {
            $('.ocbc-feature').addClass('ocbc-feature--has-sub');
          }

          $(`.${_subCategoryPane}`).slideUp(150).removeClass(`${_subCategoryPane}--active`);
          $(`.${_subCategoryPane}[data-category=${_dataCategory}]`).slideDown(250, () => {
            const _getHeight = $('.ocbc-how-to__sub-filter').outerHeight() + _selector.find('.owl-stage-outer').outerHeight();
            _selector.find('.owl-stage-outer').height(_getHeight);
          });

        }

        // carousel trigger
        const _index = handleGetIndex('sub', _activeCategory);
        handleCarouselTrigger(_index, true);

        if ($('.ocbc-how-to__sub-filter__item--active').length) {
          // set active
          _subSelector.siblings('.ocbc-how-to__sub-filter__item').removeClass('ocbc-how-to__sub-filter__item--active');
          _subSelector.addClass('ocbc-how-to__sub-filter__item--active');
          // scroll left
          handleSubScrollLeft(`[data-category=${_dataCategory}] .ocbc-how-to__sub-filter__item--active`);
        } else {
          // set active
          $(`.${_subCategoryItem}[data-subcategory="${_activeCategory}"]`).addClass('ocbc-how-to__sub-filter__item--active');
        }

      }
      // main category
      else if (!$(`.${_categoryItem}[data-value="${_activeCategory}"]`).hasClass(`${_categoryItem}--selected`)) {
        $(`.js-how-to-filter option[value="${_activeCategory}"]`).prop('selected', true);
        $('.js-how-to-filter select').trigger('change');

        $(`.${_subCategoryPane}`).slideUp(150, ()=>{
          const _getHeight = _selector.find('.owl-stage-outer').outerHeight() - ($('.ocbc-how-to__sub-filter').outerHeight() + 25);
          _selector.find('.owl-stage-outer').height(_getHeight);
        }).removeClass(`${_subCategoryPane}--active`);

        // remove sub active
        $(`.${_subCategoryItem}`).removeClass(`${_subCategoryItem}--active`);
        $('.ocbc-feature').removeClass('ocbc-feature--has-sub');

        const _index = handleGetIndex('main', _activeCategory);

        // carousel trigger
        handleCarouselTrigger(_index, true);

      }
    }

  }

  // --- handleSubScrollLeft
  const handleSubScrollLeft = (selector) => {
    let _spaceMin = 16;
    if ($(window).width() >= 992) {
      _spaceMin = 32;
    }
    const _getPosLeft = document.querySelector(selector).offsetLeft - _spaceMin;
    $('.ocbc-how-to__sub-filter__list').animate({
      scrollLeft: _getPosLeft
    }, 300);
  }

  // --- handleClickNext
  const handleClickNext = () => {
    $('.js-how-to-next').on('click', (e) => {
      _selector.trigger('next.owl.carousel', [750]);
    });
  }

  // --- handleClickBack
  const handleClickBack = () => {
    $('.js-how-to-back').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-how-to__item');
      const _parentData = _parent.attr('data-category');
      const _item = $('.ocbc-how-to__item');
      const _current = _item.filter(`[data-category=${_parentData}]`)
      const _index = _item.index(_current);

      _selector.trigger('to.owl.carousel', [_index, 750]);
    });
  }

  // --- handleSetFrameMobile
  const handleSetFrameMobile = () => {
    if ($(window).width() < 992) {
      const _getImage = $('.ocbc-how-to__frame-wrapper .ocbc-how-to__frame').attr('src');
      $('.ocbc-how-to__screen-wrapper').css('background-image', `url(${_getImage})`);
    } else {
      $('.ocbc-how-to__screen-wrapper').removeAttr('style');
    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        itemsCheck();
        handleSetFrameMobile();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-how-to-carousel').length) {
      itemsCheck();
      handleClickCategory();
      handleClickSubCategory();
      handleClickNext();
      handleClickBack();
      handleSetFrameMobile();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCHowTo;
