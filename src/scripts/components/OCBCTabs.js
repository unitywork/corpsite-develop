/* ------------------------------------------------------------------------------
@name: OCBC Tabs
@description: OCBC Tabs
--------------------------------------------------------------------------------- */

// --- OCBCTabs
const OCBCTabs = (() => {

  // --- handleClick
  const handleClick = () => {
    $('.js-tabs .ocbc-tabs__controll__item').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-target');
      const _parent = _this.parents('.ocbc-tabs__controll');

      if (!_this.hasClass('ocbc-tabs__controll__item--active')) {
        _this.siblings('.ocbc-tabs__controll__item').removeClass('ocbc-tabs__controll__item--active');
        _this.addClass('ocbc-tabs__controll__item--active');

        // if compare
        if (_parent.hasClass('js-tabs-compare-product')) {
          const _url = _this.attr('data-url');
          $('.js-compare-product').attr('href', _url);
        }

        if ($(window).width() < 768) {
          handleCardScrollLeft();
        }
        $('.ocbc-tabs__pane').fadeOut(150);
        $(`.ocbc-tabs__pane[data-pane=${_target}]`).fadeIn(300);
        setTimeout(() => {
          $('.ocbc-tabs__pane').removeClass('ocbc-tabs__pane--active');
        }, 150);
      }

    });

  }

  // --- handleCardScrollLeft
  const handleCardScrollLeft = () => {
    const _getPosLeft = document.querySelector(
      '.ocbc-tabs__controll__item--active'
    ).offsetLeft - 16;
    $('.js-tabs').animate({
      scrollLeft: _getPosLeft
    }, 300);
  }

  // --- init
  const init = () => {
    if ($('.js-tabs').length) {
      handleClick();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCTabs;
