/* ------------------------------------------------------------------------------
@name: OCBC Premier Serive
@description: Premier Serive
--------------------------------------------------------------------------------- */

// --- OCBCPremierService
const OCBCPremierService = (() => {

  // --- handleSelect
  const handleSelect = () => {
    // service category
    $('.js-select-service-category select').on('change', (e) => {
      const _this = $(e.currentTarget);
      const _value = _this.val();

      if (_value == 'all') {
        $('.ocbc-card-prod-category').fadeIn(250);
        setTimeout(() => {
          $('.ocbc-card-prod-category').removeAttr('style');
        }, 250);
      } else {
        $('.ocbc-card-prod-category').fadeOut(25);
        $(`.ocbc-card-prod-category[data-category=${_value}]`).fadeIn(250);
      }

    });

    // market outlook
    $('.js-select-market-outlook select').on('change', (e) => {
      const _this = $(e.currentTarget);
      const _value = _this.val();

      $('.ocbc-popup-general__content__item').fadeOut(150);
      $(`.ocbc-popup-general__content__item[data-pane=${_value}]`).fadeIn(300);
      setTimeout(() => {
        $('.ocbc-popup-general__content__item').removeClass('ocbc-popup-general__content__item--active');
      }, 150);

    });

  }


  // --- init
  const init = () => {
    if ($('.js-select-service-category').length || $('.js-select-market-outlook').length) {
      handleSelect();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCPremierService;
