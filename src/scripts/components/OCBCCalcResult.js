/* ------------------------------------------------------------------------------
@name: OCBC Icon Text
@description: OCBC Icon Text
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCCalcResult
const OCBCCalcResult = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleClickShare
  const handleClickShare = () => {

    // --- show share
    $('.js-calc-result-share').on('click', (e) => {
      const _this = $(e.currentTarget),
        _parent = _this.parents('.ocbc-icon-text');

      if ($(window).width() < 768) {
        _parent.addClass('show-share-list');
      } else {
        if (_parent.hasClass('show-share-list')) {
          _parent.removeClass('show-share-list');
        } else {
          _parent.addClass('show-share-list');
        }
      }

      e.preventDefault();
      e.stopPropagation();

    });

    // --- hide
    $('.js-calc-result-share-close, .js-calc-result-share-list .ocbc-icon-text__share__item').on('click', () => {
      if ($('.ocbc-icon-text').hasClass('show-share-list')) {
        $('.js-calc-result-share').parents('.ocbc-icon-text').removeClass('show-share-list');
      }
    });

    // --- stopPropagation
    $('.ocbc-icon-text__share').on('click', (e) => {
      e.stopPropagation();
    });

    // --- hide
    $('body').on('click', () => {
      if ($('.ocbc-icon-text').hasClass('show-share-list')) {
        $('.js-calc-result-share').parents('.ocbc-icon-text').removeClass('show-share-list');
      }
    });
  }

  // --- handleClickAccordion
  const handleClickAccordion = () => {
    $('.js-calc-result-accordion').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-calc-result');

      if (_parent.hasClass('show-fields')) {
        _parent.removeClass('show-fields');
        _parent.find('.ocbc-calc-result__flds-wrapper').slideUp(300);
      } else {
        _parent.find('.ocbc-calc-result__flds-wrapper').slideDown(300);
        setTimeout(() => {
          _parent.addClass('show-fields');
        }, 250);
      }

      e.preventDefault();

    });
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      // --- trigger show
      if ($(window).width() !== _windowTemp) {
        if ($(window).width() > 768) {
          $('.ocbc-calc-result').removeClass('show-fields');
          $('.ocbc-calc-result').find('.ocbc-calc-result__flds-wrapper').removeAttr('style');
        }
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.ocbc-icon-text').length) {
      handleClickShare();
    }

    if ($('.js-calc-result-accordion').length) {
      handleClickAccordion();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCCalcResult;
