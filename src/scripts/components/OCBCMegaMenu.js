/* ------------------------------------------------------------------------------
@name: OCBC Mega Menu
@description: OCBC Mega Menu
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCMegaMenu
const OCBCMegaMenu = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleHover
  const handleHover = () => {

    // --- mouse-enter
    $('.js-mega-menu').on('mouseenter', (e) => {
      const _this = $(e.currentTarget);
      const _target = _this.find('.ocbc-header__mega-menu');

      // --- if window with > 1200
      if ($(window).width() > 1200) {
        if (_target.length && !_this.hasClass('ocbc-nav__item--mega-menu--show')) {
          _this.addClass('ocbc-nav__item--mega-menu--show');
          $('body').addClass('ocbc-mega-menu--show');
          Scrolllable.disable();
        }
      }

    });

    // --- mouse-leave
    $('.js-mega-menu').on('mouseleave', (e) => {
      const _this = $(e.currentTarget);

      // --- if window with > 1200
      if ($(window).width() > 1200) {
        $('body').removeClass('ocbc-mega-menu--show');
        _this.removeClass('ocbc-nav__item--mega-menu--show');
        Scrolllable.enable();
      }

    });

  }

  // --- handleClickLevel1 (Accordion Level 1 on Mega Menu)
  const handleClickLevel1 = () => {
    $('.js-mega-menu > .ocbc-lnk').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _selectorSiblings = $('.js-mega-menu > .ocbc-lnk');
      const _parentSiblingsClass = '.js-mega-menu';
      const _parent = _this.parents(_parentSiblingsClass);
      const _targetClass = '.ocbc-header__mega-menu';
      const _target = _parent.find(_targetClass);

      // --- if window with <= 1200
      if ($(window).width() <= 1200) {
        if (_selectorSiblings.hasClass('ocbc--mega-menu-lvl-1-showed')) {
          // --- trigger collapse level 2
          $('.js-mega-menu .ocbc-mega-menu__col').removeClass('ocbc-mega-menu-accordion-lvl-2--expand');
          $('.js-mega-menu .ocbc-mega-menu__title > .ocbc-lnk').removeClass('ocbc--mega-menu-lvl-2-showed');
          // --- /trigger collapse level 2

          $(_parentSiblingsClass).removeClass('ocbc-mega-menu-accordion-lvl-1--expand');
          setTimeout(() => {
            // --- trigger collapse level 2
            $('.ocbc-mega-menu__list').slideUp(250);
            // --- /trigger collapse level 2

            $(_targetClass).slideUp(250);
            if (_this.hasClass('ocbc--mega-menu-lvl-1-showed')) {
              _this.removeClass('ocbc--mega-menu-lvl-1-showed');
            } else {
              _selectorSiblings.removeClass('ocbc--mega-menu-lvl-1-showed');
              _this.addClass('ocbc--mega-menu-lvl-1-showed');
              _target.slideDown(250, () => {
                _parent.addClass('ocbc-mega-menu-accordion-lvl-1--expand');
              });
            }
          }, 200);

        } else {
          _this.addClass('ocbc--mega-menu-lvl-1-showed');
          _target.slideDown(250, () => {
            _parent.addClass('ocbc-mega-menu-accordion-lvl-1--expand');
          });
        }
        e.preventDefault();
      }

    });

  }

  // --- handleClickLevel2 (Accordion Level 2 on Mega Menu)
  const handleClickLevel2 = () => {
    $('.js-mega-menu .ocbc-mega-menu__title > .ocbc-lnk').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _selectorSiblings = $('.js-mega-menu .ocbc-mega-menu__title > .ocbc-lnk');
      const _parentSiblingsClass = '.js-mega-menu .ocbc-mega-menu__col';
      const _parent = _this.parents(_parentSiblingsClass);
      const _targetClass = '.ocbc-mega-menu__list';
      const _target = _parent.find(_targetClass);

      // --- if window with <= 1200
      if ($(window).width() <= 1200) {
        if (_selectorSiblings.hasClass('ocbc--mega-menu-lvl-2-showed')) {
          $(_parentSiblingsClass).removeClass('ocbc-mega-menu-accordion-lvl-2--expand');
          setTimeout(() => {
            $(_targetClass).slideUp(250);
            if (_this.hasClass('ocbc--mega-menu-lvl-2-showed')) {
              _this.removeClass('ocbc--mega-menu-lvl-2-showed');
            } else {
              _selectorSiblings.removeClass('ocbc--mega-menu-lvl-2-showed');
              _this.addClass('ocbc--mega-menu-lvl-2-showed');
              _target.slideDown(250, () => {
                _parent.addClass('ocbc-mega-menu-accordion-lvl-2--expand');
              });
            }
          }, 200);

        } else {
          _this.addClass('ocbc--mega-menu-lvl-2-showed');
          _target.slideDown(250, () => {
            _parent.addClass('ocbc-mega-menu-accordion-lvl-2--expand');
          });

        }
        e.preventDefault();
      }

    });

  }

  // --- collapseAccordtionMegaMenu
  const collapseAccordtionMegaMenu = (component) => {
    // --- trigger collapse level 2
    $('.js-mega-menu .ocbc-mega-menu__col').removeClass('ocbc-mega-menu-accordion-lvl-2--expand');
    $('.js-mega-menu .ocbc-mega-menu__title > .ocbc-lnk').removeClass('ocbc--mega-menu-lvl-2-showed');
    if (component === 'mobile-menu') {
      $('.ocbc-mega-menu__list').slideUp(250);
    } else {
      $('.ocbc-mega-menu__list').removeAttr('style');
    }
    // --- /trigger collapse level 2

    // --- trigger collapse level 1
    $('.js-mega-menu').removeClass('ocbc-mega-menu-accordion-lvl-1--expand');
    $('.js-mega-menu > .ocbc-lnk').removeClass('ocbc--mega-menu-lvl-1-showed');
    if (component === 'mobile-menu') {
      $('.ocbc-header__mega-menu').slideUp(250);
    } else {
      $('.ocbc-header__mega-menu').removeAttr('style');
    }
    // --- /trigger collapse level 1

  }

  // --- handleCheckWidth
  const handleCheckWidth = () => {
    if ($(window).width() > 1200) {
      collapseAccordtionMegaMenu();
    } else if ($(window).width() <= 1200) {
      $('body').removeClass('ocbc-mega-menu--show');
      $('.js-mega-menu').removeClass('ocbc-nav__item--mega-menu--show');
    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        handleCheckWidth();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    handleHover();
    handleClickLevel1();
    handleClickLevel2();
    handleCheckWidth();
    handleWindowResize();

  }

  // --- return
  return {
    init,
    collapseAccordtionMegaMenu
  }

})();

export default OCBCMegaMenu;
