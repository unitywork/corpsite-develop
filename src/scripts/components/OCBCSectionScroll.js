/* ------------------------------------------------------------------------------
@name: OCBC Section Scroll
@description: OCBC Section Scroll
--------------------------------------------------------------------------------- */

// --- OCBCSectionScroll
const OCBCSectionScroll = (() => {

  // dotOnClick
  let _dotOnClick = false;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleClickDot
  const handleClickDot = () => {
    $('body').on('click', () => {}).on('click', '.js-section-scroll-dot', (e) => {
      _dotOnClick = true;
      const _this = $(e.currentTarget);
      const _target = _this.attr('data-target');
      let _scrollTo = $(`[data-section="${_target}"]`).position().top;

      // if target scroll < current scroll
      if (_scrollTo < $(window).scrollTop()) {
        _scrollTo = _scrollTo - $('.ocbc-header').height();
      }

      $('.ocbc-section-scroll-dots__dot').removeClass('ocbc-section-scroll-dots__dot--active');
      _this.addClass('ocbc-section-scroll-dots__dot--active');
      $("html, body").animate({
        scrollTop: _scrollTo
      }, 750, 'swing', () => {
        _dotOnClick = false;
      });
    });
  }

  // --- handleCheckScroll
  const handleCheckScroll = () => {
    const _windowScroll = $(window).scrollTop();
    let _sectionTarget = '';
    let _sectionActive = '';

    $('.js-section-scroll').each((index, element) => {
      const _this = $(element);
      // 3/4 height windows
      const _dataSectionPos = _this.position().top - (0.25 * $(window).height());

      if (_dataSectionPos <= _windowScroll) {
        const _dataSection = _this.attr('data-section');
        _sectionTarget = _dataSection;
        _sectionActive = _dataSectionPos;
      }
    });

    if (_sectionActive <= _windowScroll) {
      if (!$(`.ocbc-section-scroll-dots__dot[data-target="${_sectionTarget}"]`).hasClass('ocbc-section-scroll-dots__dot--active') && !_dotOnClick) {
        $('.ocbc-section-scroll-dots__dot').removeClass('ocbc-section-scroll-dots__dot--active');
        $(`.ocbc-section-scroll-dots__dot[data-target="${_sectionTarget}"]`).addClass('ocbc-section-scroll-dots__dot--active');
      }
    }

    // 108 = positition dots from bottom
    const _scrollPos = $(window).scrollTop() + $(window).height() - 108;
    let _dotWhiteStart = 0;
    let _dotWhiteStop = 0;
    if ($('.ocbc-one-mobile').length) {
      _dotWhiteStart = $('.ocbc-one-mobile').position().top;
      // 24 = space dots margin 8x2 + padding 4x2
      _dotWhiteStop = $('.ocbc-one-mobile').position().top + $('.ocbc-one-mobile').height() + ($('.ocbc-section-scroll-dots').height() - 24);
    }
    if (_scrollPos >= _dotWhiteStart && _scrollPos <= _dotWhiteStop) {
      if (!$('.ocbc-section-scroll-dots').hasClass('ocbc-section-scroll-dots--white')) {
        $('.ocbc-section-scroll-dots').addClass('ocbc-section-scroll-dots--white');
      }
    } else {
      $('.ocbc-section-scroll-dots').removeClass('ocbc-section-scroll-dots--white');
    }
  }

  // --- handleCreateDots
  const handleCreateDots = () => {
    if ($('.js-section-scroll').length > 1) {
      let _blockScrollDots = '<ul class="ocbc-section-scroll-dots ocbc-section-scroll-dots--hide">';
      $('.js-section-scroll').each((index, element) => {
        const _target = $(element).attr('data-section');
        _blockScrollDots += `<li class='ocbc-section-scroll-dots__item'><button type="button" class="ocbc-section-scroll-dots__dot js-section-scroll-dot" data-target="${_target}"></button></li>`;
      });
      _blockScrollDots += '</ul>';
      $('body').append(_blockScrollDots);
      setTimeout(() => {
        $('.ocbc-section-scroll-dots').removeClass('ocbc-section-scroll-dots--hide');
      }, 50);
    }

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        if ($(window).width() > 1200) {
          handleCheckScroll();
        }
        _windowTemp = $(window).width();
      }
    });
  }

  // --- handleScroll
  const handleScroll = () => {
    $(window).scroll(() => {
      if ($(window).width() > 1200) {
        handleCheckScroll();
      }
    });

  }

  // --- init
  const init = () => {
    if ($('.js-section-scroll').length) {
      handleCreateDots();
      handleWindowResize();
      handleCheckScroll();
      handleScroll();
      handleClickDot();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCSectionScroll;
