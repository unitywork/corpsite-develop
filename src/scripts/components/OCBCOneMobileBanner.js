/* ------------------------------------------------------------------------------
@name: OCBC OneMobileBanner
@description: OCBC OneMobileBanner Event (Click, Input, etc)
--------------------------------------------------------------------------------- */

// --- OCBCOneMobileBanner
const OCBCOneMobileBanner = (() => {

  const handleClick = () => {

    $('.js-scroll-to-feature').on('click', () => {
      const _scrollTo = $('.ocbc-one-mobile-scroll').offset().top;
      $("html, body").animate({
        scrollTop: _scrollTo
      }, 500);
    });
  }

  // --- init
  const init = () => {
    if ($('.js-scroll-to-feature').length) {
      handleClick();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCOneMobileBanner;
