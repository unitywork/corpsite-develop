/* ------------------------------------------------------------------------------
@name: OCBC Card Head Product
@description: OCBC Card Head Product
--------------------------------------------------------------------------------- */

// --- OCBCCardHeadProduct
const OCBCCardHeadProduct = (() => {

  // --- selector
  const _selector = $('.js-card-head-product-list');
  const _itemLength = $('.js-card-head-product-list .ocbc-card--head-product').length;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- itemsCheck
  const itemsCheck = () => {
    // --- destroy carousel
    _selector.owlCarousel('destroy');

    let _lengthLimit = 3;
    if ($(window).width() <= 992) {
      _lengthLimit = 2;
    }

    // --- run carousel
    if ($(window).width() >= 768 && _itemLength > _lengthLimit) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      // --- set carousel
      _selector.owlCarousel({
        loop: false,
        autoWidth: true,
        mouseDrag: false,
        touchDrag: true,
        pullDrag: false,
        items: 1,
        nav: true,
        rewind: false,
        dots: false,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        autoplaySpeed: 500,
        navSpeed: 500
      });

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');
      if (_itemLength <= 3) {
        _selector.addClass('ocbc-card-head-product__list--no-carousel');
      }

    }

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        itemsCheck();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-card-head-product-list').length) {
      itemsCheck();
      handleWindowResize();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCCardHeadProduct;
