/* ------------------------------------------------------------------------------
@name: OCBC OCBCCookiesFloating
@description: OCBC OCBCCookiesFloating Event (Click, Input, etc)
--------------------------------------------------------------------------------- */

// --- OCBCCookiesFloating
const OCBCCookiesFloating = (() => {

  // handleShow
  const handleShow = () => {
    setTimeout(() => {
      $('body').addClass('ocbc-show-cookies');
    }, 2000);
  }

  // --- init
  const init = () => {
    if ($('.ocbc-cookies').length) {
      handleShow();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCCookiesFloating;
