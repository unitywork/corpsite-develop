/* ------------------------------------------------------------------------------
@name: OCBC Card Head Product
@description: OCBC Card Head Product
--------------------------------------------------------------------------------- */

// --- OCBCCardAchievement
const OCBCCardAchievement = (() => {

  // --- selector
  const _selector = $('.js-card-achievement-list');
  const _itemLength = $('.js-card-achievement-list .ocbc-card--achievement').length;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- itemsCheck
  const itemsCheck = () => {
    // --- destroy carousel
    _selector.owlCarousel('destroy');

    // --- run carousel
    if ($(window).width() >= 768 && _itemLength > 3) {
      // --- add class owl-carousel
      if (!_selector.hasClass('owl-carousel')) {
        _selector.addClass('owl-carousel');
      }

      // --- set carousel
      _selector.owlCarousel({
        loop: false,
        autoWidth: true,
        mouseDrag: false,
        touchDrag: true,
        pullDrag: true,
        nav: true,
        rewind: false,
        dots: false,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        autoplaySpeed: 500,
        navSpeed: 500
      });

    } else {
      // --- show item
      _selector.removeClass('owl-carousel');

    }

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        itemsCheck();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-card-achievement-list').length) {
      itemsCheck();
      handleWindowResize();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCCardAchievement;
