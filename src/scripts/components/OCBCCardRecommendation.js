/* ------------------------------------------------------------------------------
@name: OCBC Card Recommendation
@description: OCBC Card Recommendation
--------------------------------------------------------------------------------- */

// --- OCBCCardRecommendation
const OCBCCardRecommendation = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleHeight
  const handleHeight = () => {
    if ($('.js-card-rec').length) {
      if ($(window).width() > 767) {
        let _heightTemp = 0;
        $('.js-card-rec .ocbc-card--rec').each((index, element) => {
          const _this = $(element);
          if (_heightTemp != _this.outerHeight()) {
            _heightTemp = _this.outerHeight();
          }
        });
        if (_heightTemp > 84) {
          $('.js-card-rec .ocbc-card--rec').css('height', `${_heightTemp}px`);
        }
      } else {
        $('.js-card-rec .ocbc-card--rec').removeAttr('style');
      }
    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        handleHeight();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    handleHeight();
    handleWindowResize();
  }

  // --- return
  return {
    init
  }

})();

export default OCBCCardRecommendation;
