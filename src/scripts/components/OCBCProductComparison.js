/* ------------------------------------------------------------------------------
@name: OCBC Product Comparison
@description: OCBC Product Comparison
--------------------------------------------------------------------------------- */

// fix parent issue
// fix mobile issue
// fix scrolling issue

// --- OCBCProductComparison
const OCBCProductComparison = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // handle close product comparison
  const handleCloseProductComparison = () => {
    $('.js-saving-comparison').removeClass('ocbc-saving-comparison__section-wrapper--active');
    setTimeout(() => {
      $('.js-saving-comparison').slideUp(400);
    }, 150);

  }

  // handle open product comparison
  const handleOpenProductComparison = () => {
    $('.js-saving-comparison').slideDown(450);
    setTimeout(() => {
      $('.js-saving-comparison').addClass('ocbc-saving-comparison__section-wrapper--active');
    }, 500);

  }

  const handleAddProductComparison = () => {
    $('.js-add-product-comparison').on('click', (e) => {

    });
  }

  // handle selected product
  const handleSelectedProduct = (e) => {
    // conditional for label comparison
    const _this = $(e.currentTarget);
    const _dataProduct = _this.attr('data-product');
    const _dataImg = _this.attr('data-img');
    const _dataDescription = _this.attr('data-description');
    const _dataLinkTo = _this.attr('data-link-to');
    const _dataLinkText = _this.attr('data-link-text');
    const _dataBtnTo = _this.attr('data-button-to');
    const _dataBtnText = _this.attr('data-button-text');
    const _title = _this.parents('.ocbc-card--product-comparison').find('.ocbc-card__title').html();
    const _isChecked = _this.is(':checked');
    if (!_isChecked) {
      $('.ocbc-label-comparison__item[data-id=' + _dataProduct + ']').fadeOut(400, function () {
        $(this).remove();
      });
      $('.ocbc-card--chosen-product[data-id=' + _dataProduct + ']').fadeOut(400, function () {
        $(this).remove();
      });
      $('.ocbc-tbl-comp__data[data-product=' + _dataProduct + ']').fadeOut(400, function () {
        $(this).remove();
      });
    } else {
      // append label comparison
      $('.ocbc-label-comparison__scrollable').append('<div data-id="' + _dataProduct + '" class="ocbc-col-xl-3 ocbc-col-md-4 ocbc-col-sm-6 ocbc-label-comparison__item"><div class="ocbc-label-comparison__btn-trigger"><img class="ocbc-label-comparison__btn-ico" src="img/icon/close.svg" alt="' + _title + '"></div>' + _title + '</div>');
      // append chosen product comparison
      // conditional for maximum product comparison length
      if ($('.js-product-comparison:checked').length > 2) {
        setTimeout(() => {
          $('<div class="ocbc-col-xl-3 ocbc-col-md-4 ocbc-col-sm-6 ocbc-card ocbc-card--chosen-product" data-id="' + _dataProduct + '"><div class="ocbc-card__box"><div class="ocbc-card__img-wrapper"><img class="ocbc-card__img" src="img/dummy/card/chosen-product/' + _dataImg + '" alt="' + _dataProduct + '"></div><div class="ocbc-card__txt"><p class="ocbc-card__description">"' + _dataDescription + '"</p><a class="ocbc-btn-txt-lnk ocbc-card__btn-link" type="button" href="' + _dataLinkTo + '">' + _dataLinkText + '</a><div class="ocbc-card__btn"><a class="ocbc-btn ocbc-btn--primary" href="' + _dataBtnTo + '">' + _dataBtnText + '</a></div></div></div></div></div>').insertBefore('.ocbc-card--chosen-skeleton');
        }, 400);
      } else {
        $('<div class="ocbc-col-xl-3 ocbc-col-md-4 ocbc-col-sm-6 ocbc-card ocbc-card--chosen-product" data-id="' + _dataProduct + '"><div class="ocbc-card__box"><div class="ocbc-card__img-wrapper"><img class="ocbc-card__img" src="img/dummy/card/chosen-product/' + _dataImg + '" alt="' + _dataProduct + '"></div><div class="ocbc-card__txt"><p class="ocbc-card__description">"' + _dataDescription + '"</p><a class="ocbc-btn-txt-lnk ocbc-card__btn-link" type="button" href="' + _dataLinkTo + '">' + _dataLinkText + '</a><div class="ocbc-card__btn"><a class="ocbc-btn ocbc-btn--primary" href="' + _dataBtnTo + '">' + _dataBtnText + '</a></div></div></div></div></div>').insertBefore('.ocbc-card--chosen-skeleton');
      }
      // get data from chosen product
      getProductData(_dataProduct);

    }
  }

  // get product data
  const getProductData = (data) => {
    $('.ocbc-tbl-comp__src[data-product="' + data + '"]').each((index, element) => {
      const _this = $(element);
      const _slug = _this.attr('data-slug');
      const _product = _this.attr('data-product');
      const _text = _this.html();
      $('.ocbc-tbl-comp__row[data-title="' + _slug + '"]').append('<div class="ocbc-tbl-comp__data" data-product="' + _product + '" data-slug="' + _slug + '">' + _text + '</i></div>');

    });

  }

  // remove product from label
  const removeProductFromLabel = () => {
    $(document).on('click', '.ocbc-label-comparison__btn-trigger', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-label-comparison__item');
      const _dataID = _parent.attr('data-id');
      $(".js-product-comparison[data-product='" + _dataID + "']").prop('checked', false);
      _parent.fadeOut(400, function () {
        $(this).remove();
      });
      $('.ocbc-card--chosen-product[data-id=' + _dataID + ']').fadeOut(400, function () {
        $(this).remove();
      });
      $('.ocbc-tbl-comp__data[data-product=' + _dataID + ']').fadeOut(400, function () {
        $(this).remove();
      });
      if ($('.js-product-comparison:checked').length <= 1) {
        handleCloseProductComparison();
        handleScrollToProduct();
      }

    });
  }

  // handle scroll to product
  const handleScrollToProduct = () => {
    if ($(".ocbc-card-product-comparison").length) {
      const _headerHeight = $('.ocbc-header').innerHeight();
      $('html, body').animate({
        scrollTop: $(".ocbc-card-product-comparison").offset().top - _headerHeight
      }, 800);
    }

  }

  // handle scroll to comparison
  const handleScrollToComparison = () => {
    if ($(".js-saving-comparison").length) {
      $('html, body').animate({
        scrollTop: $(".js-saving-comparison").offset().top
      }, 800);
    }

  }

  // handle reset comparison
  const handleButtonToProduct = () => {
    $('.js-scroll-to-product').click(() => {
      handleScrollToProduct();
    });

  }

  // sticky comparison bar
  const stickyComparisonBar = () => {
    const _windowScroll = $(window).scrollTop();
    if ($('.js-saving-comparison').length) {
      const _label = $('.ocbc-saving-comparison__section--lbl-comp');
      const _triggerComparison = $('.js-saving-comparison').position().top;
      const _triggerTable = $('.ocbc-saving-comparison__section--tbl-comp').offset().top;
      const _button = $('.ocbc-label-comparison__button');
      if ($('.js-saving-comparison').is(':visible')) {
        if (_triggerTable <= _windowScroll) {
          _button.addClass('ocbc-label-comparison__button__active');
        } else {
          _button.removeClass('ocbc-label-comparison__button__active');
        }
        if (_triggerComparison <= _windowScroll) {
          _label.addClass('ocbc-saving-comparison__section--lbl-comp__active');
        } else {
          _label.removeClass('ocbc-saving-comparison__section--lbl-comp__active');
        }
      }
    }

  }

  // check maximum selected product
  const checkMaximumSelected = () => {
    $('.js-product-comparison').on('change', function (e) {

      let _maximumAllowed = 0;

      if ($(window).width() > 768) {
        _maximumAllowed = 3;
      } else {
        _maximumAllowed = 2;
      }

      // conditional for maximum product comparison length
      if ($('.js-product-comparison:checked').length > _maximumAllowed) {
        this.checked = false;
      } else {
        // select product
        if ($('.js-product-comparison:checked').length > 0) {
          handleSelectedProduct(e);
        } else {
          $('.ocbc-label-comparison__item').fadeOut(400, function () {
            $(this).remove();
          });
        }

        // conditional for product more than 1 checked
        if ($('.js-product-comparison:checked').length > 1) {
          handleOpenProductComparison();
          handleScrollToComparison();
        } else {
          if ($('.js-saving-comparison').is(':visible')) {
            handleCloseProductComparison();
            handleScrollToProduct();
          }
        }
      }

    });
  }

  // handle scroll
  const handleScroll = () => {
    $(window).scroll(() => {
      if ($('.ocbc-card-product-comparison').length) {
        stickyComparisonBar();
      }
    });

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      // --- trigger show
      if ($(window).width() !== _windowTemp) {
        checkMaximumSelected();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.ocbc-card-product-comparison').length) {
      checkMaximumSelected();
      removeProductFromLabel();
      handleScroll();
      handleButtonToProduct();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCProductComparison;
