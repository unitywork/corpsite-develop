/* ------------------------------------------------------------------------------
@name: OCBC Filter
@description: OCBC Filter
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCFilter
const OCBCFilter = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleClick
  const handleClick = () => {
    // --- show button
    $('.js-show-filter').on('click', () => {
      if ($(window).width() < 768) {
        $('.ocbc-filter').addClass('ocbc-filter--show');
        Scrolllable.disable();
      }
    });

    // --- close button
    $('.js-close-filter').on('click', () => {
      if ($(window).width() < 768) {
        $('.ocbc-filter').removeClass('ocbc-filter--show');
        Scrolllable.enable();
      }
    });

  }

  // --- handleAccordion
  const handleAccordion = () => {
    $('.js-filter-accordion').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-filter__item');

      if (_this.hasClass('ocbc--collapse')) {
        _this.removeClass('ocbc--collapse');
        _parent.find('.ocbc-filter__body').slideDown(250, () => {
          _parent.removeClass('ocbc-filter__item--accordion-collapse');
        });
      } else {
        _this.addClass('ocbc--collapse');
        _parent.addClass('ocbc-filter__item--accordion-collapse');
        setTimeout(() => {
          _parent.find('.ocbc-filter__body').slideUp(250);
        }, 300);
      }

    })
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        $('.js-filter-accordion').removeClass('ocbc--collapse');
        $('.ocbc-filter__item').removeClass('ocbc-filter__item--accordion-collapse');
        $('.ocbc-filter__body').removeAttr('style');
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.ocbc-filter').length) {
      handleClick();
      handleAccordion();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCFilter;
