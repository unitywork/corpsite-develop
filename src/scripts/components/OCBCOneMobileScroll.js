/* ------------------------------------------------------------------------------
@name: OCBC One Mobile Scroll
@description: OCBC One Mobile Scroll
--------------------------------------------------------------------------------- */

// --- OCBCOneMobileScroll
const OCBCOneMobileScroll = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleSetHeight
  const handleSetHeight = () => {
    if ($(window).width() > 992) {
      const _sectionHeight = $(window).height();
      $('.ocbc-one-mobile-scroll__section').height(_sectionHeight);
    } else {
      $('.ocbc-one-mobile-scroll__section').removeAttr('style');
    }
  }

  // --- handleRunSkrollr
  const handleRunSkrollr = () => {

    if ($(window).width() > 992) {
      const _banner = $('.js-main-banner').outerHeight();
      skrollr.init({
        constants: {
          view: _banner
        },
        mobileCheck: function () {
          return false;
        },
        forceHeight: false,
        smoothScrolling: true,
        smoothScrollingDuration: 500
      });
    } else {
      skrollr.init().destroy();
    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        handleSetHeight();
        handleRunSkrollr();
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-one-mobile-scroll').length) {
      handleSetHeight();
      handleRunSkrollr();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCOneMobileScroll;
