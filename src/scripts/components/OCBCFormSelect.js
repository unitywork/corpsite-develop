/* ------------------------------------------------------------------------------
@name: OCBC Form Select
@description: OCBC Form Select
--------------------------------------------------------------------------------- */


// --- utilities
import {
  WHITESPACE
} from 'variables';

// --- OCBCFormSelect
const OCBCFormSelect = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleChange
  const handleChange = () => {
    $('body').on('click', () => {
      // --- click item
    }).on('click', '.js-select .ocbc-select__item', (e) => {
      const _this = $(e.currentTarget);
      const _value = _this.attr('data-value');
      const _parents = _this.parents('.js-select');

      if (_value != '') {
        _parents.find('.ocbc-select__item--selected').removeClass('ocbc-select__item--selected');
        _this.addClass('ocbc-select__item--selected');
        _parents.find(`.ocbc-select__element option[value="${_value}"]`).prop('selected', true);
        _parents.find('.ocbc-select__element').trigger('change');
        _parents.find('.ocbc-select--show').removeClass('ocbc-select--show');
      }

      // --- change
    }).on('change', '.js-select .ocbc-select__element', (e) => {
      const _this = $(e.currentTarget);
      const _value = _this.val();
      const _text = _this.find('option:selected').text();
      const _parents = _this.parents('.js-select');

      if (!WHITESPACE.test(_value)) {
        _parents.addClass('ocbc-select--filled')
      } else {
        _parents.removeClass('ocbc-select--filled')
      }

      _parents.find('.ocbc-select__text').text(_text);
      _parents.find('.ocbc-select__item').removeClass('ocbc-select__item--selected');
      _parents.find(`.ocbc-select__item[data-value="${_value}"]`).addClass('ocbc-select__item--selected');

    });
  }

  // --- handleClick
  const handleClick = () => {
    // --- hide
    $('body').on('click', () => {
      if ($('.js-select .ocbc-select').hasClass('ocbc-select--show')) {
        setTimeout(() => {
          $('.js-select .ocbc-select--show').removeClass('ocbc-select--show');
        }, 5);
      }

      // --- stop propagation
    }).on('click', '.js-select .ocbc-select__list', (e) => {
      e.stopPropagation();

      // --- show
    }).on('click', '.js-select .ocbc-select__text', (e) => {
      const _this = $(e.currentTarget);
      const _parents = _this.parents('.js-select');

      if (_parents.find('.ocbc-select').hasClass('ocbc-select--show')) {
        $('.js-select .ocbc-select--show').removeClass('ocbc-select--show');
      } else {
        $('.js-select .ocbc-select--show').removeClass('ocbc-select--show');
        setTimeout(() => {
          _parents.find('.ocbc-select').addClass('ocbc-select--show');
        }, 5);
      }


    });
  }

  // --- handleChangeTag
  const handleChangeTag = () => {
    $('.js-select').each((index, element) => {
      const _this = $(element);
      const _width = _this.attr('data-width');
      const _select = _this.find('select');
      const _label = _select.attr('data-label');
      const _option = _select.find('option');
      let _list = '';
      let _classSelect = 'ocbc-select';

      if (_width != undefined) {
        _classSelect += ' ocbc-select--full';
      }

      _list = `<div class='${_classSelect}'><p class='ocbc-select__text'>${_label}</p><div class='ocbc-select__list'>`;
      _option.each((idx, el) => {
        if ($(el).attr('data-type') == 'sub') {
          _list += `<div class='ocbc-select__item ocbc-select__item-sub' data-value="${$(el).val()}"><p class='ocbc-select__item__text'>${$(el).text()}</p></div>`;
        } else {
          _list += `<div class='ocbc-select__item' data-value="${$(el).val()}"><p class='ocbc-select__item__text'>${$(el).text()}</p></div>`;
        }
      });
      _list += '</div></div>';
      _this.prepend(_list);
      // label
      $('.ocbc-select__item.ocbc-select__item-sub').prev().not('.ocbc-select__item.ocbc-select__item-sub').addClass('ocbc-select__item--sub-label');

      handleCustomScrollbar();
    });

  }

  // --- handleDefault
  const handleDefault = () => {
    $('.js-select .ocbc-select__element').trigger('change');
  }

  // --- handleSelectCity
  const handleSelectCity = () => {

    $('.js-select-city').select2({
      placeholder: $('.js-select-city').attr('placeholder'),
      dropdownParent: $('.js-select-city').parents('.ocbc-select-search')
    });

    $('.js-select-city').on('select2:open', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-select-search');
      const _dropdown = _parent.find('.select2-results');
      _parent.find('.select2-results ul.select2-results__options').unbind('mousewheel');
      _dropdown.mCustomScrollbar({
        autoHideScrollbar: false,
        scrollButtons: {
          enable: true
        },
        mouseWheel: true
      });
    });

  }

  // --- handleCustomScrollbar
  const handleCustomScrollbar = (selector) => {
    if ($(window).width() > 1200) {
      $('.js-select .ocbc-select__list').mCustomScrollbar({
        autoHideScrollbar: false
      });
    } else {
      $('.js-select .ocbc-select__list').mCustomScrollbar('destroy');
    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {

        handleCustomScrollbar();
        _windowTemp = $(window).width();
      }
    });

  }

  // --- init
  const init = () => {
    // - select
    if ($('.js-select').length) {
      handleChangeTag();
      handleClick();
      handleChange();
      handleDefault();
      handleWindowResize();
    }

    // - select-city
    if ($('.js-select-city').length) {
      handleSelectCity();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCFormSelect;
