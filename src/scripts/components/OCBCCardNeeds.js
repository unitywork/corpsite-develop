/* ------------------------------------------------------------------------------
@name: OCBC Card Needs
@description: OCBC Card Needs
--------------------------------------------------------------------------------- */

// -- import components
import {
  OCBCAutoSlide
} from "components";


// --- OCBCCardNeeds
const OCBCCardNeeds = (() => {

  // --- selector
  const _carouselSelector = $('.js-card-needs-list');
  const _itemLength = $('.js-card-needs-list .ocbc-card--needs').length;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleCardClick
  const handleCardClick = () => {
    // --- show and scroll focus
    $('body').on('click', () => {}).on('click', '.js-card-needs', (e) => {
      const _this = $(e.currentTarget);
      const _parentClass = '.ocbc-card--needs';
      const _parent = _this.parents(_parentClass);
      const _parentSibligs = $(_parentClass);
      const _target = _this.attr('data-target');
      const _carouselItemHide = !Boolean(Number(_this.parents('.owl-item').next('.owl-item.active').length));
      let _scrollTo = $('.ocbc-card-needs').offset().top - 24;

      // -- trigger next carousel
      if (_carouselItemHide) {
        _carouselSelector.trigger('next.owl.carousel');
      }

      if ($('.ocbc-scroll-down').length && $(window).scrollTop() != _scrollTo && $(window).width() >= 768) {
        _scrollTo = _scrollTo - $('.ocbc-header').height();
      }

      // --- scroll to section
      $("html, body").animate({
        scrollTop: _scrollTo
      }, 500);

      if (_parentSibligs.hasClass('ocbc-card--needs--showed')) {
        if (_parent.hasClass('ocbc-card--needs--showed')) {
          // special case if card need as tabs
          if (!_this.parents('.ocbc-needs-container').hasClass('ocbc-needs-container--as-tabs')) {
            $('.ocbc-presentation-needs').removeClass('ocbc-presentation-needs--expand');
            setTimeout(() => {
              _parent.removeClass('ocbc-card--needs--showed');
              $('.ocbc-presentation-needs').slideUp(350, () => {
                $('.ocbc-presentation-needs__pane').removeClass('ocbc-presentation-needs__pane--active').removeAttr('style');
              });

              // special case
              if ($('.ocbc-presentation-needs__pane[data-presentation="pengumuman-penting-ocbc"').length) {
                $('.ocbc-presentation-needs').removeClass('ocbc-presentation-needs--no-pb');
              }

              // --- trigger destroy autoslide
              OCBCAutoSlide.destroy();

            }, 200);
          }
        } else {
          $(_parentSibligs).removeClass('ocbc-card--needs--showed');
          $('.ocbc-presentation-needs__pane').removeClass('ocbc-presentation-needs__pane--active');
          _parent.addClass('ocbc-card--needs--showed');
          setTimeout(() => {
            $('.ocbc-presentation-needs__pane').removeAttr('style');
            $(`.ocbc-presentation-needs__pane[data-presentation="${_target}"]`).show(0).addClass('ocbc-presentation-needs__pane--active');
            $('.ocbc-presentation-needs').slideDown(350, () => {
              $('.ocbc-presentation-needs').addClass('ocbc-presentation-needs--expand');
            });

            // special case
            if (_target == 'pengumuman-penting-ocbc') {
              $('.ocbc-presentation-needs').addClass('ocbc-presentation-needs--no-pb');
            }

            // --- trigger run autoslide
            if ($('.js-auto-slide').parents('.ocbc-presentation-needs__pane').hasClass('ocbc-presentation-needs__pane--active')) {
              OCBCAutoSlide.run();
            }
            // --- trigger destroy autoslide
            else {
              OCBCAutoSlide.destroy();
            }

          }, 250);
        }
      } else {
        _parent.addClass('ocbc-card--needs--showed');
        $(`.ocbc-presentation-needs__pane[data-presentation="${_target}"]`).show(0).addClass('ocbc-presentation-needs__pane--active');
        $('.ocbc-presentation-needs').slideDown(350, () => {
          $('.ocbc-presentation-needs').addClass('ocbc-presentation-needs--expand');
        });

        // special case
        if (_target == 'pengumuman-penting-ocbc') {
          $('.ocbc-presentation-needs').addClass('ocbc-presentation-needs--no-pb');
        }

        // --- trigger run autoslide
        if ($('.js-auto-slide').parents('.ocbc-presentation-needs__pane').hasClass('ocbc-presentation-needs__pane--active')) {
          OCBCAutoSlide.run();
        }
        // --- trigger destroy autoslide
        else {
          OCBCAutoSlide.destroy();
        }

      }

      // --- auto scroll left in mobile (focus card)
      if ($(window).width() < 768) {
        handleCardScrollLeft();

      }

    });

    // --- close
    $('.js-close-presentation-needs').on('click', (e) => {
      $('.ocbc-presentation-needs').removeClass('ocbc-presentation-needs--expand');
      setTimeout(() => {
        $('.ocbc-card--needs').removeClass('ocbc-card--needs--showed');
        // special case
        if ($('.ocbc-presentation-needs__pane[data-presentation="pengumuman-penting-ocbc"').length) {
          $('.ocbc-presentation-needs').removeClass('ocbc-presentation-needs--no-pb');
        }
        $('.ocbc-presentation-needs').slideUp(350, () => {
          $('.ocbc-presentation-needs__pane').removeClass('ocbc-presentation-needs__pane--active').removeAttr('style');
        });
      }, 200);
    });

  }

  // --- handleCardCarouselFocus
  const handleCardCarouselFocus = () => {
    const _index = $('.ocbc-card--needs--showed').parents('.owl-item').not('.cloned').index() - 5;
    _carouselSelector.trigger('to.owl.carousel', [_index, 1, true]);
  }

  // --- handleCardScrollLeft
  const handleCardScrollLeft = () => {
    const _getPosLeft = document.querySelector(
      '.ocbc-card--needs--showed'
    ).offsetLeft;
    $('.js-card-needs-list').animate({
      scrollLeft: _getPosLeft
    }, 300);

  }

  // --- handleFocusCard
  const handleFocusCard = () => {
    setTimeout(() => {
      if ($('.ocbc-card--needs--showed').length) {
        // --- auto scroll left in mobile (focus card)
        if ($(window).width() < 768) {
          handleCardScrollLeft();
        } else {
          handleCardCarouselFocus();
        }
      }
    }, 50);

  }

  // --- handleItemsCheck
  const handleItemsCheck = () => {
    // --- destroy carousel
    _carouselSelector.owlCarousel('destroy');

    let _itemsCount = 5;
    if ($(window).width() >= 1868) {
      _itemsCount = 5;
    } else if ($(window).width() >= 1280) {
      _itemsCount = 4;
    } else if ($(window).width() >= 1024) {
      _itemsCount = 3;
    } else if ($(window).width() >= 768) {
      _itemsCount = 2;
    }

    // --- run carousel
    if ($(window).width() >= 768 && _itemLength > _itemsCount) {
      // --- add class owl-carousel
      if (!_carouselSelector.hasClass('owl-carousel')) {
        _carouselSelector.addClass('owl-carousel');
      }

      // --- set carousel
      _carouselSelector.owlCarousel({
        loop: true,
        autoWidth: true,
        mouseDrag: false,
        touchDrag: true,
        pullDrag: true,
        nav: true,
        rewind: false,
        dots: false,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        autoplaySpeed: 500,
        navSpeed: 500
      });

    } else {
      // --- show item
      _carouselSelector.removeClass('owl-carousel');

    }

  }

  // --- handleAsTabs
  const handleAsTabs = () => {
    if ($('.ocbc-needs-container--as-tabs').length) {
      const _cardNeeds = $('.ocbc-card--needs:first-child .js-card-needs');
      const _cardNeedsTarget = _cardNeeds.attr('data-target');
      const _presentationPane = $('.ocbc-presentation-needs__pane:first');
      const _presentationTarget = _presentationPane.attr('data-presentation');

      $(`.js-card-needs[data-target="${_cardNeedsTarget}"]`).parents('.ocbc-card--needs').addClass('ocbc-card--needs--showed');
      $(`.ocbc-presentation-needs__pane[data-presentation="${_presentationTarget}"]`).show(0).addClass('ocbc-presentation-needs__pane--active');
      $('.ocbc-presentation-needs').slideDown(350, () => {
        $('.ocbc-presentation-needs').addClass('ocbc-presentation-needs--expand');
      });
    }
  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      if ($(window).width() !== _windowTemp) {
        handleItemsCheck();
        _windowTemp = $(window).width();
        setTimeout(() => {
          handleFocusCard();
        }, 100);
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-card-needs-list').length) {
      handleWindowResize();
      handleItemsCheck();
      handleCardClick();
      handleAsTabs();

    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCCardNeeds;
