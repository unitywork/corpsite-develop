/* ------------------------------------------------------------------------------
@name: OCBC Contact Us
@description: OCBC Contact Us
--------------------------------------------------------------------------------- */

// --- OCBCContactUs
const OCBCContactUs = (() => {

  // --- handleSetTargetCallButton
  const handleSetTargetCallButton = () => {
    const _getInternationalPhone = $('.js-call-ocbc-nisp').attr('data-phone-international');

    $.getJSON('http://www.geoplugin.net/json.gp?jsoncallback=?', (data) => {
      const _data = data;
      const _country = _data.geoplugin_countryCode;
      if (_country != 'ID') {
        $('.js-call-ocbc-nisp').attr('href', _getInternationalPhone);
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.js-call-ocbc-nisp').length) {
      handleSetTargetCallButton();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCContactUs;
