/* ------------------------------------------------------------------------------
@name: OCBC Header Search
@description: OCBC Header Search
--------------------------------------------------------------------------------- */

// -- import components
import {
  OCBCMobileMenu,
  OCBCMegaMenu
} from "components";

// --- OCBCHeaderLogin
const OCBCHeaderLogin = (() => {

  // --- handleHover
  const handleHover = () => {
    // --- on desktop
    $('.js-header-login-show').hover(
      // --- mouse-over
      (e) => {
        const _this = $(e.currentTarget);
        if ($(window).width() > 1200) {
          _this.addClass("show-menu");
        }
      },

      // --- mouse-out
      (e) => {
        const _this = $(e.currentTarget);
        if ($(window).width() > 1200) {
          _this.removeClass("show-menu");
        }
      }
    );
  }

  // --- handleClick
  const handleClick = () => {
    // --- on tablet show
    $('.js-header-login-show > a.ocbc-btn-icon-txt').on('click', (e) => {
      const _this = $(e.currentTarget)
      const _headerLogin = _this.parents('.js-header-login-show');
      if ($(window).width() <= 1200) {
        if ($('.ocbc-show-navigation').length) {
          if ($('.js-burger-menu').hasClass('ocbc-burger-menu--close')) {
            // --- trigger hidenav
            OCBCMobileMenu.handleHideNav();
            // --- trigger collapse
            OCBCMegaMenu.collapseAccordtionMegaMenu('mobile-menu');
          }
          setTimeout(() => {
            _headerLogin.addClass('show-menu');
          }, 700);
        } else {
          _headerLogin.toggleClass('show-menu');
        }

        e.preventDefault();
        e.stopPropagation();
      }
    });

    // --- on tablet hide
    $('body').on('click', () => {
      if ($(window).width() <= 1200 && $('.js-header-login-show').hasClass('show-menu')) {
        $('.js-header-login-show').removeClass('show-menu');
      }
    });

  }

  // --- init
  const init = () => {
    handleHover();
    handleClick();

  }

  // --- return
  return {
    init
  }

})();

export default OCBCHeaderLogin;
