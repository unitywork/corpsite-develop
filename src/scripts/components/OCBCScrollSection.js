/* ------------------------------------------------------------------------------
@name: OCBC ScrollSection
@description: OCBC ScrollSection Event (Click, Input, etc)
--------------------------------------------------------------------------------- */

// --- OCBCScrollSection
const OCBCScrollSection = (() => {

  const handleClick = () => {

    $('.js-scroll-section').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _attr = _this.attr('target-section');

      const _scrollTo = $(".ocbc-scroll-section[data-target='"+_attr+"']").offset().top;
      $("html, body").animate({
        scrollTop: _scrollTo
      }, 500);
    });
  }

  // --- init
  const init = () => {
    if ($('.js-scroll-section').length) {
      handleClick();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCScrollSection;
