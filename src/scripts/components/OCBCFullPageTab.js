/* ------------------------------------------------------------------------------
@name: OCBC Full Page Tab
@description: OCBC Full Page Tab
--------------------------------------------------------------------------------- */

// --- OCBCFullPageTab
const OCBCFullPageTab = (() => {

  const initTab = () => {
    $('.ocbc-full-page-tab__item').first().addClass('ocbc-full-page-tab__item--active');
    $('.ocbc-full-page-tab__content').first().addClass('ocbc-full-page-tab__content--active');
  }

  const onTabClick = () => {
    $('.ocbc-full-page-tab__item').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _selectedId = _this.data('id') - 1;
      const _item = $('.ocbc-full-page-tab__item');
      const _content = $('.ocbc-full-page-tab__content');

      _item.removeClass('ocbc-full-page-tab__item--active');
      _this.addClass('ocbc-full-page-tab__item--active');

      _content.removeClass('ocbc-full-page-tab__content--active');
      _content.eq(_selectedId).addClass('ocbc-full-page-tab__content--active');

    });

  }

  // --- init
  const init = () => {
    if($('.ocbc-full-page-tab')) {
      initTab();
      onTabClick();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCFullPageTab;
