/* ------------------------------------------------------------------------------
@name: OCBC Range Slider
@description: OCBC Range Slider
--------------------------------------------------------------------------------- */

// --- OCBCRangeSlider
const OCBCRangeSlider = (() => {
 
  // --- selector
  const _selector = $('.js-range-slider');

  // --- handleInitialize
  const handleInitialize = () => {
    //default range slider
    _selector.ionRangeSlider({
      skin: "round",
      step: 1, // default 1 (set step)
      grid_snap: false, // default false (snap grid to step)
      onStart: function (data) {
        handleStart(data)
      },
      onChange: function (data) {
        handleChange(data)
      },
      onFinish: function (data) {
        handleFinish(data)
      }
    });
  }

  // --- handleStart
  const handleStart = (data) => {
    const _classname = data.slider[0].className,
      _target = _classname.replace(/\s/g, '.');

    if (data.min === data.from) {
      $(`.${_target.concat(' .irs-single')}`).css('opacity', '0');
    } else {
      $(`.${_target.concat(' .irs-single')}`).css('opacity', '1');
    }
  }

  // --- handleChange
  const handleChange = (data) => {
    const _classname = data.slider[0].className,
      _target = _classname.replace(/\s/g, '.');
    let _max = parseInt(data.max) - 5,
      _min = parseInt(data.min) + 5;

    // if max value is 20
    if(data.max === 20){
      _max = parseInt(data.max) - 2,
      _min = parseInt(data.min) + 2;
    }

    $(`.${_target.concat(' .irs-single')}`).css('opacity', '1');

    // left align
    if (_min > data.from) {
      $(`.${_target.concat(' .irs-single')}`).css({
        'left': 0,
        'right': 'auto',
      });
    }
    // right align
    else if (_max < data.from) {
      $(`.${_target.concat(' .irs-single')}`).css({
        'left': 'auto',
        'right': 0
      });
    } else {
      $(`.${_target.concat(' .irs-single')}`).css('right', 'auto');
    }
  }

  // --- handleFinish
  const handleFinish = (data) => {
    const _classname = data.slider[0].className,
      _target = _classname.replace(/\s/g, '.');

    // --- if user select min value
    if (data.min === data.from) {
      $(`.${_target.concat(' .irs-single')}`).css({
        'left': 0,
        'right': 'auto',
      });
    }
    // --- if user select max value
    else if (data.max === data.from) {
      $(`.${_target.concat(' .irs-single')}`).css({
        'left': 'auto',
        'right': 0
      });
    }
  }

  // --- init
  const init = () => {
    if ($('.js-range-slider')) {
      handleInitialize();
    }
  }

  // --- return
  return {
    init,
    handleStart,
    handleChange,
    handleFinish
  }

})();

export default OCBCRangeSlider;
