/* ------------------------------------------------------------------------------
@name: OCBC MobileTabCurrency
@description: OCBC MobileTabCurrency Event (Click, Input, etc)
--------------------------------------------------------------------------------- */

// --- OCBCMobileTabCurrency
const OCBCMobileTabCurrency = (() => {

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleMobileTabCurrency
  const handleMobileTabCurrency = () => {

    $('.js-xr-tbl-tab').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _data = _this.attr('data-type-id');

      $('.ocbc-widget-xr-tbl__data[data-type=' + _data + ']').show();
      $('.ocbc-widget-xr-tbl__data[data-type!=' + _data + ']').hide();

      $('.ocbc-widget-xr-tbl__tab-btn').removeClass('ocbc-widget-xr-tbl__tab-btn--active');
      _this.addClass('ocbc-widget-xr-tbl__tab-btn--active');

    });

  }

  // --- handleWindowResize
  const handleWindowResize = () => {
    $(window).resize(() => {
      // --- trigger show
      if ($(window).width() !== _windowTemp) {
        if ($(window).width() > 768) {
          $('.ocbc-widget-xr-tbl__data').removeAttr('style');
          $('.ocbc-widget-xr-tbl__tab-btn').removeClass('ocbc-widget-xr-tbl__tab-btn--active');
          $('.ocbc-widget-xr-tbl__tab-btn[data-type-id="bn"]').addClass('ocbc-widget-xr-tbl__tab-btn--active');
        }
        _windowTemp = $(window).width();
      }
    });
  }

  // --- init
  const init = () => {
    if ($('.ocbc-widget-xr-tbl').length) {
      handleMobileTabCurrency();
      handleWindowResize();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCMobileTabCurrency;
