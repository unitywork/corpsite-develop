/* ------------------------------------------------------------------------------
@name: OCBC About Us
@description: About Us
--------------------------------------------------------------------------------- */

// --- utilities
import {
  Scrolllable
} from 'utilities';

// --- OCBCAboutUs
const OCBCAboutUs = (() => {

  // --- handleClickHistory
  const handleClickHistory = () => {
    $('.js-history-about-us').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _parent = _this.parents('.ocbc-company-history__ctrl__item');
      const _target = _this.attr('data-target');

      if (!_parent.hasClass('ocbc-company-history__ctrl__item--active')) {
        $('.ocbc-company-history__ctrl__item').removeClass('ocbc-company-history__ctrl__item--active');
        _parent.addClass('ocbc-company-history__ctrl__item--active');

        $('.ocbc-company-history__pane').fadeOut(150);
        setTimeout(() => {
          $('.ocbc-company-history__pane').removeClass('ocbc-company-history__pane--active');
          $(`.ocbc-company-history__pane[data-pane=${_target}]`).fadeIn(300);
        }, 150);
      }

    });
  }

  // --- handleShowPopupBOD
  const handleShowPopupBOD = () => {
    $('.js-show-popup-bod').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _name = _this.find('.ocbc-card__name').html();
      const _position = _this.find('.ocbc-card__position').html();
      const _photo = _this.attr('data-popup-photo');
      const _body = _this.attr('data-popup-body');

      $('.ocbc-popup-bod .ocbc-popup-bod__img').attr({
        'src': _photo,
        'alt': _name
      });

      $('.ocbc-popup-bod .ocbc-popup-bod__name').html(_name);
      $('.ocbc-popup-bod .ocbc-popup-bod__position').html(_position);
      $('.ocbc-popup-bod .ocbc-popup-bod__body').html(_body);
      $('body').addClass('ocbc-popup-bod-show');
      Scrolllable.disable();

    });

  }

  // --- handleHidePopupBOD
  const handleHidePopupBOD = () => {
    $('.js-hide-popup-bod').on('click', (e) => {
      $('body').removeClass('ocbc-popup-bod-show');
      Scrolllable.enable();
    });

  }

  // handleYearsCarousel
  const handleYearsCarousel = () => {
    const _historyCarousel = new Swiper('.swiper-container', {
      calculateHeight: true,
      slidesPerView: 3,
      spaceBetween: 72,
      direction: 'vertical',
      loop: false,
      noSwiping: true,
      allowTouchMove: false,
      updateOnWindowResize: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        320: {
          spaceBetween: 56
        },
        1024: {
          spaceBetween: 48
        },
        1200: {
          spaceBetween: 72
        }
      }
    })

    _historyCarousel.on('slideChange', () => {
      const _swiperActive = $('.ocbc-company-history__ctrl__item--active').parent('.swiper-slide');
      const _swiperWrapper = _swiperActive.parent('.swiper-wrapper');
      const _isSlideNext = _historyCarousel.activeIndex > _historyCarousel.previousIndex;
      let _slideIndex = 0;

      if (_swiperActive.index() == _historyCarousel.previousIndex) {
        _slideIndex = _historyCarousel.activeIndex;
      } else if (_swiperActive.index() < _historyCarousel.previousIndex) {
        if (!_isSlideNext && _swiperActive.index() == 0) {
          return false;
        } else {
          _slideIndex = _historyCarousel.activeIndex - 1;
        }
      } else if (_swiperActive.index() == _historyCarousel.previousIndex + 2) {
        _slideIndex = _historyCarousel.activeIndex + 2;
      }

      $('.ocbc-company-history__ctrl__item').removeClass('ocbc-company-history__ctrl__item--active');
      _swiperWrapper.find('.swiper-slide').eq(_slideIndex).find('.ocbc-company-history__ctrl__item').find('.js-history-about-us').trigger('click');

    });

  }

  // --- init
  const init = () => {

    if ($('.ocbc-company-history').length) {
      handleYearsCarousel();
      handleClickHistory();
    }

    if ($('.ocbc-popup-bod').length) {
      handleShowPopupBOD();
      handleHidePopupBOD();
    }

  }

  // --- return
  return {
    init
  }

})();

export default OCBCAboutUs;
