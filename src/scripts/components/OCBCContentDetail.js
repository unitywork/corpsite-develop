/* ------------------------------------------------------------------------------
@name: OCBC Content Detail
@description: OCBC Content Detail
--------------------------------------------------------------------------------- */

// --- OCBCContentDetail
const OCBCContentDetail = (() => {

  const _selectorCarousel = $('.js-content-detail-image');
  const _itemLength = $('.js-content-detail-image .ocbc-content-detail__img__item').length;

  // --- windowTemp
  let _windowTemp = $(window).width();

  // --- handleSetAttributeShare
  const handleSetAttributeShare = () => {
    const _url = window.location.href;
    const _facebookShare = `https://www.facebook.com/sharer/sharer.php?u=${_url}`;
    const _twitterShare = `https://twitter.com/intent/tweet?url=${_url}`;
    let _whatsappShare = `whatsapp://send/?text=${_url}`;
    const _lineShare = `https://social-plugins.line.me/lineit/share?url=${_url}`;
    const _linkedinShare = `https://www.linkedin.com/shareArticle?mini=true&url=${_url}`;

    if ($('.ocbc-content-detail').length) {
      const _titleDetail = $('.ocbc-content-detail .ocbc-content-detail__title').text();
      _whatsappShare = `whatsapp://send/?text=${_titleDetail} ${_url}`;
    }

    // facebook
    $('.js-share-content .ocbc-share-content__item--facebook .ocbc-lnk').attr('href', _facebookShare);
    // twitter
    $('.js-share-content .ocbc-share-content__item--twitter .ocbc-lnk').attr('href', _twitterShare);
    // whatsapp
    $('.js-share-content .ocbc-share-content__item--whatsapp .ocbc-lnk').attr('href', _whatsappShare);
    // line
    $('.js-share-content .ocbc-share-content__item--line .ocbc-lnk').attr('href', _lineShare);
    // linkedin
    $('.js-share-content .ocbc-share-content__item--linkedin .ocbc-lnk').attr('href', _linkedinShare);
    // copy-link
    $('.js-share-content .ocbc-share-content__btn').attr('data-clipboard-text', _url);

  }

  // --- handleSetClipboard
  const handleSetClipboard = () => {
    const _promoCode = $('.js-copy-code-promo').parents('.ocbc-card--code-promo').find('.ocbc-card__promo-code__code').text();
    $('.js-copy-code-promo').attr('data-clipboard-text', _promoCode);

  }

  // --- handleClick
  const handleClick = () => {

    const _clipboardLink = new ClipboardJS('.js-copy-code');
    const _clipboardPromoCode = new ClipboardJS('.js-copy-code-promo');

    // -- copy code link
    $('.js-copy-code').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _textCopied = _this.attr('data-text-copied');
      if (_textCopied != undefined) {
        const _textBefore = _this.find('.ocbc-share-content__btn__txt').text();
        _this.find('.ocbc-share-content__btn__txt').text(_textCopied);
        setTimeout(function () {
          _this.find('.ocbc-share-content__btn__txt').text(_textBefore);
        }, 5000);
      } else {
        if (_this.hasClass('ocbc-share-content__btn')) {
          $('.ocbc-share-content__btn').addClass('ocbc-share-content__btn--copied');
          setTimeout(function () {
            $('.ocbc-share-content__btn').removeClass('ocbc-share-content__btn--copied');
          }, 5000);
        }
      }

    });

    // -- copy promo code
    $('.js-copy-code-promo').on('click', (e) => {
      const _this = $(e.currentTarget);
      const _textCopied = _this.attr('data-text-copied');
      const _textBefore = _this.find('.ocbc-card__promo-code__txt').text();
      _this.find('.ocbc-card__promo-code__txt').text(_textCopied);
      setTimeout(function () {
        _this.find('.ocbc-card__promo-code__txt').text(_textBefore);
      }, 5000);
    });

  }


  // --- handleCarouselImages
  const handleCarouselImages = () => {
    // --- run carousel
    if (_itemLength >= 2) {
      // --- add class owl-carousel
      if (!_selectorCarousel.hasClass('owl-carousel')) {
        _selectorCarousel.addClass('owl-carousel');
      }

      // --- set carousel
      _selectorCarousel.owlCarousel({
        loop: true,
        items: 1,
        mouseDrag: false,
        touchDrag: true,
        pullDrag: true,
        nav: true,
        rewind: false,
        dots: false,
        autoHeight: true,
        autoplay: true,
        autoplayTimeout: 7000,
        autoplayHoverPause: true,
        autoplaySpeed: 500,
        navSpeed: 500,
        dragEndSpeed: 500,
        responsive: {
          0: {
            nav: false
          },
          768: {
            nav: true
          }
        }
      });

    } else {
      // --- show item
      _selectorCarousel.removeClass('owl-carousel');

    }
  }

  // --- init
  const init = () => {
    if ($('.js-share-content').length) {
      handleSetAttributeShare();
    }
    if ($('.js-copy-code').length || $('.js-copy-code-promo')) {
      handleClick();
      handleSetClipboard();
    }
    if ($('.js-content-detail-image').length) {
      handleCarouselImages();
    }
  }

  // --- return
  return {
    init
  }

})();

export default OCBCContentDetail;
