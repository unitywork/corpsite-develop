/* ------------------------------------------------------------------------------
@name: Scrolllable
@description: Scrolllable
--------------------------------------------------------------------------------- */

// --- Scrolllable
const Scrolllable = (() => {
  // --- handleEnable
  const handleEnable = () => {
    $('body').removeClass('ocbc-rm-scroll');
    // --- vendor scrollLock for solve (position changed when on hover) in window/mac show scrollbar
    scrollLock.enablePageScroll();
  }

  // --- handleDisable
  const handleDisable = () => {
    if ($(window).width() <= 1200) {
      $('body').addClass('ocbc-rm-scroll');
    } else {
      // --- vendor scrollLock for solve (position changed when on hover) in window/mac show scrollbar
      scrollLock.setFillGapMethod('padding');
      const _fillGapHeader = document.querySelector('.ocbc-header');
      scrollLock.addFillGapTarget(_fillGapHeader);

      const _fillGapMegaMenu = document.querySelector('.ocbc-header__mega-menu');
      scrollLock.addFillGapTarget(_fillGapMegaMenu);

      scrollLock.disablePageScroll();

    }
  }

  // --- return
  return {
    enable: handleEnable,
    disable: handleDisable
  }

})();

export default Scrolllable;
