/* ------------------------------------------------------------------------------
@name: CurrencyNumber
@description: CurrencyNumber
--------------------------------------------------------------------------------- */

// --- CurrencyNumber
const CurrencyNumber = (() => {
  // --- handleFormat
  const handleFormat = (number, separator) => {
    let _numberString = String(number).replace(/[^,\d]/g, '').replace(/,/g, '').toString(),
      _mod = _numberString.length % 3,
      _currency = _numberString.substr(0, _mod),
      _thousand = _numberString.substr(_mod).match(/\d{3}/gi),
      _separator = '';

    // _thousand
    if (_thousand) {
      _separator = _mod ? separator : '';
      _currency += _separator + _thousand.join(separator);
    }
    return _currency;
  }

  // --- return
  return {
    format: handleFormat
  }

})();

export default CurrencyNumber;
