var isMobile = {
  Android: function () {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function () {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function () {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function () {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function () {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: function () {
    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  }
};

// DesignSystem
var DesignSystem = {

  // handleClickMenuItemLink
  handleClickMenuItemLink: function () {

    $('.js-ds-menu-list-link').on('click', function () {
      var _this = $(this),
        _parent = _this.parents('.ocbc-ds__menu-list__item'),
        _target = _this.attr('href').split('#')[1],
        _scrollTo = $('[data-ds-item="' + _target + '"]').offset().top + 2;

      _parent.siblings('.ocbc-ds__menu-list__item').removeClass('ocbc-ds__menu-list__item--active');
      _parent.addClass('ocbc-ds__menu-list__item--active');

      if ($(window).width() < 768) {
        $('body').removeClass('ocbc-show-navigation ocbc-rm-scroll');
        $('.js-ds-burger-menu').removeClass('ocbc-burger-menu--close');
        setTimeout(function () {
          $("html, body").animate({
            scrollTop: _scrollTo
          }, 500);
        }, 650);
      } else {
        $("html, body").animate({
          scrollTop: _scrollTo
        }, 500);
      }

    });

  },

  // handleClickBurgerMenu
  handleClickBurgerMenu: function () {
    $('.js-ds-burger-menu').on('click', function (e) {
      var _this = $(this);
      if (_this.hasClass('ocbc-burger-menu--close')) {
        $('body').removeClass('ocbc-show-navigation ocbc-rm-scroll');
        _this.removeClass('ocbc-burger-menu--close');
      } else {
        _this.addClass('ocbc-burger-menu--close');
        $('body').addClass('ocbc-show-navigation ocbc-rm-scroll');
      }
    });

    $('.js-ds-nav-content').on('click', function (e) {
      if ($('.js-ds-burger-menu').hasClass('ocbc-burger-menu--close')) {
        $('body').removeClass('ocbc-show-navigation ocbc-rm-scroll');
        $('.js-ds-burger-menu').removeClass('ocbc-burger-menu--close');
      }
    });

    $('.ocbc-ds__search-form, .ocbc-ds__nav__content__scrollover').on('click', function (e) {
      e.stopPropagation();
    });

  },

  // handleClickToggleCode
  handleClickToggleCode: function () {
    $('.js-show-code').on('click', function () {
      var _this = $(this),
        _target = _this.parents('.ocbc-ds__components-code').find('.ocbc-ds__components-tabs');
      if (_this.hasClass('show')) {
        _this.removeClass('show').text('Show Code');
        _target.fadeOut(300);
      } else {
        _this.addClass('show').text('Hide Code');
        _target.fadeIn(300);
      }
    });
  },

  // handleCheckScroll
  handleCheckScroll: function () {
    var _windowScroll = $(window).scrollTop();



  },

  // handleScroll
  handleScroll: function () {
    $(window).scroll(function (event) {
      DesignSystem.handleCheckScroll();
    });

  },

  // handleCopyComponentsCode
  handleCopyComponentsCode: function () {
    $('.js-ds-visual-block').each(function (index, element) {
      var _this = $(element),
        t = _this.html().trim(),
        _pugCodeAll = '';

      _this.children().each(function (idx, val) {
        var _text = val.textContent,
          _className = ' ' + val.className,
          _classNamePug = _className.replace(/\s+/g, '.').toLowerCase(),
          _tagName = val.tagName.toLowerCase(),
          _pugCode = _tagName + _classNamePug + ' ' + _text;
        _pugCodeAll = _pugCode;
      });

      var _parent = _this.parents('.ocbc-ds__components-visualxcode');
      _parent.find('[data-code="html"]').html("<pre><code class='xml'>" + t.replace(/</g, '&lt;').replace(/>/g, '&gt;') + "</code></pre>");
      // _parent.find('[data-code="pug"]').html("<pre><code class='pug'>" + _pugCodeAll + "</code></pre>");
      hljs.initHighlightingOnLoad();
    });

  },

  // handleClickComponentsTabs
  handleClickComponentsTabs: function () {
    $('.js-ds-components-tabs').on('click', function (e) {
      var _this = $(this),
        _parent = _this.parents('.ocbc-ds__components-tabs'),
        _target = _this.attr('data-target'),
        _classPane = 'ocbc-ds__components-tabs__pane',
        _classControl = 'ocbc-ds__components-tabs__control__item';

      _parent.find('.' + _classControl).removeClass(_classControl + '--active');
      _this.addClass(_classControl + '--active');

      _parent.find('.' + _classPane).removeClass(_classPane + '--active');
      _parent.find('.' + _classPane + '[data-code="' + _target + '"]').addClass(_classPane + '--active');

    });

  },

  // handleCopyCode
  handleCopyCode: function () {

    var clipboard = new ClipboardJS('.js-ds-copy-code');

    $('.js-ds-copy-code').on('click', function (e) {
      var _this = $(this),
        _parent = _this.parents('.js-ds-code-block'),
        _text = _parent.find('.ocbc-ds__components-tabs__pane--active code').text().trim();
      _this.attr('data-clipboard-text', _text);

      var _copiedClass = 'ocbc-ds__copied-text';
      $('.' + _copiedClass).addClass(_copiedClass + '--show');
      setTimeout(function () {
        $('.' + _copiedClass).removeClass(_copiedClass + '--show');
      }, 2500);

    });

  },

  // load
  load: function () {
    $(window).on("load", function () {
      var _splashScreenShow = 'ocbc-ds__splash-screen--show';
      $('.ocbc-ds__splash-screen').addClass(_splashScreenShow + '-desc');
      setTimeout(function () {
        $('.ocbc-ds__splash-screen').removeClass(_splashScreenShow + ' ' + _splashScreenShow + '-desc');
        DesignSystem.handleCopyCode();
      }, 1500);
      setTimeout(function () {
        $('.ocbc-ds__splash-screen').remove();
        DesignSystem.handleCheckScroll();
      }, 4500);
    });

  },

  // init
  init: function () {
    DesignSystem.handleScroll();
    DesignSystem.handleClickMenuItemLink();
    DesignSystem.handleClickBurgerMenu();
    DesignSystem.handleClickComponentsTabs();
    DesignSystem.handleClickToggleCode();
    DesignSystem.handleCopyComponentsCode();
    DesignSystem.load();
  }

};

(function ($) {
  DesignSystem.init();
})(jQuery);
