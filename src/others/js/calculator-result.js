/* ------------------------------------------------------------------------------
@name: Calculator Result
@description: Calculator Result dummy JS
--------------------------------------------------------------------------------- */

// -- REGEX
var WHITESPACE = /^ *$/;
var NUMREG = /^[0-9]+$/;

/**** GLOBAL FUNCTION
 * --------------------------------------------------------------------------------- */
// -- handleFormatIDR
var handleFormatIDR = function (number, prefix = 'Rp') {
  var _split = String(number).split('.'),
    _mod = _split[0].length % 3,
    _idr = _split[0].substr(0, _mod),
    _thousands = _split[0].substr(_mod).match(/\d{3}/gi),
    _separator = '',
    _result;

  // if thousands
  if (_thousands) {
    _separator = _mod ? ',' : '';
    _idr += _separator + _thousands.join(',');
  }

  _idr = (_split[1] != undefined ? _idr + '.' + _split[1] : _idr);
  _result = (prefix != undefined ? prefix + _idr : _idr);
  return _result;
};

// --- handleCleancurrency
var handleCleancurrency = function (string) {
  var _rp = string.split('Rp'),
    _numberFormat = _rp[0],
    _numberClean;

  if (_rp[1] != undefined) {
    _numberFormat = _rp[1];
  }
  _numberClean = parseInt(_numberFormat.split(',').join(''));
  if (Number.isNaN(_numberClean)) {
    _numberClean = 0;
  }
  return _numberClean;
};

// -- handleCleanPercentage
var handleCleanPercentage = function (string) {
  var _split = parseInt(string.split('%').join('')),
    _result = _split;
  if (Number.isNaN(_result)) {
    _result = 0;
  }
  return _result;
};

// -- handleFormatPercentage
var handleFormatPercentage = function (number) {
  var _result = number + '%';
  return _result;
};

// -- handleCleanYear
var handleCleanYear = function (string) {
  var _split = parseInt(string.split(' Tahun').join('')),
    _result = _split;
  if (Number.isNaN(_result)) {
    _result = 0;
  }
  return _result;
};

// -- handleFormatYear
var handleFormatYear = function (number) {
  var _result = number + ' Tahun';
  return _result;
};

// --- handleStartRangeSlider
var handleStartRangeSlider = function (data) {
  var _classname = data.slider[0].className,
    _target = _classname.replace(/\s/g, '.');

  if (data.min === data.from) {
    $(`.${_target.concat(' .irs-single')}`).css('opacity', '0');
  } else {
    $(`.${_target.concat(' .irs-single')}`).css('opacity', '1');
  }
};

// --- handleChangeRangeSlider
var handleChangeRangeSlider = function (data) {
  var _classname = data.slider[0].className,
    _target = _classname.replace(/\s/g, '.');

  var _max = parseInt(data.max) - (data.max / 20),
    _min = parseInt(data.min) + (data.max / 20);

  $(`.${_target.concat(' .irs-single')}`).css('opacity', '1');

  // left align
  if (_min > data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 0,
      'right': 'auto',
    });
  }
  // right align
  else if (_max < data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 'auto',
      'right': 0
    });
  } else {
    $(`.${_target.concat(' .irs-single')}`).css('right', 'auto');
  }
};

// --- handleFinishRangeSlider
var handleFinishRangeSlider = function (data) {
  var _classname = data.slider[0].className,
    _target = _classname.replace(/\s/g, '.');

  // --- if user select min value
  if (data.min === data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 0,
      'right': 'auto',
    });
  }
  // --- if user select max value
  else if (data.max === data.from) {
    $(`.${_target.concat(' .irs-single')}`).css({
      'left': 'auto',
      'right': 0
    });
  }
};

/**** ShareResult
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var ShareResult = (function () {

  // --- handleConvert
  var handleConvert = function () {

    var _element = document.getElementById('shareResult'),
      _heightContent = $('.ocbc-share-result').height(),
      _title = $('.ocbc-share-result__desc__title').text(),
      _opt = {
        filename: _title + '.pdf',
        image: {
          type: 'jpeg',
          quality: 1
        },
        html2canvas: {
          scale: 2
        },
        jsPDF: {
          unit: 'px',
          format: [1440, _heightContent],
          orientation: 'portrait'
        }
      };

    // Print
    if (location.hash == '#print') {
      setTimeout(function(){
        window.print()
      }, 3000);
    }

    // Share Email
    else if (location.hash == '#email') {
      html2pdf().set(_opt).from(_element).toPdf().get('pdf').then(function (pdf) {
        window.open('mailto:?subject=' + _title + '&body=' + _title + ' - ' + pdf.output('bloburl'), '_blank');
      });
    }
    // Share WA
    else if (location.hash == '#whatsapp') {
      html2pdf().set(_opt).from(_element).toPdf().get('pdf').then(function (pdf) {
        window.open('whatsapp://send?text=' + _title + ' - ' + pdf.output('bloburl'), '_blank');
      });
    }
    // Save
    else if (location.hash == '#save') {
      html2pdf().set(_opt).from(_element).save();
    }

  }

  // --- handlePrint
  var handlePrint = function () {
    $('.js-calc-result-print').on('click', function () {
      var _this = $(this),
        _to = _this.attr('data-to');
      window.open(_to, '_blank');

    });
  }

  // --- init
  var init = function () {
    if ($('.js-share-result').length) {
      handleConvert();
    }
    if ($('.js-calc-result-print').length) {
      handlePrint();
    }
  }

  // --- return
  return {
    init
  }

})();

/**** CalculatorPensiun
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorPensiun = (function () {

  // --- handleValidation
  var handleValidation = function () {
    var _isError = false,
      _usiaAndaEl = $('.js-usia-anda'),
      _usiaAndaVal = parseInt(handleCleanYear(_usiaAndaEl.val())),
      _usiaPensiunEl = $('.js-usia-pensiun'),
      _usiaPensiunVal = parseInt(handleCleanYear(_usiaPensiunEl.val())),
      _masaPensiunEl = $('.js-masa-pensiun'),
      _masaPensiunVal = parseInt(handleCleanYear(_masaPensiunEl.val())),
      _danaSaatIniVal = handleCleancurrency($('.js-dana-saat-ini').val()),
      _pengembalianTahunanEl = $('.js-pengembalian-tahunan'),
      _pengembalianTahunanVal = handleCleanPercentage(_pengembalianTahunanEl.val());

    if (Number.isNaN(_usiaAndaVal)) {
      _usiaAndaVal = 0;
    }

    if (Number.isNaN(_usiaPensiunVal)) {
      _usiaPensiunVal = 0;
    }

    if (Number.isNaN(_masaPensiunVal)) {
      _masaPensiunVal = 0;
    }

    if (Number.isNaN(_danaSaatIniVal)) {
      _danaSaatIniVal = 0;
    }

    if (Number.isNaN(_pengembalianTahunanVal)) {
      _pengembalianTahunanVal = 0;
    }

    _usiaPensiunEl.parents('.ocbc-field').removeClass('ocbc-field--error-note');
    _masaPensiunEl.parents('.ocbc-field').removeClass('ocbc-field--error-note');
    if (_usiaPensiunVal <= _usiaAndaVal) {
      setTimeout(function () {
        _usiaPensiunEl.parents('.ocbc-field').addClass('ocbc-field--error-note');
      }, 5);
      _isError = true;
    } else if (_masaPensiunVal <= _usiaPensiunVal) {
      setTimeout(function () {
        _masaPensiunEl.parents('.ocbc-field').addClass('ocbc-field--error-note');
      }, 5);
      _isError = true;
    }

    if (_usiaAndaVal < 1) {
      _isError = true;
    } else if (_usiaPensiunVal < 1) {
      _isError = true;
    } else if (_masaPensiunVal < 1) {
      _isError = true;
    }

    // cek dana saat ini
    if (_danaSaatIniVal < 1) {
      _isError = true;
    }

    // error state _pengembalianTahunan > 100
    if (_pengembalianTahunanVal < 1 || _pengembalianTahunanVal > 100) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-submit-calc-result').prop('disabled', true);
    } else {
      $('.js-form-submit-calc-result').prop('disabled', false);
    }

  }

  // --- handleInput
  var handleInput = function () {

    // Usia Anda + Usia Pensiun + Masa Pensiun
    $('.js-usia-anda, .js-usia-pensiun, .js-masa-pensiun').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanYear = handleCleanYear(_val),
        _formatYear = handleFormatYear(_cleanYear);

      // validation
      handleValidation();

      // set val
      _this.val(_formatYear);

    });

    // Dana Saat Ini
    $('.js-dana-saat-ini').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);

    });

    // Dana Saat Ini + Usia Anda + Usia Pensiun + Masa Pensiun
    $('.js-dana-saat-ini, .js-usia-anda, .js-usia-pensiun, .js-masa-pensiun').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

    // Usia Anda + Usia Pensiun + Masa Pensiun Backspace
    $('.js-usia-anda, .js-usia-pensiun, .js-masa-pensiun').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 7) + ' Tahun');
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // validation
      handleValidation();

      // set val
      _this.val(_percentage);

    });

    // Pengembalian Tahunan Backspace
    $('.js-pengembalian-tahunan').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _lengthVal = e.target.value.length;
      if (!NUMREG.test(_key) || _lengthVal > 3) return false;
    });

  }

  // --- handleClick
  var handleClick = function () {
    $('.js-form-submit-calc-result').on('click', function () {
      if ($(window).width() <= 768) {
        $('.ocbc-calc-result').removeClass('show-fields');
        $('.ocbc-calc-result').find('.ocbc-calc-result__flds-wrapper').slideUp(300);
        e.preventDefault();
      }
    });
  }

  // --- initialize
  var init = function () {
    if ($('[data-form="pensiun"]').length) {
      handleInput();
      handleClick();
    }

  }

  return {
    init: init
  }

})();

/**** CalculatorKPM
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorKPM = (function () {

  // --- handleValidation
  var handleValidation = function () {
    var _isError = false,
      _zoneVal = $('.js-select-zone select').val(),
      _OTDVal = handleCleancurrency($('.js-input-otd').val()),
      _tenorVal = $('.js-select-tenor select').val(),
      _DPNumber = handleCleanPercentage($('.js-input-dp').val()),
      _installmentVal = handleCleancurrency($('.js-input-installment').val());

    // cek value wilayah
    if (WHITESPACE.test(_zoneVal)) {
      _isError = true;
    }

    // cek value OTD
    if (_OTDVal < 1) {
      _isError = true;
    }

    // cek value tenor
    if (WHITESPACE.test(_tenorVal)) {
      _isError = true;
    }

    // error state DP < 25%
    if (_DPNumber < 25) {
      _isError = true;
    }

    // cek installment
    if (_installmentVal < 1) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-submit-calc-result').prop('disabled', true);
    } else {
      $('.js-form-submit-calc-result').prop('disabled', false);
    }

  }

  // --- handleChange
  var handleChange = function () {
    $('.js-select-zone select, .js-select-tenor select').on('change', function (e) {
      // validation
      handleValidation();
    });

  }

  // --- handleInput
  var handleInput = function () {

    // OTD
    $('.js-input-otd').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);

    });


    // DP
    $('.js-input-dp').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      _this.parents('.ocbc-field').removeClass('ocbc-field--error-note');
      if (_percentNumber < 25) {
        _this.parents('.ocbc-field').addClass('ocbc-field--error-note');
      }

      // validation
      handleValidation();

      // set val
      _this.val(_percentage);

    });

    // DP Backspace
    $('.js-input-dp').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // Installment
    $('.js-input-installment').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);
    });

    // OTD + DP + Installment
    $('.js-input-otd, .js-input-dp, .js-input-installment').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

  }

  // --- handleClick
  var handleClick = function () {
    $('.js-form-submit-calc-result').on('click', function () {
      if ($(window).width() <= 768) {
        $('.ocbc-calc-result').removeClass('show-fields');
        $('.ocbc-calc-result').find('.ocbc-calc-result__flds-wrapper').slideUp(300);
        e.preventDefault();
      }
    });
  }

  // --- initialize
  var init = function () {
    if ($('[data-form="kpm"]').length) {
      handleChange();
      handleInput();
      handleClick();
    }

  }

  return {
    init: init
  }

})();

/**** CalculatorPendidikan
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorPendidikan = (function () {

  // --- handleValidation
  var handleValidation = function () {

    var _isError = false,
      _jangkaWaktuEl = $('.js-jangka-waktu'),
      _jangkaWaktuVal = parseInt(handleCleanYear(_jangkaWaktuEl.val())),
      _danaSaatIniVal = handleCleancurrency($('.js-dana-saat-ini').val()),
      _pengembalianTahunanEl = $('.js-pengembalian-tahunan'),
      _pengembalianTahunanVal = handleCleanPercentage(_pengembalianTahunanEl.val());

    // jangka waktu
    if (Number.isNaN(_jangkaWaktuVal)) {
      _jangkaWaktuVal = 0;
    }

    if (_jangkaWaktuVal < 1) {
      _isError = true;
    }

    //  Tahu Dana
    if ($('[data-form="pendidikan-tahu-dana"]').length) {
      var _estimasiDanaVal = handleCleancurrency($('.js-estimasi-dana').val());

      // estimasi dana
      if (Number.isNaN(_estimasiDanaVal)) {
        _estimasiDanaVal = 0;
      }

      if (_estimasiDanaVal < 1) {
        _isError = true;
      }
    }

    //  Tidak Tahu Dana
    if ($('[data-form="pendidikan-tidak-tahu-dana"]').length) {
      var _tujuanSekolah = $('.js-tujuan-sekolah select').val();

      // tujuan sekolah
      if (WHITESPACE.test(_tujuanSekolah)) {
        _isError = true;
      }
    }


    // cek dana saat ini
    if (Number.isNaN(_danaSaatIniVal)) {
      _danaSaatIniVal = 0;
    }

    if (_danaSaatIniVal < 1) {
      _isError = true;
    }

    // cek pengembalian tahunan
    if (Number.isNaN(_pengembalianTahunanVal)) {
      _pengembalianTahunanVal = 0;
    }

    if (_pengembalianTahunanVal < 1) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-submit-calc-result').prop('disabled', true);
    } else {
      $('.js-form-submit-calc-result').prop('disabled', false);
    }

  }

  // --- handleInput
  var handleInput = function () {

    // Jangka Waktu
    $('.js-jangka-waktu').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanYear = handleCleanYear(_val),
        _formatYear = handleFormatYear(_cleanYear);

      // validation
      handleValidation();

      // set val
      _this.val(_formatYear);

    });

    // Estimasi Dana + Dana Saat Ini
    $('.js-estimasi-dana, .js-dana-saat-ini').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);

    });

    // Jangka Waktu + Estimasi Dana + Dana Saat Ini
    $('.js-jangka-waktu, .js-estimasi-dana, .js-dana-saat-ini').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

    // Jangka Waktu
    $('.js-jangka-waktu').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 7) + ' Tahun');
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // validation
      handleValidation();

      // set val
      _this.val(_percentage);

    });

    // Pengembalian Tahunan Backspace
    $('.js-pengembalian-tahunan').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _lengthVal = e.target.value.length;
      if (!NUMREG.test(_key) || _lengthVal > 3) return false;
    });

  }

  // --- handleChange
  var handleChange = function () {
    $('.js-tujuan-sekolah').on('change', function (e) {
      // validation
      handleValidation();
    });

  }


  // --- initialize
  var init = function () {
    if ($('[data-form="pendidikan-tahu-dana"]').length || $('[data-form="pendidikan-tidak-tahu-dana"]').length) {
      handleInput();
      handleChange();
    }

    // dummy data untuk chart pendidikan tahu dana
    if ($('[data-chart="pendidikan-tahu-dana"]').length) {
      var _pendidikanTahuDanaChart = {
        /*
          calc: 'string'
          - untuk pembeda grafik pendikan atau KPR. Value calc: pendidikan | kpr
        */
        calc: 'pendidikan',
        /*
          chart: 'string'
          - chart diambil dari value custom attribute data-chart HTML ya, sebagai identias chart
        */
        chart: 'pendidikan-tahu-dana',
        /*
          label: arrray object
          - array 0 posisi kiri di UI
          - array 1 posisi kanan di UI
        */
        label: [
          {
            title: 'Tahun ke',
            number: 0
          },
          {
            title: 'Tahun ke',
            number: 6
          }
        ],
        /*
          data: arrray object
          dibawah contoh untuk grafik kelebihan dana
          - array 0 Dana Saat ini
          - array 1 Dana di tahun ke-N
          - array 2 Biaya sekolah pada tahun N
          - array 3 Kelebihan Dana
        */
        data: [
          {
            title: 'Dana Saat Ini',
            amount: 400000000
          },
          {
            title: 'Dana di tahun ke-6',
            amount: 7279395496
          },
          {
            title: 'Biaya sekolah pada tahun 2026',
            amount: 3463157329
          },
          {
            title: 'Kelebihan Dana',
            amount: 3816238167
          }
        ]
      };

      // draw chart
      calculatorChart.draw(_pendidikanTahuDanaChart);
    }

    // dummy data untuk chart pendidikan tidak tahu dana
    if ($('[data-chart="pendidikan-tidak-tahu-dana"]').length) {
      var _pendidikanTidakTahuDanaChart = {
        /*
          calc: 'string'
          - untuk pembeda grafik pendikan atau KPR. Value calc: pendidikan | kpr
        */
        calc: 'pendidikan',
        /*
          chart: 'string'
          - chart diambil dari value custom attribute data-chart HTML ya, sebagai identias chart
        */
        chart: 'pendidikan-tidak-tahu-dana',
        /*
          label: arrray object
          - array 0 posisi kiri di UI
          - array 1 posisi kanan di UI
        */
        label: [
          {
            title: 'Tahun ke',
            number: 0
          },
          {
            title: 'Tahun ke',
            number: 6
          }
        ],
        /*
          data: arrray object
          dibawah contoh untuk grafik kelebihan dana
          - array 0 Dana Saat ini
          - array 1 Dana di tahun ke-N
          - array 2 Biaya sekolah pada tahun N
          - array 3 Selisih Biaya
          - array 4 Dana yang harus disisihkan per bulan (Selama 6 Tahun)
        */
        data: [
          {
            title: 'Dana Saat Ini',
            amount: 500000000
          },
          {
            title: 'Dana di tahun ke-6',
            amount: 909924437
          },
          {
            title: 'Biaya sekolah pada tahun 2026',
            amount: 2578616310
          },
          {
            title: 'Selisih Biaya',
            amount: 1688691873
          },
          {
            title: 'Dana yang harus disisihkan per bulan (Selama 6 Tahun)',
            amount: 5230000
          }
        ]
      };

      // draw chart
      calculatorChart.draw(_pendidikanTidakTahuDanaChart);
    }

  }

  return {
    init: init
  }

})();

/**** CalculatorKPR
function ini asumsi kita saja, silakan kalau memang ada penyeseuaian dari temen-temen blend free untuk di ubah
--------------------------------------------------------------------------------- */
var CalculatorKPR = (function () {

  // --- handleValidation
  var handleValidation = function () {
    var _isError = false,
      _jangkaWaktuEl = $('.js-jangka-waktu'),
      _jangkaWaktuVal = parseInt(handleCleanYear(_jangkaWaktuEl.val())),
      _danaSaatIniVal = handleCleancurrency($('.js-dana-saat-ini').val()),
      _simpananPerbulanVal = handleCleancurrency($('.js-simpanan-perbulan').val()),
      _pengembalianTahunanEl = $('.js-pengembalian-tahunan'),
      _pengembalianTahunanVal = handleCleanPercentage(_pengembalianTahunanEl.val());

    // jangka waktu
    if (Number.isNaN(_jangkaWaktuVal)) {
      _jangkaWaktuVal = 0;
    }

    if (_jangkaWaktuVal < 1 || _jangkaWaktuVal > 20) {
      _isError = true;
    }

    // cek dana saat ini
    if (Number.isNaN(_danaSaatIniVal)) {
      _danaSaatIniVal = 0;
    }

    if (_danaSaatIniVal < 1) {
      _isError = true;
    }

    // cek biaya perbulan
    if (Number.isNaN(_simpananPerbulanVal)) {
      _simpananPerbulanVal = 0;
    }

    if (_simpananPerbulanVal < 1) {
      _isError = true;
    }

    // cek pengembalian tahunan
    if (Number.isNaN(_pengembalianTahunanVal)) {
      _pengembalianTahunanVal = 0;
    }

    if (_pengembalianTahunanVal < 1) {
      _isError = true;
    }

    if (_isError) {
      $('.js-form-submit-calc-result').prop('disabled', true);
    } else {
      $('.js-form-submit-calc-result').prop('disabled', false);
    }

  }

  // --- handleInput
  var handleInput = function () {

    // Jangka Waktu
    $('.js-jangka-waktu').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanYear = handleCleanYear(_val),
        _formatYear = handleFormatYear(_cleanYear);

      // validation
      handleValidation();

      // set val
      _this.val(_formatYear);

    });

    // Dana Saat Ini + Simpanan Perbulan
    $('.js-dana-saat-ini, .js-simpanan-perbulan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _cleanCurrency = handleCleancurrency(_val),
        _formatCurrency = handleFormatIDR(_cleanCurrency);

      // validation
      handleValidation();

      // set val
      _this.val(_formatCurrency);

    });

    // Jangka Waktu + Dana Saat Ini + Simpanan Perbulan
    $('.js-jangka-waktu, .js-dana-saat-ini, .js-simpanan-perbulan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _valLength = $(this).val().trim().length; // the value length without whitespaces:
      // check against minimum length and backspace
      if (_valLength > 0 && e.keyCode != 8) {
        if (!NUMREG.test(_key)) return false;
      }
    });

    // Jangka Waktu
    $('.js-jangka-waktu').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 7) + ' Tahun');
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('input', function (e) {
      var _this = $(this),
        _val = _this.val(),
        _percentNumber = handleCleanPercentage(_val),
        _percentage = handleFormatPercentage(_percentNumber);

      // validation
      handleValidation();

      // set val
      _this.val(_percentage);

    });

    // Pengembalian Tahunan Backspace
    $('.js-pengembalian-tahunan').on('keydown', function (e) {
      var _this = $(this),
        _val = _this.val();
      if (e.which == 8) {
        _this.val(_val.substring(0, _val.length - 2) + '%');
      }
    });

    // Pengembalian Tahunan
    $('.js-pengembalian-tahunan').on('keypress', function (e) {
      var _key = parseInt(e.key),
        _lengthVal = e.target.value.length;
      if (!NUMREG.test(_key) || _lengthVal > 3) return false;
    });

  }

  // --- handleRangeSlider
  var handleRangeSlider = function () {
    // initialize
    $('.js-tenor-pinjaman').ionRangeSlider({
      skin: "round",
      step: 1,
      grid_snap: false,
      onStart: function (data) {
        // kebutuhan UI
        handleStartRangeSlider(data);
      },
      onChange: function (data) {
        // kebutuhan UI
        handleChangeRangeSlider(data);
      },
      onFinish: function (data) {
        // kebutuhan UI
        handleFinishRangeSlider(data);
        var _value = $('.js-tenor-pinjaman').val();
        console.log('Tenor Pinjaman Value: ' + _value);
      }
    });
  }


  // --- handleClick
  var handleClick = function () {
    // Risk Profile
    $('.js-risk-profile').on('click', function(){
      var _this = $(this),
      _value = _this.val();
      console.log('Risk Profile Value: ' + _value);
    });
  }

  // --- initialize
  var init = function () {
    if ($('[data-form="kpr"]').length) {
      handleInput();
      handleClick();
      handleRangeSlider();
    }

    // dummy data untuk chart KPR solusi dengan KPR
    if ($('[data-chart="kpr-solusi-kpr"]').length) {
      var _KPRSolusiKPR = {
        /*
          calc: 'string'
          - untuk pembeda grafik pendikan atau KPR. Value calc: pendidikan | kpr
        */
        calc: 'kpr',
        /*
          chart: 'string'
          - chart diambil dari value custom attribute data-chart HTML ya
        */
        chart: 'kpr-solusi-kpr',
        /*
          label: arrray object
          - array 0 posisi kiri di UI
          - array 1 posisi tengah di UI
          - array 2 posisi kanan di UI
        */
        label: [
          {
            title: 'Tahun ke',
            number: 0
          },
          {
            title: 'Tahun ke',
            number: 10
          },
          {
            title: 'Tahun ke',
            number: 15
          }
        ],
        /*
          data: arrray object
          dibawah contoh untuk grafik solusi dengan KPR
          - array 0 Dana Saat ini
          - array 1 Simpanan per Bulan (Selama N Tahun)
          - array 2 Dana yang terkumpul (DP)
          - array 3 Target Harga Rumah
          - array 4 Pembayaran KPR per Bulan (Selama N Tahun)
        */
        data: [
          {
            title: 'Dana Saat Ini',
            amount: 100000000
          },
          {
            title: 'Simpanan per Bulan (Selama 10 Tahun)',
            amount: 3000000,
            label: 'Pengembalian 5%'
          },
          {
            title: 'Dana yang terkumpul (DP)',
            amount: 615693594
          },
          {
            title: 'Target Harga Rumah',
            amount: 3078697970
          },
          {
            title: 'Pembayaran KPR per Bulan (Selama <strong>5 Tahun</strong>)',
            amount: 62052555
          }
        ]
      };

      // draw chart
      calculatorChart.draw(_KPRSolusiKPR);
    }

    // dummy data untuk chart KPR solusi Optimalkan Return
    if ($('[data-chart="kpr-solusi-return"]').length) {
      var _KPRSolusiReturn = {
        /*
          calc: 'string'
          - untuk pembeda grafik pendikan atau KPR. Value calc: pendidikan | kpr
        */
        calc: 'kpr-return',
        /*
          chart: 'string'
          - chart diambil dari value custom attribute data-chart HTML ya
        */
        chart: 'kpr-solusi-return',
        /*
          label: arrray object
          - array 0 posisi kiri di UI
          - array 1 posisi kanan di UI
        */
        label: [
          {
            title: 'Tahun ke',
            number: 0
          },
          {
            title: 'Tahun ke',
            number: 10
          }
        ],
        /*
          data: arrray object
          dibawah contoh untuk grafik solusi dengan optimalkan return
          - array 0 Dana Saat ini
          - array 1 Simpanan per Bulan (Selama N Tahun)
          - array 2 Dana yang terkumpul (DP)
          - array 3 Target Harga Rumah
          - array 4 Profil Risiko
        */
        data: [
          {
            title: 'Dana Saat Ini',
            amount: 100000000
          },
          {
            title: 'Simpanan per Bulan (Selama 10 Tahun)',
            amount: 3000000,
            label: 'Pengembalian 5%'
          },
          {
            title: 'Dana yang terkumpul (DP)',
            amount: 615693594,
            percentage: '5%'
          },
          {
            title: 'Target Harga Rumah',
            amount: 3078697970
          },
          {
            title: 'Dengan Conservative Dana yang bisa terkumpul',
            amount: 3128698970,
            percentage: '7.18%'
          }
        ]
      };

      // draw chart
      calculatorChart.draw(_KPRSolusiReturn);
    }

  }

  return {
    init: init
  }

})();


/****************************** CALL ALL FUNCTION ******************************/
ShareResult.init();
CalculatorPensiun.init();
CalculatorKPM.init();
CalculatorPendidikan.init();
CalculatorKPR.init();
